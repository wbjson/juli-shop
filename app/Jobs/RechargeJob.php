<?php

namespace App\Jobs;

use App\Models\IncomeLog;
use App\Models\LevelConfig;
use App\Models\Order;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Models\Recharge;
use App\Models\ShareSet;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class RechargeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $this->orderFinsh($data);
    }

    /**
     * 购买商品
     * @param $data
     * @return void
     */
    private function orderFinsh($data)
    {
        Log::channel('recharge_callback')->info('开始处理支付订单请求');
        $remarks = explode('@', $data['remarks']);
        $orderIds = $remarks[0];
        if ($orderIds <= 0) {
            Log::channel('recharge_callback')->info('未找到订单ID');
            exit();
        }
        $order = Order::query()
            ->where('id', $orderIds)
            ->first();
        if (empty($order)) {
            Log::channel('recharge_callback')->info('该订单不存在');
            exit();
        }
        if ($order->order_status != 0) {
            Log::channel('recharge_callback')->info('该订单已完成');
            exit();
        }

        DB::beginTransaction();
        try {
            $user = User::query()->where('name', $data['from_address'])->first();

            $order->total_usdt = $data['amount'];
            $order->order_status = 1;
            $order->payment_type = 1;
            $order->pay_time = date('Y-m-d H:i:s');
            $order->save();

            $orderList = OrdersDetail::query()
                ->where('order_id', $order->id)
                ->select(['id', 'order_id', 'product_id', 'product_count', 'order_status'])
                ->get();
            if (!empty($orderList)) {
                foreach ($orderList as $v) {
                    Product::query()->where('id', $v->product_id)->increment('sales', $v->product_count);
                    Product::query()->where('id', $v->product_id)->decrement('stock', $v->product_count);
                    OrdersDetail::query()->where('id', $v->id)->update(['order_status' => 1]);
                }
            }

            $coinPrice = config('lvy_price');
            if ($order->type == 1) {
                $coinAmount = 600;       //获得冻结LVY
                $user->myperfor = bcadd($user->myperfor, $data['amount'], 4);   //我的USDT业绩
                $user->member_freeze = bcadd($user->member_freeze, $coinAmount, 4);   //冻结积分累计
                $user->total_member_freeze = bcadd($user->total_member_freeze, $coinAmount, 4);   //冻结积分累计
            } else {
                $coinAmount = bcdiv($data['amount'], $coinPrice, 4);       //获得冻结LVY
                $user->product_freeze = bcadd($user->product_freeze, $coinAmount, 4);   //冻结积分累计
                $user->total_product_freeze = bcadd($user->total_product_freeze, $coinAmount, 4);   //冻结积分累计
            }

            $beforeAmount = $user->freeze_coin;
            $user->freeze_coin = bcadd($user->freeze_coin, $coinAmount, 4);   //冻结积分累计
            $user->save();

            /*修改会员等级*/
            IncomeLog::query()->insertGetId([
                'user_id' => $user->id,
                'amount_type' => 2,
                'type' => 1,
                'before' => $beforeAmount,
                'total' => $coinAmount,
                'after' => $user->freeze_coin,
                'remark' => '购买商品' . $data['amount'] . 'USDT',
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if ($order->type == 1 && $user->parent_id > 0) {
                /*推荐等级*/
                LevelConfig::upgrade_level($user->id, $data['amount']);
                /*推荐收益*/
                ShareSet::share_award($user->id, $data['amount']);
                /*加权收益*/
                ShareSet::weight_award($user->id, $data['amount']);
            }

            if ($user->activate <= 0) {
                User::query()->where('id',$user->id)->update(['activate'=>1]);
            }

            Recharge::query()->insert([
                'user_id' => $user->id,
                'name' => $user->name,
                'type' => 1,
                'chargeid' => $order->id,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'status' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'finished_at' => date('Y-m-d H:i:s')
            ]);

            DB::commit();
            Log::channel('recharge_callback')->info('结束处理支付订单请求');
            echo 'success';
        } catch (\Exception $e) {

            DB::rollBack();
            Log::channel('recharge_callback')->info('处理支付订单请求失败' . $e->getLine() . $e->getMessage());
            echo 'failed';
        }
    }


    /**
     * @param $data
     * @return void
     * 参与IDO
     */
    public function joinIdo($data)
    {
        $userInfo = User::query()->where('name', $data['from_address'])
            ->first();
        if (empty($userInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到用户，无法继续');
            exit();
        }
        $orderType = explode('-', $data['remarks'])[0];
        $chargeId = explode('-', $data['remarks'])[2];

        $order = Recharge::query()
            ->where('hash', $data['hash'])
            ->first();
        if (!empty($order)) {
            if ($order->status > 1) {
                exit();
            }
            $orderId = $order->id;
        } else {
            if ($chargeId <= 0) {
                Log::channel('recharge_callback')->info($data['hash'] . '未找到投资IDO，无法继续');
                exit();
            }
            $orderId = Recharge::query()->insertGetId([
                'user_id' => $userInfo->id,
                'name' => $userInfo->name,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'type' => $orderType,
                'chargeid' => $chargeId,
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $idoInfo = IdoList::query()->where('id', $chargeId)->first();
        if (empty($idoInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到IDO信息，无法继续');
            exit();
        }

        $expireDate = date("Ymd", strtotime('+ ' . $idoInfo->arrival_days . 'day'));
        $idoData = [];
        $idoData['ido_id'] = $idoInfo->id;
        $idoData['user_id'] = $userInfo->id;
        $idoData['usdtnum'] = $data['amount'];
        $idoData['coinnum'] = $idoInfo->coinnum;
        $idoData['blind_level'] = $idoInfo->blind_level;
        $idoData['expire_date'] = $expireDate;
        $idoData['hash'] = $data['hash'];
        $idoData['status'] = 0;
        $idoData['created_at'] = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            IdoOrder::query()->insertGetId($idoData);
            /*添加盲盒*/
            $blindInfo = BlindBox::query()->where('blind_level', $idoInfo->blind_level)->first();
            if (!empty($blindInfo)) {
                $blindData = [];
                $blindData['user_id'] = $userInfo->id;
                $blindData['blind_level'] = $blindInfo->blind_level;
                $blindData['blind_num'] = 1;
                $blindData['status'] = 1;
                $blindData['created_at'] = date('Y-m-d H:i:s');
                UserBlindBox::query()->insertGetId($blindData);
            }

            /*修改用户信息*/
            $userInfo->me_performance_usdt = bcadd($userInfo->me_performance_usdt, $data['amount'], 4);     //我的USDT
            $userInfo->me_performance = bcadd($userInfo->me_performance, $idoInfo->coinnum, 4);     //我的IDO Bamboo
            $userInfo->ido_num = bcadd($userInfo->ido_num, $data['amount'], 4);     //IDO USDT
            if ($userInfo->activate != 1) {
                $userInfo->activate = 1;
            }
            $userInfo->save();

            if ($userInfo->parent_id > 0) {
                /*计算业绩*/
                LevelConfig::performace($userInfo->id, $data['amount'], $idoInfo->coinnum);
                /*联创是否达成*/
                LevelConfig::jointUser($userInfo->id);
                /*添加直推奖*/
                Award::share_award($userInfo->id, $idoInfo->coinnum);
                /*添加联创奖金*/
                Award::JointAward($userInfo->id, $idoInfo->coinnum);
            }

            Recharge::query()->where('id', $orderId)->where('hash', $data['hash'])->update(['status' => 2, 'finished_at' => date('Y-m-d H:i:s')]);
            DB::commit();
            Log::channel('recharge_callback')->info('处理完成');
            echo 'success';
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理失败' . $e->getLine() . $e->getMessage());
            echo 'failed';
        }
    }


    /**
     * @return void
     * 认购KOL
     */
    public function JoinKol($data)
    {
        $userInfo = User::query()->where('name', $data['from_address'])
            ->select('id', 'name', 'path', 'deep', 'activate', 'kol_activate', 'amount', 'parent_id', 'me_performance_usdt', 'me_performance', 'path')
            ->first();
        if (empty($userInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到用户，无法继续');
            exit();
        }

        $orderType = explode('-', $data['remarks'])[0];
        $chargeId = explode('-', $data['remarks'])[2];

        $order = Recharge::query()
            ->where('hash', $data['hash'])
            ->first();
        if (!empty($order)) {
            if ($order->status > 1) {
                exit();
            }
            $orderId = $order->id;
        } else {
            if ($chargeId <= 0) {
                Log::channel('recharge_callback')->info($data['hash'] . '未找到认购KOL，无法继续');
                exit();
            }
            $orderId = Recharge::query()->insertGetId([
                'user_id' => $userInfo->id,
                'name' => $userInfo->name,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'type' => $orderType,
                'chargeid' => $chargeId,
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }

        $kolInfo = KolList::query()->where('id', $chargeId)->first();
        if (empty($kolInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到KOL信息，无法继续');
            exit();
        }

        $kolData = [];
        $kolData['kol_id'] = $kolInfo->id;
        $kolData['user_id'] = $userInfo->id;
        $kolData['usdtnum'] = $data['amount'];
        $kolData['coinnum'] = $kolInfo->coinnum;
        $kolData['card_level'] = $kolInfo->card_level;      //赠送卡牌
        $kolData['blind_level'] = $kolInfo->blind_level;        //赠送盲盒
        $kolData['hash'] = $data['hash'];
        $kolData['status'] = 0;
        $kolData['created_at'] = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            KolOrder::query()->insertGetId($kolData);

            $beforeAmount = $userInfo->amount;
            $userInfo->amount = bcadd($userInfo->amount, $kolInfo->coinnum, 4);
            $userInfo->me_performance = bcadd($userInfo->me_performance, $kolInfo->coinnum, 4);     //我的代币业绩
            $userInfo->me_performance_usdt = bcadd($userInfo->me_performance_usdt, $data['amount'], 4);     //我的USDT业绩
            if ($userInfo->kol_activate == 0) {
                $userInfo->kol_activate = 1;
            }
            if ($userInfo->activate != 1) {
                $userInfo->activate = 1;
            }
            $userInfo->save();
            /*添加资金记录*/
            IncomeLog::query()->insert([
                'user_id' => $userInfo->id,
                'amount_type' => 1,
                'type' => 1,
                'before' => $beforeAmount,
                'total' => $kolInfo->coinnum,
                'after' => $userInfo->amount,
                'content' => '认购KOL到账' . $kolInfo->coinnum,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            /*铸造记录*/
            CastingCoin::query()->insertGetId([
                'num' => $kolInfo->coinnum,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if ($userInfo->parent_id > 0) {
                /*计算业绩*/
                LevelConfig::performace($userInfo->id, $data['amount'], $kolInfo->coinnum);
                /*联创是否达成*/
                LevelConfig::jointUser($userInfo->id);
                /*添加直推奖*/
                Award::share_award($userInfo->id, $kolInfo->coinnum);
                /*添加联创奖金*/
                Award::JointAward($userInfo->id, $kolInfo->coinnum);
            }

            Recharge::query()
                ->where('id', $orderId)
                ->where('hash', $data['hash'])
                ->update(['status' => 2, 'finished_at' => date('Y-m-d H:i:s')]);

            DB::commit();
            Log::channel('recharge_callback')->info('处理完成');
            echo 'success';
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理失败' . $e->getLine() . $e->getMessage());
            echo 'failed';
        }
    }

    /**
     * @param $data
     * @return void
     *
     */
    public function upNode($data)
    {
        $userInfo = User::query()->where('name', $data['from_address'])
            ->select('id', 'name', 'path', 'deep', 'activate', 'amount', 'parent_id', 'me_performance_usdt', 'me_performance', 'path')
            ->first();
        if (empty($userInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到用户，无法继续');
            exit();
        }

        $orderType = explode('-', $data['remarks'])[0];
        $chargeId = explode('-', $data['remarks'])[2];

        $order = Recharge::query()
            ->where('hash', $data['hash'])
            ->first();
        if (!empty($order)) {
            if ($order->status > 1) {
                exit();
            }
            $orderId = $order->id;
        } else {
            if ($chargeId <= 0) {
                Log::channel('recharge_callback')->info($data['hash'] . '未找到联创升级，无法继续');
                exit();
            }
            $orderId = Recharge::query()->insertGetId([
                'user_id' => $userInfo->id,
                'name' => $userInfo->name,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'type' => $orderType,
                'chargeid' => $chargeId,
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }

        $jointInfo = JointList::query()->where('id', $chargeId)->first();

        $jointData = [];
        $jointData['joint_id'] = $jointInfo['id'];
        $jointData['user_id'] = $userInfo->id;
        $jointData['usdtnum'] = $data['amount'];
        $jointData['coinnum'] = $jointInfo->coinnum;
        $jointData['card_level'] = $jointInfo->card_level;
        $jointData['blind_level'] = $jointInfo->blind_level;
        $jointData['blind_num'] = $jointInfo->blind_box;
        $jointData['hash'] = $data['hash'];
        $jointData['status'] = 0;
        $jointData['created_at'] = date('Y-m-d H:i:s');

        DB::beginTransaction();
        try {
            JointOrder::query()->insertGetId($jointData);

            $beforeAmount = $userInfo->amount;
            $userInfo->me_performance = bcadd($userInfo->me_performance, $jointInfo->coinnum, 4);     //我的代币业绩
            $userInfo->me_performance_usdt = bcadd($userInfo->me_performance_usdt, $data['amount'], 4);     //我的USDT业绩
            $userInfo->amount = bcadd($userInfo->amount, $jointInfo->coinnum, 4);
            if ($userInfo->activate == 0) {
                $userInfo->activate = 1;
            }
            if ($userInfo->node_status == 0) {
                $userInfo->node_status = 1;
            }
            $userInfo->save();
            /*添加资金记录*/
            IncomeLog::query()->insert([
                'user_id' => $userInfo->id,
                'amount_type' => 1,
                'type' => 3,
                'before' => $beforeAmount,
                'total' => $jointInfo->coinnum,
                'after' => $userInfo->amount,
                'content' => '认购节点成功' . $jointInfo->coinnum,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            /*铸造记录*/
            CastingCoin::query()->insertGetId([
                'num' => $jointInfo->coinnum,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if ($userInfo->parent_id > 0) {
                /*计算业绩*/
                LevelConfig::performace($userInfo->id, $data['amount'], $jointInfo->coinnum);
                /*联创是否达成*/
                LevelConfig::jointUser($userInfo->id);
                /*添加直推奖*/
                Award::share_award($userInfo->id, $jointInfo->coinnum);
                /*添加联创奖金*/
                Award::JointAward($userInfo->id, $jointInfo->coinnum);
            }
            Recharge::query()
                ->where('id', $orderId)
                ->where('hash', $data['hash'])
                ->update(['status' => 2, 'finished_at' => date('Y-m-d H:i:s')]);

            DB::commit();
            Log::channel('recharge_callback')->info('处理完成');
            echo 'success';
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理失败' . $e->getLine() . $e->getMessage());
            echo 'failed';
        }
    }

    /**
     * @param $data
     * @return void
     *升级NFT
     */
    public function upNft($data)
    {
        $userInfo = User::query()->where('name', $data['from_address'])
            ->select('id', 'name', 'path', 'deep', 'activate', 'amount', 'parent_id', 'me_performance_usdt', 'me_performance', 'path')
            ->first();
        if (empty($userInfo)) {
            Log::channel('recharge_callback')->info($data['hash'] . '未找到用户，无法继续');
            exit();
        }

        $orderType = explode('-', $data['remarks'])[0];
        $chargeId = explode('-', $data['remarks'])[2];

        $order = Recharge::query()
            ->where('hash', $data['hash'])
            ->first();
        if (!empty($order)) {
            if ($order->status > 1) {
                exit();
            }
            $orderId = $order->id;
        } else {
            if ($chargeId <= 0) {
                Log::channel('recharge_callback')->info($data['hash'] . '未找到卡牌订单，无法继续');
                exit();
            }
            $orderId = Recharge::query()->insertGetId([
                'user_id' => $userInfo->id,
                'name' => $userInfo->name,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'type' => $orderType,
                'chargeid' => $chargeId,
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $userCard = UserNftcard::query()
            ->where('id', $chargeId)
            ->where('user_id', $userInfo->id)
            ->first();
        if (empty($userCard)) {
            Log::channel('recharge_callback')->info($data['hash'] . 'NFT不存在，无法继续');
            exit();
        }

        /*执行升级*/
        DB::beginTransaction();
        try {
            $cardInfo = CardList::query()->where('card_level', $userCard->card_level)->first();
            $upgradeLevel = CardLevelList::query()
                ->where('level', '>', $userCard->level)
                ->orderByRaw('level')
                ->first();
            /*销毁之前NFT*/
            UserNftcard::destoryCard($userCard->no);

            $no = NftTokenid::query()->where('id', 1)->value('token_id') + 1;
            $cardNo = 'NFT' . $no;

            /*铸造一张新的NFT*/
            UserNftcard::castingCard($no, $upgradeLevel->nftjson_url, $cardNo);
            NftTokenid::query()->where('id', 1)->update(['token_id' => $no]);

            $userCard->token_id = $no;
            $userCard->no = $no;
            $userCard->token_id = $no;
            $userCard->cardno = $cardNo;
            $userCard->name = $cardInfo->name . '*' . $upgradeLevel->name;;
            $userCard->level = $upgradeLevel->level;
            $userCard->rate = $upgradeLevel->rate; //升级之后的数量
            $userCard->save();


            DB::commit();
            Recharge::query()
                ->where('id', $orderId)
                ->where('hash', $data['hash'])
                ->update(['status' => 2, 'finished_at' => date('Y-m-d H:i:s')]);

            DB::commit();
            Log::channel('recharge_callback')->info('处理完成');
            echo 'success';
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理失败' . $e->getLine() . $e->getMessage());
            echo 'failed';
        }
    }

}
