<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BlackHole extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'black_hole';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }


    public static function insertLog($userId,$num,$coin){
        self::query()->insert([
            'user_id' => $userId,
            'num' => $num,
            'status' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        if (config('black_hole_address')){
            $withdrawCoinContract = MainCurrency::query()->where('name',$coin)->value('contract_address');
            try{
                $http = new Client();
                $data = [
                    'address' =>  config('black_hole_address'),
                    'amount' => $num,
                    'contract_address' => $withdrawCoinContract,
                    'notify_url' => '',
                    'remarks' => '进入黑洞'
                ];
                $response = $http->post('http://127.0.0.1:9090/v1/bnb/withdraw',[
                    'form_params' => $data
                ]);
                $result = $response->getBody()->getContents();
                Log::channel('withdraw')->info('提交黑洞申请'.var_export($data, true).'---'.var_export($result, true));

            }catch (\Exception $e){
                Log::channel('withdraw')->info('提交黑洞申请'.$e->getMessage().$e->getLine());
            }
        }
    }

}
