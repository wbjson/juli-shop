<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class BoxLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'box_log';

    public function currency(){
        return $this->hasOne(MainCurrency::class,'id','currency_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
