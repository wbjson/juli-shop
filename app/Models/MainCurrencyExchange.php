<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class MainCurrencyExchange extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'main_currency_exchange';


    public function from(){
        return $this->hasOne(MainCurrency::class,'id','from_coin');
    }

    public function to(){
        return $this->hasOne(MainCurrency::class,'id','to_coin');
    }


    /**
     * 查询币种汇率
     * @param $coin1
     * @param $coin2
     * @return integer
     */
    public static function getRate($coin1,$coin2){
        if ($coin1==$coin2){
            return 1;
        }
        //查询币种汇率
        $exchange = self::query()->where('from_coin_name',$coin1)->where('to_coin_name',$coin2)->first();
        if (empty($exchange)){
            $exchange = self::query()->where('from_coin_name',$coin2)->where('to_coin_name',$coin1)->first();
        }

        if (empty($exchange)){
            throw new \Exception('未找到汇率');
        }
        if ($exchange->from_coin_name==$coin1){
            return $exchange->rate;
        }else{
            return $exchange->fan_rate;
        }
    }

}
