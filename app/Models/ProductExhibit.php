<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class ProductExhibit extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'products_exhibit';


    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }

}
