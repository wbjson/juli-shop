<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class ExchangeOrder extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'exchange_order';


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
