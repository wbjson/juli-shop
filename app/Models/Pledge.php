<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Pledge extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'pledge';

    public function coin(){
        return $this->hasOne(Coin::class,'id','coin_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }


    public static function insertLog($user,$amount,$invest_id){
        $invest = Invest::query()->where('id',$invest_id)->first();
        //LP汇率需要特殊处理下
        if ($invest->type==2){
            $rate = 0;
        }else{
            $rate = Coin::query()->where('id',$invest->coin_id)->value('rate');
        }

        $total = bcmul($amount,$rate,8);
        self::query()->insert([
            'user_id' => $user->id,
            'type' => $invest->type,
            'invest_id' => $invest_id,
            'num' => $amount,
            'rate' => $rate,
            'total' => $total,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        //更新用户算力
        UsersPower::query()->where(['user_id'=>$user->id,'type'=>$invest->type])->increment('power',$total);
    }

}
