<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class ShopApplication extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'shop_application';

    protected $appends = ['legal_card1_path','legal_card2_path','company_logo_path','company_zhi_path'];


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function category(){
        return $this->hasOne(ShopCategory::class,'id','shop_category_id');
    }

    public function getLegalCard1PathAttribute(){
        return Image::query()->where('id',$this->legal_card1)->value('path');
    }

    public function getLegalCard2PathAttribute(){
        return Image::query()->where('id',$this->legal_card2)->value('path');
    }

    public function getCompanyLogoPathAttribute(){
        return Image::query()->where('id',$this->company_logo)->value('path');
    }
    public function getCompanyZhiPathAttribute(){
        return Image::query()->where('id',$this->company_zhi)->value('path');
    }
}
