<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class RecommendList extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'recommend_list';
    
}
