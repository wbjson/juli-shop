<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WithdrawRateLog extends Model
{

    protected $table = 'withdraw_rate_log';

}
