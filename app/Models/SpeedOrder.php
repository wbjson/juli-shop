<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class SpeedOrder extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'speed_order';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function user_nickname(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
