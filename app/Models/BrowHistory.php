<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class BrowHistory extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'brow_history';


    public $appends = ['product_img_path'];

    public function getProductImgPathAttribute(){
        return assertUrl(Image::query()->where('id',$this->product_img)->value('path'),'admin');
    }
}
