<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;

class ShareSet extends Model
{
    use HasDateTimeFormatter;

    protected $table = 'share_set';

    /*推荐收益*/
    public static function share_award($uid, $usdtnum)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'deep', 'path')
            ->first();
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }

        $shareMaxLayer = LevelConfig::query()->max('share_max_layer');
        $userPath = User::query()
            ->whereIn('id', $path)
            ->where('activate', 1)
            ->where('level', '>', 1)
            ->whereBetween('deep', [$userInfo->deep - $shareMaxLayer, $userInfo->deep - 1])
            ->orderByDesc('id')
            ->pluck('id');
        if (empty($userPath)) {
            return true;
        }

        $levelCount = 0;        //总共分发奖金次数
        $lastLevel = 0;        //上一次收益的等级
        foreach ($userPath as $v) {
            if ($levelCount > 1) {
                break;
            }
            if ($lastLevel >= 3) {
                break;
            }
            $parentInfo = User::query()->where('id', $v)->first();
            $deepDiff = $userInfo->deep - $parentInfo->deep;
            $levelInfo = LevelConfig::query()
                ->where('level', $parentInfo->level)
                ->first();
            if ($levelInfo->share_max_layer < $deepDiff) {
                continue;
            }
            $awardList = explode(',', $levelInfo->share_rate);
            $awardRate = $awardList[$deepDiff - 1];

            if ($awardList <= 0) {
                continue;
            }

            $awardNum = bcmul($usdtnum, $awardRate, 4) / 100;         //USDT收益比例
            $beforeUsdt = $parentInfo->amount;
            $parentInfo->amount = bcadd($parentInfo->amount, $awardNum, 4);
            $parentInfo->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $parentInfo->id,
                'amount_type' => 1,
                'type' => 2,
                'before' => $beforeUsdt,
                'total' => $awardNum,
                'after' => $parentInfo->amount,
                'remark' => '下' . $deepDiff . '级购买会员区商品' . $usdtnum . 'USDT',
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            $levelCount++;
            $lastLevel = $parentInfo->level;
        }
        return true;
    }

    /*加权收益*/
    public static function weight_award($uid, $usdtnum)
    {
        $levelList = LevelConfig::query()
            ->where('weight_num', '>', 0)
            ->orderByRaw('level')
            ->select('id', 'name', 'level', 'weight_num')
            ->get();
        foreach ($levelList as $v) {
            $levelUser = User::query()
                ->where('level', $v->level)
                ->pluck('id');
            if ($levelUser->isEmpty()) {
                continue;
            }
            $awardNum = bcdiv($v->weight_num, count($levelUser), 2);
            if ($awardNum <= 0) {
                continue;
            }

            $content = '购买会员区商品' . $usdtnum . ',' . $v->name . '共' . count($levelUser) . '个';
            foreach ($levelUser as $v) {
                $beforeUsdt = User::query()->where('id', $v)->value('amount');
                IncomeLog::query()->insertGetId([
                    'user_id' => $v,
                    'amount_type' => 1,
                    'type' => 3,
                    'before' => $beforeUsdt,
                    'total' => $awardNum,
                    'after' => bcadd($beforeUsdt, $awardNum, 2),
                    'remark' => $content,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
            User::query()->whereIn('id', $levelUser)->increment('amount', $awardNum);
        }
        return true;
    }


    public static function share_coin_award($uid, $num)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'phone', 'parent_id', 'deep', 'path', 'level', 'myperfor', 'coin_money', 'sign_money')
            ->first();
        if ($userInfo->parent_id <= 0) {
            return true;
        }

        $parentInfo = User::query()->where('id', $userInfo->parent_id)->first();
        if ($parentInfo->activate != 1) {
            return true;
        }
        $maxLevel = LevelConfig::query()->max('level');
        $shareList = User::query()
            ->where('parent_id', $userInfo->parent_id)
            ->where('activate', '>=', 1)
            ->where('level', '<', $maxLevel)
            ->orderByRaw('id')
            ->pluck('id');
        if ($shareList->isEmpty()) {
            return true;
        }
        $shareList = $shareList->toArray();
        $shareCount = array_search($uid, $shareList) + 1;
        if ($shareCount < 1) {
            return true;
        }
        $shareInfo = ShareSet::query()
            ->where('start_count', '<=', $shareCount)
            ->where('end_count', '>=', $shareCount)
            ->orderByDesc('award_rate')
            ->first();
        if (empty($shareInfo)) {
            return true;
        }

        $parent_point = ReleaseOrder::query()->where('user_id', $parentInfo->id)->sum('total_num');
        $user_point = ReleaseOrder::query()->where('user_id', $userInfo->id)->sum('total_num');
        $burnRate = bcdiv($parent_point, $user_point, 3);
        if ($burnRate >= 1) {
            $burnRate = 1;
        }

        $theoryNum = bcmul($num * $burnRate, $shareInfo->award_rate, 2) / 100;
        $parentOrder = ReleaseOrder::query()
            ->where('user_id', $parentInfo->id)
            ->where('status', 1)
            ->orderByRaw('reset_num')
            ->first();
        if (empty($parentOrder)) {
            return true;
        }
        $theoryNum > $parentOrder->reset_num ? $awardNum = $parentOrder->reset_num : $awardNum = $theoryNum;
        if ($awardNum <= 0) {
            return true;
        }
        /*执行收益*/
        $beforeAmount = $parentInfo->sign_money;
        $parentInfo->sign_money = bcadd($parentInfo->sign_money, $awardNum, 4);
        $parentInfo->coin_money = bcsub($parentInfo->coin_money, $awardNum, 4);
        $parentInfo->save();

        $content = '第' . $shareCount . '个直推' . $userInfo->phone . '我总积分' . $parent_point . '积分释放' . $num;
        IncomeLog::query()->insertGetId([
            'user_id' => $parentInfo->id,
            'from_id' => $uid,
            'amount_type' => 5,
            'type' => 3,
            'before' => $beforeAmount,
            'total' => $awardNum,
            'after' => $parentInfo->sign_money,
            'remark' => $content,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        $parentOrder->released_num = bcadd($parentOrder->released_num, $awardNum, 2);     //增加释放数量
        $parentOrder->speed_num = bcadd($parentOrder->speed_num, $awardNum, 2);     //加速数量
        $parentOrder->reset_num = bcsub($parentOrder->reset_num, $awardNum, 2);     //增加释放数量
        /*出局*/
        if ($theoryNum >= $parentOrder->reset_num) {
            $parentOrder->status = 2;
        }
        $parentOrder->save();

        return true;
    }


    /**
     * @return void
     * 加速释放
     */
    public static function speed_release($uid, $num)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'deep', 'path')
            ->first();
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }

        $userPath = User::query()
            ->whereIn('id', $path)
            ->where('level', '>=', 2)
            ->orderByDesc('id')
            ->pluck('id');
        if (empty($userPath)) {
            return true;
        }

        $sameRate = 20;     //同级收益比例

        $lastLevel = 0;     //上一次的等级
        $lastAward = 0;     //上一次的奖金
        $levelCount = 0;     //同等级出现次数
        foreach ($userPath as $v) {
            $parentInfo = User::query()->where('id', $v)
                ->select('id', 'name', 'deep', 'amount', 'level', 'coin_money', 'freeze_coin', 'member_freeze',
                    'total_member_freeze', 'product_freeze', 'total_product_freeze')
                ->first();

            $levelInfo = LevelConfig::query()->where('level', $parentInfo->level)->first();
            $deepDiff = $userInfo->deep - $parentInfo->deep;
            if ($levelInfo->release_max_layer < $deepDiff) {
                continue;
            }

            $awardList = explode(',', $levelInfo->release_rate);
            if ($parentInfo->level == $lastLevel) {
                if ($levelCount >= 2) {
                    continue;
                }
                $teamAward = bcmul($lastAward, $sameRate, 2) / 100;     //同级收益
                $levelCount++;
                $levelName = LevelConfig::query()->where('level', $lastLevel)->value('name');
                $content = '上一个' . $levelName . '收益' . $lastAward;
            } else {
                $awardRate = $awardList[$deepDiff - 1];
                if ($awardRate <= 0) {
                    continue;
                }
                $teamAward = bcmul($num, $awardRate, 2) / 100;
                $levelCount = 0;

                $content = '下' . $deepDiff . '级LVX收益' . $num;
            }

            if ($teamAward < 0.001) {
                continue;
            }
            if ($parentInfo->member_freeze > 0) {
                if ($teamAward >= $parentInfo->member_freeze) {
                    $awardNum = $parentInfo->member_freeze;
                    $parentInfo->total_member_freeze = 0;
                } else {
                    $awardNum = $teamAward;
                    $parentInfo->member_freeze = bcsub($parentInfo->member_freeze, $awardNum, 2);
                }
            } else {
                if ($teamAward >= $parentInfo->product_freeze) {
                    $awardNum = $parentInfo->product_freeze;
                    $parentInfo->total_product_freeze = 0;
                } else {
                    $awardNum = $teamAward;
                    $parentInfo->product_freeze = bcsub($parentInfo->product_freeze, $awardNum, 2);
                }
            }

            if ($awardNum <= 0.001) {
                continue;
            }
            $beforeCoin = $parentInfo->coin_money;
            $parentInfo->coin_money = bcadd($parentInfo->coin_money, $awardNum, 4);
            $parentInfo->freeze_coin = bcadd($parentInfo->freeze_coin, $awardNum, 4);
            $parentInfo->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $parentInfo->id,
                'amount_type' => 2,
                'type' => 6,
                'before' => $beforeCoin,
                'total' => $awardNum,
                'after' => $parentInfo->coin_money,
                'remark' => $content,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            $lastLevel = $parentInfo->level;     //上一次的等级
            $lastAward = $awardNum;     //上一次的奖金
        }

        return true;

    }
}
