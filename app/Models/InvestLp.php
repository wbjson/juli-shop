<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class InvestLp extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest_lp';


    public function mainCoinDb(){
        return $this->hasOne(MainCurrency::class,'id','main_coin');
    }

    public function otherCoinDb(){
        return $this->hasOne(MainCurrency::class,'id','other_coin');
    }

}
