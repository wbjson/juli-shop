<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class CardAuction extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'card_auction';


    public function source(){
        return $this->hasOne(User::class,'id','source_id');
    }
}
