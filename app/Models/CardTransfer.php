<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class CardTransfer extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'card_transfer';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function other(){
        return $this->hasOne(User::class,'id','other_id');
    }

    public function card(){
        return $this->hasOne(UsersCard::class,'id','users_card_id');
    }
}
