<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class AllPerformanceRate extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'all_performance_rate';


    public static function getRate(){
        $total_power = User::query()->sum('total_power');
        $rate = self::query()->where('min_performance','<=',$total_power)->where('max_performance','>',$total_power)->first();
        if (empty($rate)){
            return 100;
        }
        $rate = bcadd(bcmul(floor(($total_power-$rate->min_performance)/100000),$rate->rate,4),$rate->basic);
        return $rate;
    }

}
