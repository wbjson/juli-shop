<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class LpMiningPool extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'lp_mining_pool';

    public function currency1(){
        return $this->hasOne(MainCurrency::class,'id','coin1');
    }

    public function currency2(){
        return $this->hasOne(MainCurrency::class,'id','coin2');
    }

}
