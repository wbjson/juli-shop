<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class QuickCardUseLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'quick_card_use_log';


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
