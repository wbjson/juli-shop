<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Dcat\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProductsCategory extends Model
{
    use HasFactory, Notifiable ,HasDateTimeFormatter, ModelTree;

    protected $appends = ['icon_path','pictures_path'];

    protected $table = 'products_category';


    // 父级ID字段名称，默认值为 parent_id
    protected $parentColumn = 'parent_id';

    // 排序字段名称，默认值为 order
    protected $orderColumn = 'sort';

    // 标题字段名称，默认值为 title
    protected $titleColumn = 'name';

    public function parent(){
        return $this->hasOne(self::class,'id','parent_id');
    }

    public function getIconPathAttribute(){
        return assertUrl(Image::query()->where('id',$this->icon)->value('path'),'admin');
    }

    public function getPicturesPathAttribute(){
        return Image::query()->whereIn('id',explode(',',$this->picture))->pluck('path')->toArray();
    }
}
