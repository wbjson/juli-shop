<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class DynamicRate extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'dynamic_rate';


    public static function getDynamicRate($total_static_power){
        if (empty($total_static_power)){
            return null;
        }
        $all = self::query()->orderByDesc('id')->get()->toArray();
        foreach ($all as $item){
            if ($item['min_pledge'] < $total_static_power && $total_static_power<=$item['pledge_num']){
                return $item['dynamic_rate'];
            }
        }
        return null;
    }

}
