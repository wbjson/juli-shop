<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	use HasDateTimeFormatter;

    protected $appends = ['main_picture_path','pictures_path'];


    public function shop(){
        return $this->hasOne(ShopApplication::class,'user_id','shop_id')->where('audit_status',1);
    }

    public function category(){
        return $this->hasOne(ProductsCategory::class,'id','category_id');
    }

    public function getMainPicturePathAttribute(){
        return assertUrl(Image::query()->where('id',$this->main_picture)->value('path'),'admin');
    }

    public function getPicturesPathAttribute(){
        return Image::query()->whereIn('id',explode(',',$this->picture))->pluck('path')->toArray();
    }

    public function attr(){
        return $this->hasMany(ProductsAttr::class,'product_id','id');
    }

}
