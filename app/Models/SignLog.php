<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class SignLog extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'sign_log';
    
}
