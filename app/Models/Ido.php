<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Ido extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'ido';


    public function coin(){
        return $this->hasOne(MainCurrency::class,'id','coin_id');
    }

    public function freedCoin(){
        return $this->hasOne(MainCurrency::class,'id','freed_coin_id');
    }

}
