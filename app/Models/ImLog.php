<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class ImLog extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'im_log';
    
}
