<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class InvestPledge extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest_pledge';


    public function coin(){
        return $this->hasOne(MainCurrency::class,'id','coin_id');
    }

}
