<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class InvestPledgeLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest_pledge_log';


    public function invest(){
        return $this->hasOne(InvestPledge::class,'id','invest_id');
    }


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
