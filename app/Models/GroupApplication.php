<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class GroupApplication extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'group_application';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function group(){
        return $this->hasOne(GroupCategory::class,'id','group_id');
    }
}
