<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class MainCurrency extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'main_currency';

    /**
     * 获取BHB兑U汇率
     * @return mixed
     */
    public static function getBhbRate(){
        return self::query()->where('name','SCC')->value('rate');
    }

    /**
     * 根据方案和类型
     * @param $invest
     * @param $num
     * @param int $p_type
     * @return array
     */
    public static function getPledgePowerByInvest($invest,$num,$p_type){
        $otherRate = $invest->otherCoinDb->rate;
        $coin = $invest->mainCoinDb;
        if ($p_type==1){
            $power = bcdiv(bcmul($num,$coin->rate),0.7);
            $acPower = bcmul($power,$invest->seven_rate/100);
            $bhbNum = bcdiv(bcmul($power,0.3),$otherRate);
        }elseif ($p_type==2){
            $power = bcdiv(bcmul($num,$coin->rate),0.6);
            $acPower = bcmul($power,$invest->six_rate/100);
            $bhbNum = bcdiv(bcmul($power,0.4),$otherRate);
        }elseif ($p_type==3){
            $power = bcdiv(bcmul($num,$coin->rate),0.5);
            $acPower = bcmul($power,$invest->five_rate/100,9);
            $bhbNum = bcdiv(bcmul($power,0.5),$otherRate,9);
        }else{
            $power = bcmul($otherRate,$num);
            $acPower = bcmul($power,$invest->zero_rate/100,9);
            $bhbNum = $num;
        }
        return compact('bhbNum','power','acPower');
    }


    public static function getDestroyPowerByInvest($invest,$num){
        $bhbRate = $invest->coin->rate;
        $power = bcmul($bhbRate,$num);
        $acPower = bcmul($power,$invest->rate/100,9);
        $bhbNum = $num;
        return compact('bhbNum','power','acPower');
    }


    public static function getLpPowerByInvest($invest,$num){
        $bhbPrice = self::query()->where('name','LP')->value('rate');
        $client = new Client();
        $response = $client->post('http://127.0.0.1:9090/api/wallet/pro/getSwapInfo',[
            'form_params' => [
                'mainChain' => 'BNB',
                'contractAddress' => MainCurrency::query()->where('name','LP')->value('contract_address'),
            ]
        ]);
        $lpResponse = json_decode($response->getBody()->getContents(),true);

        $a= number_format($lpResponse['obj']['reserve1']/$lpResponse['obj']['totalSupply'], 9, '.', '');
        $b = bcmul($num,2,9);
        $power = bcdiv(bcmul($b,$a,9),$bhbPrice,9);
        $acPower = bcmul($power,$invest->rate/100,9);
        $bhbNum = $num;
        return compact('bhbNum','power','acPower');
    }

}
