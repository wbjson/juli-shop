<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class WageLog extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'wage_log';
    
}
