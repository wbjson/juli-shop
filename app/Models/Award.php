<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    use HasDateTimeFormatter;

    protected $table = 'income_log';

    /**
     * @param $uid
     * @param $num
     * @return void
     * 推广收益
     */
    public static function share_award($uid, $num, $oid)
    {
        $userInfo = User::query()->where('id', $uid)->first();
        if ($userInfo->parent_id < 0) {
            return true;
        }
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }
        $useridArr = User::query()
            ->whereIn('id', $path)
            ->orderByDesc('id')
            ->limit(2)
            ->pluck('id');
        if ($useridArr->isEmpty()) {
            return true;
        }

        $share_one = OrdersDetail::query()->where('order_id', $oid)->sum('share_num');
        $share_two = OrdersDetail::query()->where('order_id', $oid)->sum('indirect_num');

        $awardList = [
            1 => $share_one,
            2 => $share_two,
        ];

        foreach ($useridArr as $v) {
            $parentInfo = User::query()->where('id', $v)->first();
            if (empty($parentInfo)) {
                continue;
            }
            if ($parentInfo->activate != 1) {
                continue;
            }
            $deepDiff = ($userInfo->deep) - ($parentInfo->deep);
            $awardNum = $awardList[$deepDiff];
            if ($awardNum <= 0) {
                continue;
            }
            $beforeAmount = $parentInfo->sign_money;
            $parentInfo->sign_money = bcadd($parentInfo->sign_money, $awardNum, 2);
            $parentInfo->dynamic_income = bcadd($parentInfo->dynamic_income, $awardNum, 2);
            $parentInfo->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $parentInfo->id,
                'from_id' => $uid,
                'amount_type' => 5,
                'type' => 7,
                'before' => $beforeAmount,
                'total' => $awardNum,
                'after' => $parentInfo->sign_money,
                'remark' => '第' . $deepDiff . '代佣金' . $awardNum,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return true;
    }

    /**
     * @param $uid
     * @param $num
     * @return void
     * 推广收益
     */
    public static function point_share_award($uid, $num)
    {
        $userInfo = User::query()->where('id', $uid)->first();
        if ($userInfo->parent_id < 0) {
            return true;
        }

        $awardNum = bcmul($num, config('share_rate'), 2) / 100;       //推广收益
        $parentInfo = User::query()->where('id', $userInfo->parent_id)->first();
        if (empty($parentInfo)) {
            return true;
        }

        $beforeAmount = $parentInfo->amount;
        $parentInfo->amount = bcadd($parentInfo->amount, $awardNum, 2);
        $parentInfo->coin_money = bcsub($parentInfo->coin_money, $awardNum, 2);
        $parentInfo->share_income = bcadd($parentInfo->share_income, $awardNum, 2);
        $parentInfo->total_income = bcadd($parentInfo->total_income, $awardNum, 2);
        $parentInfo->save();

        IncomeLog::query()->insertGetId([
            'user_id' => $parentInfo->id,
            'amount_type' => 1,
            'type' => 3,
            'before' => $beforeAmount,
            'total' => $awardNum,
            'after' => $parentInfo->amount,
            'remark' => '推荐收益' . $awardNum,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return true;
    }

    /**
     * @param $uid
     * @param $num
     * @return void
     * 股东收益
     */
    public static function share_freeze_award($uid, $num)
    {
        $userInfo = User::query()->where('id', $uid)->first();
        if ($userInfo->parent_id < 0) {
            return true;
        }
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }

        $awardNum = bcmul($num, config('hold_award_rate'), 2) / 100;       //股东收益
        $maxLevel = LevelConfig::query()->max('level');

        $parentInfo = User::query()
            ->whereIn('id', $path)
            ->orderByDesc('id')
            ->where('level', $maxLevel)
            ->first();
        if (empty($parentInfo)) {
            return true;
        }

        $beforeAmount = $parentInfo->freeze_coin;
        $parentInfo->freeze_coin = bcadd($parentInfo->freeze_coin, $awardNum, 2);
        $parentInfo->save();

        IncomeLog::query()->insertGetId([
            'user_id' => $parentInfo->id,
            'from_id' => $uid,
            'amount_type' => 4,
            'type' => 5,
            'before' => $beforeAmount,
            'total' => $awardNum,
            'after' => $parentInfo->amount,
            'remark' => '股东收益冻结积分' . $awardNum,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return true;
    }

    /**
     * @return void
     * 等级收益
     */
    public static function level_award($uid, $num)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'deep', 'path')
            ->first();
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }
        $useridArr = User::query()
            ->whereIn('id', $path)
            ->where('level', '>', 0)
            ->orderByDesc('id')
            ->pluck('id');
        if ($useridArr->isEmpty()) {
            return true;
        }

        $lastLevel = 0;
        $levelCount = 0;     //同一个等级出现的次数
        $lastAward = 0;     //上一个收益比例
        foreach ($useridArr as $v) {
            $parentInfo = User::query()->where('id', $v)
                ->select('id', 'name', 'path', 'deep', 'level', 'deep',
                    'activate', 'parent_id', 'amount', 'coin_money',
                    'dynamic_income', 'total_income', 'myperfor', 'teamperfor'
                )
                ->first();
            if ($parentInfo->level < $lastLevel) {
                continue;
            }

            $levelInfo = LevelConfig::query()->where('level', $parentInfo->level)->first();
            if ($lastLevel == $parentInfo->level) {
                if ($levelCount >= $levelInfo->same_level_count) {
                    continue;
                }
                $awardNum = bcmul($lastAward, $levelInfo->same_level_rate, 2) / 100;
                if ($awardNum < 0.001) {
                    continue;
                }
                $levelCount++;
            } elseif ($parentInfo->level > $lastLevel) {
                $lastRate = LevelConfig::query()->where('level', $lastLevel)->value('rate');
                $awardRate = bcsub($levelInfo->rate, $lastRate, 2);
                $awardNum = bcmul($num, $awardRate, 2) / 100;
                if ($awardNum < 0.001) {
                    continue;
                }
                $levelCount = 0;
            }
            if ($awardNum <= 0) {
                continue;
            }

            $beforeAmount = $parentInfo->amount;
            $parentInfo->amount = bcadd($parentInfo->amount, $awardNum, 2);
            $parentInfo->coin_money = bcsub($parentInfo->coin_money, $awardNum, 2);
            $parentInfo->dynamic_income = bcadd($parentInfo->dynamic_income, $awardNum, 2);
            $parentInfo->total_income = bcadd($parentInfo->total_income, $awardNum, 2);
            $parentInfo->save();

            $deepDiff = $userInfo->deep - $parentInfo->deep;

            IncomeLog::query()->insertGetId([
                'user_id' => $parentInfo->id,
                'amount_type' => 1,
                'type' => 4,
                'before' => $beforeAmount,
                'total' => $awardNum,
                'after' => $parentInfo->amount,
                'remark' => '下' . $deepDiff . '代下单' . $levelInfo->name . '收益' . $awardNum,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $lastLevel = $parentInfo->level;
            $lastAward = $awardNum;
        }
        return true;
    }

    /**
     * @return void
     * 等级收益
     */
    public static function point_level_award($uid, $num)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'deep', 'path')
            ->first();
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }
        $useridArr = User::query()
            ->whereIn('id', $path)
            ->where('level', '>', 0)
            ->orderByDesc('id')
            ->pluck('id');
        if ($useridArr->isEmpty()) {
            return true;
        }

        $lastLevel = 0;
        $levelCount = 0;     //同一个等级出现的次数
        $lastAward = 0;     //上一个收益比例
        foreach ($useridArr as $v) {
            $parentInfo = User::query()->where('id', $v)
                ->select('id', 'name', 'path', 'deep', 'level', 'deep',
                    'activate', 'parent_id', 'amount', 'coin_money',
                    'dynamic_income', 'total_income', 'myperfor', 'teamperfor'
                )
                ->first();
            if ($parentInfo->level < $lastLevel) {
                continue;
            }

            $levelInfo = LevelConfig::query()->where('level', $parentInfo->level)->first();
            if ($lastLevel == $parentInfo->level) {
                if ($levelCount >= $levelInfo->same_level_count) {
                    continue;
                }
                $awardNum = bcmul($lastAward, $levelInfo->coin_level_rate, 2) / 100;
                if ($awardNum < 0.001) {
                    continue;
                }
                $levelCount++;
            } elseif ($parentInfo->level > $lastLevel) {
                $lastRate = LevelConfig::query()->where('level', $lastLevel)->value('coin_rate');
                $awardRate = bcsub($levelInfo->coin_rate, $lastRate, 2);
                $awardNum = bcmul($num, $awardRate, 2) / 100;
                if ($awardNum < 0.001) {
                    continue;
                }
                $levelCount = 0;
            }
            if ($awardNum <= 0) {
                continue;
            }

            $beforeAmount = $parentInfo->amount;
            $parentInfo->amount = bcadd($parentInfo->amount, $awardNum, 2);
            $parentInfo->coin_money = bcsub($parentInfo->coin_money, $awardNum, 2);
            $parentInfo->dynamic_income = bcadd($parentInfo->dynamic_income, $awardNum, 2);
            $parentInfo->total_income = bcadd($parentInfo->total_income, $awardNum, 2);
            $parentInfo->save();

            $deepDiff = $userInfo->deep - $parentInfo->deep;

            IncomeLog::query()->insertGetId([
                'user_id' => $parentInfo->id,
                'amount_type' => 1,
                'type' => 4,
                'before' => $beforeAmount,
                'total' => $awardNum,
                'after' => $parentInfo->amount,
                'remark' => '下' . $deepDiff . '代下单' . $levelInfo->name . '收益' . $awardNum,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $lastLevel = $parentInfo->level;
            $lastAward = $awardNum;
        }
        return true;
    }

    /**
     * @param $uid
     * @param $num
     * @return void
     * 股东收益 进入股东金
     */
    protected function share_hold_award($uid, $num)
    {
        $userInfo = User::query()->where('id', $uid)->first();
        if ($userInfo->parent_id < 0) {
            return true;
        }
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }
        $maxLevel = LevelConfig::query()->max('level');
        if($userInfo->level >= $maxLevel){
            /*股东报单*/
            $parentInfo = User::query()->whereIn('id', $path)
                ->where('level', $maxLevel)
                ->orderByDesc('id')
                ->limit(2)
                ->get();
        }else{
            /*会员报单*/
            $latestHold = User::query()
                ->whereIn('id', $path)
                ->where('level', $maxLevel)
                ->orderByDesc('id')
                ->value('id');
            if ($latestHold <= 0) {
                return true;
            }

            /*父级信息*/
            $parentInfo = User::query()->whereIn('id', $path)
                ->where('level', $maxLevel)
                ->where('id', '<', $latestHold)
                ->orderByDesc('id')
                ->limit(2)
                ->get();
        }

        if ($parentInfo->isEmpty()) {
            return true;
        }

        $awardList = [
            0 => config('holder_one'),
            1 => config('holder_two'),
        ];
        foreach ($parentInfo as $k => $v) {
            $awardRate = $awardList[$k];
            $awardNum = bcmul($num, $awardRate, 2) / 100;
            if ($awardNum <= 0) {
                continue;
            }

            $beforeHold = $v->holder_money;
            $afterHold = bcadd($beforeHold, $awardNum, 2);
            User::query()->where('id', $v->id)->update(['holder_money' => $afterHold]);

            $deepDiff = ($userInfo->deep) - ($v->deep);

            IncomeLog::query()->insertGetId([
                'user_id' => $v->id,
                'from_id' => $uid,
                'amount_type' => 3,
                'type' => 8,
                'before' => $beforeHold,
                'total' => $awardNum,
                'after' => $afterHold,
                'remark' => '下' . $deepDiff . '代下单' . $num,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return true;
    }
}
