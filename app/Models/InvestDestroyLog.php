<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class InvestDestroyLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest_destroy_log';


    public function invest(){
        return $this->hasOne(InvestDestroy::class,'id','invest_id');
    }


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function card(){
        return $this->hasOne(UsersCard::class,'id','user_card_id');
    }

}
