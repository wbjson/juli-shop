<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class SpeedList extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'speed_list';
    
}
