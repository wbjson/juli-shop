<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class ProductsAttr extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'products_attr';

    public $fillable = ['product_id','name','value'];

    public function store(){
        return $this->belongsTo(Product::class,'id','product_id');
    }
}
