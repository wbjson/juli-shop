<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class LoseSign extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'lose_sign';
    
}
