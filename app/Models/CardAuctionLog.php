<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class CardAuctionLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'card_auction_log';

    public function auction(){
        return $this->hasOne(CardAuction::class,'id','auction_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
