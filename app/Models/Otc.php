<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Otc extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'otc';


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
