<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest';

    public function coin(){
        return $this->hasOne(MainCurrency::class,'id','coin_id');
    }


    public function getIconAttribute($attribute){
        return assertUrl($attribute);
    }
}
