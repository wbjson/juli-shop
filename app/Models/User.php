<?php

namespace App\Models;

use App\Jobs\test;
use App\Jobs\UpdateDynamicPowerJob;
use Dcat\Admin\Traits\HasDateTimeFormatter;
use Dcat\Admin\Traits\ModelTree;
use GuzzleHttp\Client;
use Hhxsv5\LaravelS\Swoole\Task\Task;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable ,HasDateTimeFormatter, ModelTree;

    protected $titleColumn = 'name';

    protected $parentColumn = 'parent_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname',
        'phone',
        'name',
        'avatar',
        'level',
        'deep',
        'code',
        'parent_id',
        'path',
        'regip',
        'password',
        'trade_password',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'trade_password',
    ];

    public function group(){
        return $this->hasOne(LevelConfig::class,'level','level');
    }

    public function parent(){
        return $this->hasOne(self::class,'id','parent_id');
    }


    public function parentinfo(){
        return $this->hasOne(User::class,'id','parent_id');
    }


    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model){
            //生成邀请码
            while (true){
                $code = getRandStr(8);
                if (!self::query()->where('code',$code)->exists()){
                    break;
                }
            }
            $model->code = $code;
        });

        static::created(function ($model){
            //更新直推 团队
            $pUser = explode('-',trim($model->path,'-'));
            $pUserId = $pUser[count($pUser)-1];
            //给上级直推人数加1 ，以及整个链条上的所有人团队人数+1
            self::query()->where('id',$pUserId)->increment('zhi_num');
            self::query()->whereIn('id',$pUser)->increment('group_num');
            $model->save();
        });
    }


    public function getJWTIdentifier()
    {
        // TODO: Implement getJWTIdentifier() method.
        return $this->getKey();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = password_hash($value,PASSWORD_DEFAULT);
    }

    public function setTradePasswordAttribute($value)
    {
        $this->attributes['trade_password'] = password_hash($value,PASSWORD_DEFAULT);
    }

    public function getJWTCustomClaims()
    {
        // TODO: Implement getJWTCustomClaims() method.
        return [];
    }



}
