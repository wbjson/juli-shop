<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;

class LevelConfig extends Model
{
    use HasDateTimeFormatter;

    protected $table = 'level_config';

    public static function upgrade_level($uid, $usdtNum)
    {
        $userInfo = User::query()
            ->where('id', $uid)
            ->select('id', 'deep', 'path', 'level')
            ->first();
        $path = explode('-', trim($userInfo->path, '-'));
        if (empty($path)) {
            return true;
        }

        $maxLevel = LevelConfig::query()->max('level');
        if ($userInfo->level >= $maxLevel) {
//            User::query()->where('id', $uid)->increment('teamperfor', $usdtNum);       //添加团队业绩
        } else {
            /*找到最接近的那个股东*/
            $vipUser = User::query()
                ->whereIn('id', $path)
                ->where('level', '>=', $maxLevel)
                ->orderByDesc('id')
                ->value('id');
            if($vipUser <= 0){
                User::query()->whereIn('id', $path)->increment('teamperfor', $usdtNum);       //添加团队业绩
            }else{
                $getPath = User::query()
                    ->whereIn('id', $path)
                    ->where('id','>=',$vipUser)
                    ->orderByDesc('id')
                    ->pluck('id');
                if(!$getPath->isEmpty()){
                    User::query()->whereIn('id', $getPath)->increment('teamperfor', $usdtNum);       //添加团队业绩
                }else{
                    User::query()->whereIn('id', $path)->increment('teamperfor', $usdtNum);       //添加团队业绩
                }
            }
        }

//
//        $maxLevel = LevelConfig::query()->max('level');
//        $userPath = User::query()
//            ->whereIn('id', $path)
//            ->where('level', '<', $maxLevel)
//            ->orderByDesc('id')
//            ->pluck('id');
//        if (empty($userPath)) {
//            return true;
//        }
//
//        foreach ($userPath as $v) {
//            $parentInfo = User::query()->where('id', $v)->first();
//
//            $directUser = User::query()
//                ->where('parent_id', $v)
//                ->where('activate', 1)
//                ->where('level', '>=', $parentInfo->level - 1)
//                ->count();
//            $getLevel = LevelConfig::query()
//                ->where('team_vip', '<=', $directUser)
//                ->where('teamperfor', '<=', $parentInfo->teamperfor)
//                ->orderByDesc('level')
//                ->value('level');
//            if ($getLevel <= $parentInfo->level) {
//                continue;
//            }
//            $parentInfo->level = $getLevel;
//            $parentInfo->save();
//        }
        return true;
    }


    public static function getAllLevel()
    {
        $all = self::query()->get();
        $data = collect($all)->reduce(function ($lookup, $item) {
            $lookup[$item["id"]] = $item;
            return $lookup;
        });
        return collect($data)->toArray();
    }


}
