<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'box';

    public function currency(){
        return $this->hasOne(MainCurrency::class,'id','currency_id');
    }

}
