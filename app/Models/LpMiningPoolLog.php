<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class LpMiningPoolLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'lp_mining_pool_log';

    public function invest(){
        return $this->hasOne(LpMiningPool::class,'id','pool_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
