<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlatPool extends Model
{
    use HasDateTimeFormatter;


    protected $table = 'plat_pool';
}
