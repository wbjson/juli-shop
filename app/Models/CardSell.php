<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class CardSell extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'card_sell';


    public function card(){
        return $this->hasOne(UsersCard::class,'id','user_card_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function other(){
        return $this->hasOne(User::class,'id','other_id');
    }
}
