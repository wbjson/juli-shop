<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class InvestDestroy extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'invest_destroy';

    public function coin1(){
        return $this->hasOne(MainCurrency::class,'id','coin_id');
    }

    public function coin2(){
        return $this->hasOne(MainCurrency::class,'id','coin2_id');
    }

}
