<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasDateTimeFormatter;


    public function shopping()
    {
        return $this->hasOne(OrdersShopping::class, 'id', 'shopping_id');
    }

    public function getaddress()
    {
        return $this->hasOne(UsersAddress::class, 'id', 'shopping_id');
    }

    public function detaillist()
    {
        return $this->hasMany(OrdersDetail::class, 'order_id', 'id');
    }

    public function userinfo()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return void
     * 递归减少
     */
    public static function reducePoint($uid, $num)
    {
        $minOrder = ReleaseOrder::query()
            ->where('user_id', $uid)
            ->where('status', 1)
            ->orderByRaw('id')
            ->first();
        if ($num > $minOrder->reset_num) {
            $resetPoint = bcsub($num, $minOrder->reset_num, 2);      //剩余还需要消耗数量

            ReleaseOrder::query()
                ->where('id', $minOrder->id)
                ->where('user_id', $uid)
                ->where('status', 1)
                ->update(['status' => 2, 'finshed_at' => date('Y-m-d H:i:s')]);
            self::reducePoint($uid,$resetPoint);

        } elseif ($num == $minOrder->reset_num) {
            ReleaseOrder::query()
                ->where('id', $minOrder->id)
                ->where('user_id', $uid)
                ->where('status', 1)
                ->update(['status' => 2, 'finshed_at' => date('Y-m-d H:i:s')]);
        } else {
            $resetNum = bcsub($minOrder->reset_num, $num, 2);      //剩余还需要释放数量;
            ReleaseOrder::query()
                ->where('id', $minOrder->id)
                ->where('user_id', $uid)
                ->where('status', 1)
                ->update(['reset_num' => $resetNum]);
        }
        return true;
    }
}
