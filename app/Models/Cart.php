<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'cart';

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
