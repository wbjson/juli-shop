<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	use HasDateTimeFormatter;

    protected $fillable = ['user_id','path'];

    /**
     * 路径获取器
     * @param $value
     * @return mixed|string
     */
    public function getPathAttribute($value){
        return assertUrl($value,'admin');
    }


}
