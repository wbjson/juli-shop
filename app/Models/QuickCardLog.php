<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class QuickCardLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'quick_card_log';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }


}
