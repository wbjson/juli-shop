<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class PoolLog extends Model
{
	use HasDateTimeFormatter;


    protected $table = 'pool_log';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public static function insertLog($userId,$poolId,$amount,$type){
        PoolLog::query()->insert([
            'user_id' => $userId,
            'pool_id' => $poolId,
            'amount' => $amount,
            'type' => $type,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        Pool::query()->where('id',$poolId)->increment('amount',$amount);
    }

}
