<?php

namespace App\Models;

use App\Jobs\test;
use App\Jobs\UpdateDynamicPowerJob;
use Dcat\Admin\Traits\HasDateTimeFormatter;

use GuzzleHttp\Client;
use Hhxsv5\LaravelS\Swoole\Task\Task;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IncomeLog extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'income_log';

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
