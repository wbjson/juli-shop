<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class OrdersDetail extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'orders_detail';

    public $appends = ['product_img_path'];

    public function getProductImgPathAttribute(){
        return assertUrl(Image::query()->where('id',$this->product_img)->value('path'),'admin');
    }

    public function OrdersDetail(){
        return $this->hasMany(Order::class,'id','order_id');
    }

}
