<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class BankList extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'bank_list';
    
}
