<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'users_address';


    protected $fillable = ['name', 'phone', 'province', 'city','district', 'address', 'is_default', 'user_id'];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function format()
    {
        return $this->province . $this->city. $this->district. $this->address;
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
