<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Validate\Recharge\RechargeCheckValidate;
use App\Http\Validate\Recharge\RechargeListValidate;
use App\Models\Invest;
use App\Models\InvestDestroy;
use App\Models\InvestDestroyLog;
use App\Models\InvestLp;
use App\Models\InvestLpLog;
use App\Models\InvestPledge;
use App\Models\InvestPledgeLog;
use App\Models\MainCurrency;
use App\Models\Pledge;
use App\Models\Recharge;
use Illuminate\Support\Facades\Cache;

class RechargeController extends Controller
{


    public function list(RechargeListValidate $request){
        $validated = $request->validated();
        if (!isset($validated['page']) || empty($validated['page'])){
            $validated['page'] = 1;
        }
        if (!isset($validated['size']) || empty($validated['size'])){
            $validated['size'] = 10;
        }

        $query = Recharge::query()->where('user_id',auth()->id())->where('type',6)
            ->select(['coin','nums','status','created_at']);

        $total = $query->count();
        $list = $query->offset(($validated['page']-1)*$validated['size'])
            ->limit($validated['size'])
            ->orderBy('id','desc')
            ->get();

        return responseJson(compact('total','list'));
    }

    /**
     * 检查是否可购买
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(RechargeCheckValidate $request){
        $validated = $request->validated();
        if ($validated['type']==1){
            $invest = InvestPledge::query()->where('id',$validated['invest_id'])->first();
        }elseif ($validated['type']==2){
            $invest = InvestDestroy::query()->where('id',$validated['invest_id'])->first();
        }elseif ($validated['type']==3){
            $invest = InvestLp::query()->where('id',$validated['invest_id'])->first();
        }
        if (empty($invest)){
            return responseValidateError('非法请求');
        }

        if ($validated['type']==1){
            $result = MainCurrency::getPledgePowerByInvest($invest,$validated['num'],$validated['p_type']);
        }elseif ($validated['type']==2){
            $result = MainCurrency::getDestroyPowerByInvest($invest,$validated['num']);
        }else{
            $result = MainCurrency::getLpPowerByInvest($invest,$validated['num']);
        }


        $power = $result['power'];
        $acPower = $result['acPower'];
        $bhbNum = $result['bhbNum'];
        $handlingFee = 0;

        $todayPledgePower = InvestPledgeLog::query()->whereDate('created_at',date('Y-m-d'))->sum('power');
        $todayDestroyPower = InvestDestroyLog::query()->whereDate('created_at',date('Y-m-d'))->sum('power');
        $todayLpPower = InvestLpLog::query()->whereDate('created_at',date('Y-m-d'))->sum('power');

        //判断今日已购买算力
        if ($todayPledgePower+$todayDestroyPower+$todayLpPower+$power > config('limit_total_pledge')){
            return responseValidateError('今日全网算力已售罄');
        }
        if ($validated['type']==1){
            if ($validated['p_type']==1){
                $limitPower = config('limit_main_one');
            }elseif ($validated['p_type']==2){
                $limitPower = config('limit_main_two');
            }elseif ($validated['p_type']==3){
                $limitPower = config('limit_main_three');
            }else{
                $limitPower = config('limit_main_all');
            }

            if ($todayPledgePower+$power > $limitPower){
                return responseValidateError('今日质押矿池算力已售罄');
            }
            $handlingFee = bcmul($validated['num'],config('pledge_main_fee')/100);
        }elseif ($validated['type']==2){
            $limitPower = config('limit_destory_pledge');
            if ($todayDestroyPower+$power > $limitPower){
                return responseValidateError('今日销毁矿池算力已售罄');
            }
        }else{
            $limitPower = config('limit_lp_pledge');
            if ($todayLpPower+$power > $limitPower){
                return responseValidateError('今日销毁矿池算力已售罄');
            }
        }
        return responseJson(compact('bhbNum','handlingFee','power','acPower'));
    }

}
