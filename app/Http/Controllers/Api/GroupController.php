<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Group\ApplyValidate;
use App\Http\Validate\Group\GroupApplicationListValidate;
use App\Http\Validate\Group\GroupApplicationDetailValidate;
use App\Models\GroupApplication;
use App\Models\GroupCategory;

class GroupController extends Controller
{

    /**
     * 申请记录列表
     * @param GroupApplicationListValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyList(GroupApplicationListValidate $request){
        $validated = $request->validated();
        $query = GroupApplication::query()->with(['group'=>function($query){
            $query->select(['id','name']);
        }])->where('user_id',auth()->id())->select(['id','status','group_id','country','province','city','district','address','created_at']);
        $total = $query->count();
        $list = $query->offset(($validated['page']-1)*$validated['size'])
            ->limit($validated['size'])
            ->orderBy('id','desc')
            ->get();
        return responseJson(compact('total','list'));
    }

    /**
     * 申请记录详情
     * @param GroupApplicationDetailValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyDetail(GroupApplicationDetailValidate $request){
        $validated = $request->validated();
        $groupApplication = GroupApplication::query()->with(['group'=>function($query){
            $query->select(['id','name']);
        }])->where(['user_id'=>auth()->id(),'id'=>$validated['id']])->first();
        if (empty($groupApplication)){
            return responseValidateError('未找到该申请记录');
        }
        return responseJson($groupApplication);
    }

    /**
     * 申请成为社区
     * @param ApplyValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apply(ApplyValidate $request){
        $validated = $request->validated();
        $user = auth()->user();
        if ($user->is_group > 0){
            return responseValidateError('已成为社区,无法再申请');
        }
        //检查等级是否达到要求
        $groupCategory = GroupCategory::query()->where('id',$validated['group_id'])->first();
        if (empty($groupCategory)){
            return responseValidateError('所选社区类型不合法');
        }
        if (!in_array($user->level_id,explode(',',$groupCategory->grade_factor)) || $user->lp_num < $groupCategory->lp_factor){
            return responseValidateError('未达到可申请条件');
        }
        //判断是否有未审核的
        if (GroupApplication::query()->where('user_id',$user->id)->where('status',0)->count() > 0){
            return responseValidateError('有待审核的申请,请稍后');
        }
        $validated['user_id'] = $user->id;
        $validated['created_at'] = date('Y-m-d H:i:s');
        GroupApplication::query()->insert($validated);
        return responseJson();
    }


}
