<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ImLog;
use App\Models\IncomeLog;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use function request;

class ImConnectController extends Controller
{
    //

    /**
     * @return void
     * 发送红包
     */
    public function imExpend()
    {
        $postData = request()->post();

        $key = $postData['key'];
        $im_uid = $postData['im_uid'];
        $money = $postData['money'];
        $remark = $postData['remark'];

        if ($key != 'm8bH2dbw3w') {
            return responseImError('私钥不正确');
        }
        if ($im_uid <= 0) {
            return responseImError('IMUID不正确');
        }
        if ($money <= 0) {
            return responseImError('金额不正确');
        }
        $remarkList = [
            '发送红包'=>1,
            '收入红包'=>2,
            '红包退回'=>3,
        ];
        $type = $remarkList[$remark];


        $userInfo = User::query()->where('im_uid', $im_uid)->first();
        if (empty($userInfo)) {
            return responseImError('未找到对应账号');
        }


        $lockCacheKey = 'imExpend' . $userInfo->id . date('Y-m-d H:i:s');
        $lockCacheRs = Redis::setnx($lockCacheKey, 1);
        if ($lockCacheRs != 1) {
            return responseValidateError('操作频繁');
        }

        if ($userInfo->amount < $money) {
            Redis::expire($lockCacheKey, 0);
            return responseImError('余额剩余' . $userInfo->amount.'发送失败');
        }

        DB::beginTransaction();
        try {
            $beforeAmount  = $userInfo->amount;
            $userInfo->amount = bcsub($userInfo->amount, $money, 2);
            $userInfo->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $userInfo->id,
                'amount_type' => 2,
                'type' => 5,
                'before' => $beforeAmount,
                'total' => -$money,
                'after' => $userInfo->amount,
                'remark' => $remark,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            ImLog::query()->insertGetId([
                'uid' => $userInfo->id,
                'im_uid' => $userInfo->im_uid,
                'money' => $money,
                'type' => $type,
                'remark' => $remark,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            Redis::expire($lockCacheKey, 0);
            return responseImSuc($remark . '成功');
        } catch (Exception $e) {
            Redis::expire($lockCacheKey, 0);
            DB::rollBack();
            return responseImError($remark . '失败');
        }
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * 领取红包
     */
    public function imIncome()
    {
        $postData = request()->post();

        $key = $postData['key'];
        $im_uid = $postData['im_uid'];
        $money = $postData['money'];
        $remark = $postData['remark'];

        if ($key != 'm8bH2dbw3w') {
            return responseImError('私钥不正确');
        }
        if ($im_uid <= 0) {
            return responseImError('IMUID不正确');
        }
        if ($money <= 0) {
            return responseImError('金额不正确');
        }
        $remarkList = [
            '发送红包'=>1,
            '收入红包'=>2,
            '红包退回'=>3,
            '退回红包'=>3,
        ];
        $type = $remarkList[$remark];

        $userInfo = User::query()->where('im_uid', $im_uid)->first();
        if (empty($userInfo)) {
            return responseImError('未找到对应账号');
        }

        $lockCacheKey = 'imIncome' . $userInfo->id . date('Y-m-d H:i:s');
        $lockCacheRs = Redis::setnx($lockCacheKey, 1);
        if ($lockCacheRs != 1) {
            return responseValidateError('操作频繁');
        }

        if($type == 2){
            $incomeType = 6;
        }else{
            $incomeType = 7;
        }


        DB::beginTransaction();
        try {
            $beforeAmount  = $userInfo->amount;
            $userInfo->amount = bcadd($userInfo->amount, $money, 2);
            $userInfo->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $userInfo->id,
                'amount_type' => 2,
                'type' => $incomeType,
                'before' => $beforeAmount,
                'total' => $money,
                'after' => $userInfo->amount,
                'remark' => $remark,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            ImLog::query()->insertGetId([
                'uid' => $userInfo->id,
                'im_uid' => $userInfo->im_uid,
                'money' => -$money,
                'type' => $type,
                'remark' => $remark,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            Redis::expire($lockCacheKey, 0);
            return responseImSuc($remark . '成功');
        } catch (Exception $e) {
            Redis::expire($lockCacheKey, 0);
            DB::rollBack();
            return responseImError($remark . '失败');
        }
    }
}
