<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\IncomeLog;
use App\Models\OnlineLog;
use App\Models\SignLog;
use App\Models\User;
use App\Models\WageLog;
use App\Models\WageMoney;

class ProfitController extends Controller
{
    /*领取在线收益*/
    public function onlineProfit()
    {
        $userId = auth()->id();
        $todaySign = OnlineLog::query()->where('user_id', $userId)->where('line_date', date('Ymd'))->exists();
        if ($todaySign) {
            return responseValidateError('今日任务已完成');
        }

        $signAward = config('online_profit');
        if($signAward <= 0){
            return responseJson([], 200, '领取完成');
        }
        $userInfo = User::query()->where('id', $userId)->first();

        $onlineTime = config('online_time');
        /*是否满足时间*/
        $timeDiff = time()-$userInfo->line_date;
        if($timeDiff / 60 < $onlineTime){
            return responseValidateError('在线时长不足'.$onlineTime.'分钟');
        }


        $beforeAmount = $userInfo->coin_money;
        $userInfo->coin_money = bcadd($userInfo->coin_money, $signAward, 2);
        $userInfo->save();

        IncomeLog::query()->insertGetId([
            'user_id' => $userId,
            'amount_type' => 2,
            'type' => 2,
            'before' => $beforeAmount,
            'total' => $signAward,
            'after' => $userInfo->coin_money,
            'remark' => '在线任务收益',
            'created_at' => date('Y-m-d H:i:s'),
        ]);


        OnlineLog::query()->insertGetId([
            'user_id'=>$userId,
            'num'=>$signAward,
            'line_date'=>date('Ymd'),
            'created_at'=>date('Y-m-d H:i:s'),
        ]);
        return responseJson([], 200, '任务完成');

    }

    /*领取在线收益*/
    public function wageMoney()
    {
        $userId = auth()->id();
        $lastDay = date('d', strtotime(date('Y-m-01', strtotime(date('Y-m-d'))). ' 1 month -1 day'));
        $nowDay = date('d');
        if($nowDay < $lastDay){
            return responseValidateError('每个月最后一天才能领取');
        }
        $todaySign = WageLog::query()->where('user_id', $userId)->where('wage_month', date('m'))->exists();
        if ($todaySign) {
            return responseValidateError('该月工资已领取');
        }
        $userInfo = User::query()->where('id', $userId)->first();

        $shareUser = User::query()->where('parent_id',$userInfo->id)->count();
        $signAward = WageMoney::query()->where('start','<=',$shareUser)->where('end','>=',$shareUser)->orderByDesc('profit')->value('profit');
        if($signAward <= 0){
            return responseJson([], 200, '暂无达成条件');
        }


        $beforeAmount = $userInfo->coin_money;
        $userInfo->coin_money = bcadd($userInfo->coin_money, $signAward, 2);
        $userInfo->save();

        IncomeLog::query()->insertGetId([
            'user_id' => $userId,
            'amount_type' => 2,
            'type' => 4,
            'before' => $beforeAmount,
            'total' => $signAward,
            'after' => $userInfo->coin_money,
            'remark' => '每月工资',
            'created_at' => date('Y-m-d H:i:s'),
        ]);


        WageLog::query()->insertGetId([
            'user_id'=>$userId,
            'num'=>$signAward,
            'wage_month'=>date('m'),
            'created_at'=>date('Y-m-d H:i:s'),
        ]);
        return responseJson([], 200, '任务完成');

    }
}
