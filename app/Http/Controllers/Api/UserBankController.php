<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\User\UserBankRequest;
use App\Models\BankList;
use App\Models\UserBank;

class UserBankController extends Controller
{
    //
    /**
     * @return void
     * 银行卡列表
     */
    public function bankList()
    {
        $bankList = BankList::query()
            ->where('status', 1)
            ->select(['id', 'bank_name', 'bank_code'])
            ->get();
        return responseJson($bankList);
    }


    /**
     * @return void
     * 添加银行卡
     */
    public function addBankCard(UserBankRequest $request)
    {
        $userId = auth()->id();
        $validated = $request->validated();

        $bankInfo = BankList::query()->where('id', $validated['bank_id'])->first();
        if (isset($validated['id']) && $validated['id'] > 0) {
            $userBank = UserBank::query()->where('id', $validated['id'])->first();
            if (empty($userBank)) {
                return responseValidateError('未找到用户银行卡信息');
            }
            if ($userBank->bank_id != $validated['bank_id']) {
                $userBank->bank_id = $validated['bank_id'];
                $userBank->bank_name = $bankInfo->bank_name;
                $userBank->bank_code = $bankInfo->bank_code;
            }
            if ($userBank->bank_user != $validated['bank_user']) {
                $userBank->bank_user = $validated['bank_user'];
            }
            if ($userBank->bank_no != $validated['bank_no']) {
                $userBank->bank_no = $validated['bank_no'];
            }
            if ($userBank->bank_phone != $validated['bank_phone']) {
                $userBank->bank_phone = $validated['bank_phone'];
            }
            $userBank->is_default = $validated['is_default'];
            $userBank->save();
        } else {
            $validated['user_id'] = $userId;
            $validated['bank_name'] = $bankInfo->bank_name;
            $validated['bank_code'] = $bankInfo->bank_code;
            UserBank::query()->insertGetId($validated);
        }

        return responseJson('操作成功');
    }


    /**
     * @return void
     * 用户银行卡信息
     */
    public function userBank()
    {
        $userId = auth()->id();
        $bankList = UserBank::query()
            ->where('user_id', $userId)
            ->get();

        return responseJson($bankList);
    }

    /**
     * @return void
     * 删除用户银行卡
     */
    public function delUserBank()
    {
        $bid = request()->post('bid', 0);
        $userId = auth()->id();

        $userBank = UserBank::query()->where('id', $bid)->where('user_id', $userId)->first();
        if (empty($userBank)) {
            return responseValidateError('暂未找到该银行卡信息');
        }

        $userBank->delete();
        return responseJson([],200,'删除成功');
    }
}
