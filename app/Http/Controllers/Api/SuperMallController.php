<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PlatPool;
use App\Models\Product;
use App\Models\ProductExhibit;
use App\Models\ProductsCategory;

class SuperMallController extends Controller
{
    //
    public function mall()
    {
        $columnList = ProductsCategory::query()
            ->where('status', 1)
            ->where('level', 1)
            ->orderByRaw('sortorder')
            ->select('id', 'name', 'icon', 'sortorder')
            ->limit(20)
            ->get();
        /*推荐商品*/
        $recordGoodsId = ProductExhibit::query()
            ->where('type', 2)
            ->where('status', 1)
            ->orderByRaw('orders')
            ->limit(20)
            ->pluck('product_id');

        $query_str = request()->post('query_name', '');
        $goodsList = Product::query()
            ->where('status', 1)
            ->where('type', 1)
            ->where(function ($query) use ($recordGoodsId) {
                if (!$recordGoodsId->isEmpty()) {
                    $query->whereIn('id', $recordGoodsId);
                }
            })
            ->where(function ($query) use ($query_str) {
                if (!empty($query_str)) {
                    $query->where('name', 'like', '%' . $query_str . '%');
                }
            })
            ->orderByRaw('sort')
            ->select(['id', 'name', 'sales', 'main_picture', 'price', 'sales'])
            ->limit(30)
            ->get();

        return responseJson(compact('columnList', 'goodsList'));
    }


    /**
     * @return void
     * 积分商城
     */
    public function coinMall()
    {
        $columnList = ProductsCategory::query()
            ->where('status', 1)
            ->where('level', 1)
            ->orderByRaw('sortorder')
            ->select('id', 'name', 'icon', 'sortorder')
            ->limit(20)
            ->get();        //栏目

        $type = request()->post('type', 1);
        $query_str = request()->post('query_name', '');
        $goodsList = Product::query()
            ->where('status', 1)
            ->where('type', $type)
            ->where(function ($query) use ($query_str) {
                if (!empty($query_str)) {
                    $query->where('name', 'like', '%' . $query_str . '%');
                }
            })
            ->orderByRaw('sort')
            ->select(['id', 'name', 'sales', 'main_picture', 'price', 'sales'])
            ->limit(30)
            ->get();
        return responseJson(compact('columnList', 'goodsList'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * 积分资金池数量
     */
    public function pointPool()
    {
        $pointPool = PlatPool::query()->where('id', 1)->value('point_pool');
        return responseJson($pointPool);
    }
}
