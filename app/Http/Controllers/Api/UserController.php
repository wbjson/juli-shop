<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Auth\IncomeValidate;
use App\Http\Validate\Product\ProductCollectValidate;
use App\Http\Validate\User\UserCertValidate;
use App\Models\BrowHistory;
use App\Models\IncomeLog;
use App\Models\LevelConfig;
use App\Models\Product;
use App\Models\SignLog;
use App\Models\User;
use App\Models\UserCert;
use App\Models\UserCollect;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserController extends Controller
{
    //
    public function userInfo()
    {
        $userId = auth()->id();
        $userInfo = User::query()
            ->where('id', $userId)
            ->select('id', 'nickname', 'phone', 'amount', 'zhi_num', 'level', 'group_num', 'code', 'myperfor', 'teamperfor')
            ->first();

        $userInfo->share_url = 'https://app.juliapp.org/#/pages/register/register' . $userInfo->code;
        $userInfo->level_name = LevelConfig::query()->where('level', $userInfo->level)->value('name');

        if (!file_exists(public_path('qrcodes/' . $userInfo->code . '-qrcode.png'))) {
            QrCode::format('png')->size(160)->margin(2)->generate($userInfo->share_url, public_path('qrcodes/' . $userInfo->code . '-qrcode.png'));
        }
        $userInfo->share_img = assertUrl($userInfo->code . '-qrcode.png', 'qrcodes');
        return responseJson($userInfo);
    }

    /**
     * @return void
     * 浏览记录
     */
    public function browHistory()
    {
        $dataList = BrowHistory::query()
            ->where('user_id', auth()->id())
            ->groupBy('datestr')
            ->orderByDesc('id')
            ->select('id', 'datestr')
            ->get();
        if (!$dataList->isEmpty()) {
            foreach ($dataList as $v) {
                $v->history_list = BrowHistory::query()
                    ->where('user_id', auth()->id())
                    ->where('datestr', $v->datestr)
                    ->select('id', 'product_id', 'product_img', 'created_at')
                    ->orderByDesc('id')
                    ->get();
            }
        }
        return responseJson($dataList);
    }

    /**
     * @return void
     * 清空浏览记录
     */
    public function deleteHistory()
    {
        BrowHistory::query()
            ->where('user_id', auth()->id())
            ->delete();
        return responseJson();
    }

    /**
     * @return void
     * 收藏商品
     */
    public function collectGood(ProductCollectValidate $request)
    {
        $validated = $request->validated();
        $userId = auth()->id();
        $productInfo = Product::query()->where('id', $validated['product_id'])->first();
        if (empty($productInfo)) {
            return responseValidateError('该商品不存在');
        }

        $setCollect = UserCollect::query()
            ->where('user_id', $userId)
            ->where('product_id', $validated['product_id'])
            ->count();
        if ($validated['type'] == 1) {
            if (empty($setCollect) || $setCollect <= 0) {
                UserCollect::query()->insertGetId([
                    'user_id' => $userId,
                    'product_id' => $validated['product_id'],
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            }
        } else {
            if (!empty($setCollect) || $setCollect > 0) {
                UserCollect::query()->where('user_id', $userId)->where('product_id', $validated['product_id'])->delete();
            }
        }
        return responseJson();
    }

    /**
     * @return void
     * 商品收藏列表
     */
    public function collectList(IncomeValidate $request)
    {
        $validated = $request->validated();
        $query = UserCollect::query()
            ->where('user_id', auth()->id())
            ->select('id', 'product_id');

        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->get();
        if (!$list->isEmpty()) {
            foreach ($list as $v) {
                $productInfo = Product::query()
                    ->where('id', $v->product_id)
                    ->select('id', 'name', 'main_picture', 'price')
                    ->first();
                $v->product_info = $productInfo;
            }
        }
        return responseJson(compact('total', 'list'));
    }


    public function incomeLog(IncomeValidate $request)
    {
        $type = request()->post('type',0);
        $validated = $request->validated();
        $query = IncomeLog::query()
            ->where('user_id', auth()->id())
            ->where(function ($query) use ($type) {
                if ($type > 0) {
                    $query->where('type', $type);
                }
            })
            ->select(['amount_type', 'total', 'type', 'created_at']);

        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->get();
        return responseJson(compact('total', 'list'));
    }

    /*资金类型*/
    public function typeList(){
        return responseJson([
            0 => '后台操作',
            5 => '发送红包',
            6 => '收入红包',
            7 => '退回红包',
            11 => '提现收益',
            12 => '提现退回',
        ]);
    }

    /**
     * @return void
     * 我的团队
     */
    public function userTeam(IncomeValidate $request)
    {
        $userId = auth()->id();
        $validated = $request->validated();

        $userInfo = User::query()
            ->where('id', auth()->id())
            ->select('nickname', 'phone', 'myperfor', 'teamperfor', 'created_at')
            ->first();

        $query = User::query()
            ->where('parent_id', auth()->id())
            ->select(['phone as name', 'myperfor', 'teamperfor', 'created_at']);

        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->get();
        return responseJson(compact('total', 'list', 'userInfo'));
    }

    /**
     * @return void
     * 认证详情
     */
    public function userCertInfo()
    {
        $userId = auth()->id();
        $certInfo = UserCert::query()->where('user_id', $userId)->first();
        return responseJson($certInfo);
    }

    /**
     * @return void
     * 实名认证
     */
    public function userCertVerified(UserCertValidate $request)
    {
        $validated = $request->validated();
        $userId = auth()->id();
        $userInfo = User::query()->where('id', $userId)->select('id', 'phone', 'cert_status')->first();
        if ($userInfo->cert_status == 1) {
            return responseValidateError('已完成实名认证');
        }
        if (!UserCert::isChinaIDCard($validated['idcard_no'])) {
            return responseValidateError('请输入正确的身份证号');
        }
        $certInfo = UserCert::query()->where('user_id', $userId)->first();
        if ((!empty($certInfo)) && $certInfo->status == 0) {
            return responseValidateError('等待审核通过');
        }

        if ((!empty($certInfo)) && $certInfo->status == 1) {
            return responseValidateError('实名已通过');
        }

        if (!empty($certInfo)) {
            UserCert::query()->where('user_id', $userId)->
            update([
                'real_name' => $validated['real_name'],
                'idcard_no' => $validated['idcard_no'],
                'status' => 0,
            ]);
        } else {
            UserCert::query()->insertGetId([
                'user_id' => $userId,
                'real_name' => $validated['real_name'],
                'idcard_no' => $validated['idcard_no'],
                'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }

        return responseJson('实名提交成功');
    }


    /*签到*/
    public function sign()
    {
        $userId = auth()->id();
        $todaySign = SignLog::query()->where('user_id', $userId)->where('sign_date', date('Ymd'))->exists();
        if ($todaySign) {
            return responseValidateError('今日已签到');
        }

        $signAward = config('sign_award');
        if($signAward <= 0){
            return responseJson([], 200, '签到完成');
        }
        $userInfo = User::query()->where('id', $userId)->first();


        $beforeAmount = $userInfo->coin_money;
        $userInfo->coin_money = bcadd($userInfo->coin_money, $signAward, 2);
        $userInfo->save();

        IncomeLog::query()->insertGetId([
            'user_id' => $userId,
            'amount_type' => 2,
            'type' => 1,
            'before' => $beforeAmount,
            'total' => $signAward,
            'after' => $userInfo->coin_money,
            'remark' => '签到收益',
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        SignLog::query()->insertGetId([
            'user_id'=>$userId,
            'num'=>$signAward,
            'sign_date'=>date('Ymd'),
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        return responseJson([], 200, '签到完成');
    }
}
