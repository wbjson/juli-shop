<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Recharge\PayOrderValidate;
use App\Models\CardLevelList;
use App\Models\IdoList;
use App\Models\IdoOrder;
use App\Models\JointList;
use App\Models\JointOrder;
use App\Models\KolList;
use App\Models\KolOrder;
use App\Models\Recharge;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class InvestController extends Controller
{
    //投资列表
    public function investList()
    {
        $idodList = IdoList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum', 'status')
            ->get();

        $kolList = KolList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum', 'status')
            ->first();
        $jointList = JointList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum', 'status')
            ->first();
        return responseJson(compact('idodList', 'kolList', 'jointList'));
    }

    /**
     * @return void
     * IDO列表
     */
    public function idoInfo()
    {
        $idoInfo = IdoList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum', 'start_at', 'end_at')
            ->first();

        $countInfo = [];

        $startTime = strtotime(date('Y-m-d' . $idoInfo->start_at, time()));
        $endTime = strtotime(date('Y-m-d' . $idoInfo->end_at, time()));
        if ($startTime < time() && $endTime > time()) {
            $countInfo['time_diff'] = $endTime - time();
        } else {
            $countInfo['time_diff'] = 0;
        }

        $idoUsdtNum = IdoOrder::query()->where('status', '<>', 2)->sum('usdtnum');
        $kolUsdtNum = KolOrder::query()->where('status', '<>', 2)->sum('usdtnum');
        $jointUsdtNum = JointOrder::query()->where('status', '<>', 2)->sum('usdtnum');

        $countInfo['total_idonum'] = $jointUsdtNum + $kolUsdtNum + $idoUsdtNum  + config('buyed_total'); //总IDO数量
        $countInfo['join_user'] = User::query()->where('activate', 1)->count() + config('buyed_user');
        $countInfo['idoed_num'] = IdoOrder::query()->where('user_id', auth()->id())->where('status', '<>', 2)->sum('coinnum');
        $countInfo['ido_canget'] = IdoOrder::query()->where('user_id', auth()->id())->where('status', 0)->sum('coinnum');

        return responseJson(compact('idoInfo', 'countInfo'));
    }

    /**
     * @return void
     * KOL
     */
    public function kolInfo()
    {
        $kolInfo = KolList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum')
            ->first();

        $countInfo = [];

        $countInfo['koled_num'] = KolOrder::query()->where('user_id', auth()->id())->where('status', '<>', 2)->sum('coinnum');
        $countInfo['kol_canget'] = User::query()->where('id', auth()->id())->value('amount');

        return responseJson(compact('kolInfo', 'countInfo'));
    }


    /**
     * @return void
     * 联创
     */
    public function jointInfo()
    {
        $jointInfo = JointList::query()
            ->where('status', 1)
            ->select('id', 'usdtnum', 'coinnum')
            ->first();

        $countInfo = [];
        $countInfo['jointed_num'] = JointOrder::query()->where('user_id', auth()->id())->where('status', '<>', 2)->sum('coinnum');
        $countInfo['joint_canget'] = User::query()->where('id', auth()->id())->value('amount');

        return responseJson(compact('jointInfo', 'countInfo'));
    }

    /**
     * @return void
     * 加入之前检查
     */
    public function investCheck(RechargeCheckValidate $request)
    {
        $postData = $request->validated();

        $userInfo = User::query()->where('id', auth()->id())
            ->select('id', 'name', 'node_status','activate')
            ->first();
        if($postData['type'] == 4){
            $userCard = UserNftcard::query()
                ->where('id',$postData['id'])
                ->where('user_id',auth()->id())
                ->first();
            if(empty($userCard)){
                return responseValidateError('该卡牌不存在');
            }

            /*是否最高等级*/
            $maxLevel = CardLevelList::query()
                ->where('card_level',$userCard->card_level)
                ->max('level');
            if($userCard->level >= $maxLevel){
                return responseValidateError('该卡牌已经是最高等级');
            }
        }else{
            if($userInfo->activate == 1){
                return responseValidateError('您已认购过');
            }
        }

//        if ($postData['type'] == 3) {
//            if ($userInfo->node_status == 1) {
//                return responseValidateError('当前已经是联合节点');
//            }
//        } elseif ($postData['type'] == 2) {
//            $issetOrder = KolOrder::query()->where('user_id', auth()->id())->exists();
//            if ($issetOrder) {
//                return responseValidateError('KOL一个地址只能认购一次');
//            }
//        } else {
//            $issetOrder = IdoOrder::query()->where('user_id', auth()->id())->exists();
//            if ($issetOrder) {
//                return responseValidateError('IDO一个地址只能认购一次');
//            }
//        }
        return responseJson();
    }

    /**
     * @return void
     * 加入投资
     */
    public function payorder(\App\Http\Validate\Recharge\PayOrderValidate $request)
    {

        $postData = $request->validated();
        Log::channel('invest')->info('收到支付请求', $postData);

        /*该HASH是否存在*/
        $isset = Recharge::query()->where('hash', $postData['hash'])->exists();
        if ($isset) {
            return responseValidateError('该笔订单已存在');
        }
        $userName = User::query()->where('id', auth()->id())->value('name');
        Recharge::query()->insertGetId([
            'user_id' => auth()->id(),
            'name' => $userName,
            'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
            'type' => 1,
            'chargeid' => $postData['order_id'],
            'num' => $postData['num'],
            'hash' => $postData['hash'],
            'remarks' => $postData['remarks'],
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return responseJson([], 200, '加入成功,等待区块到账...');
    }
}
