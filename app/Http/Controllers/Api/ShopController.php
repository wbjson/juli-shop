<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Shop\ProductListValidate;
use App\Http\Validate\Shop\PushProductAuditValidate;
use App\Http\Validate\Shop\PushProductDetailValidate;
use App\Http\Validate\Shop\PushProductValidate;
use App\Http\Validate\Shop\ShopApplicationDetailValidate;
use App\Http\Validate\Shop\ShopApplicationListValidate;
use App\Http\Validate\Shop\ShopApplicationValidate;
use App\Http\Validate\Shop\UpDownProductValidate;
use App\Models\Image;
use App\Models\MainCurrencyExchange;
use App\Models\Product;
use App\Models\ProductsAttr;
use App\Models\ProductsCategory;
use App\Models\ShopApplication;
use App\Models\ShopCategory;
use App\Models\User;
use App\Models\UsersCoin;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{

    /**
     * 申请商户
     * @param ShopApplicationValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function shopApplication(ShopApplicationValidate $request){
        $validated = $request->validated();
        //判断，如果用户已经是商户
        $user = auth()->user();
        if ($user->is_shop > 0){
            return responseValidateError('已成为商户,无法再申请');
        }
        //检查等级是否达到要求
        $shopCategory = ShopCategory::query()->where('id',$validated['shop_category_id'])->first();
        if (empty($shopCategory)){
            return responseValidateError('所选商户类型不合法');
        }
        if (!in_array($user->level_id,explode(',',$shopCategory->apply_grade))){
            return responseValidateError('未达到可申请条件');
        }
        //检查图片是否合法
        if (Image::query()->where('user_id',$user->id)->whereIn('id',[$validated['legal_card1'],$validated['legal_card2'],$validated['company_logo'],$validated['company_zhi']])->count() < 4){
            return responseValidateError('上传图片不合法');
        }

        if (ShopApplication::query()->where('name',$validated['name'])->where('user_id','<>',$user->id)->whereIn('audit_status',[0,1])->count() > 0){
            return responseValidateError('商铺名称已被占用');
        }
        //判断是否有未审核的
        if (ShopApplication::query()->where('user_id',$user->id)->where('audit_status',0)->count() > 0){
            return responseValidateError('有待审核的申请,请稍后');
        }
        $validated['user_id'] = $user->id;
        $validated['created_at'] = date('Y-m-d H:i:s');
        ShopApplication::query()->insert($validated);
        return responseJson();
    }

    /**
     * 商户申请记录列表
     * @param ShopApplicationListValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function shopApplicationList(ShopApplicationListValidate $request){
        $validated = $request->validated();
        $query = ShopApplication::query()->with(['category'=>function($query){
            $query->select(['id','name']);
        }])->where('user_id',auth()->id())->select(['id','name','username','phone','shop_category_id','audit_status','audit_remarks','created_at']);
        $total = $query->count();
        $list = $query->offset(($validated['page']-1)*$validated['size'])
            ->limit($validated['size'])
            ->orderBy('id','desc')
            ->get();
        return responseJson(compact('total','list'));
    }

    /**
     * 商户申请记录详情
     * @param ShopApplicationDetailValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function shopApplicationDetail(ShopApplicationDetailValidate $request){
        $validated = $request->validated();
        $shopApplication = ShopApplication::query()->with(['category'=>function($query){
            $query->select(['id','name']);
        }])->where(['user_id'=>auth()->id(),'id'=>$validated['id']])->first();
        if (empty($shopApplication)){
            return responseValidateError('未找到该申请记录');
        }
        return responseJson($shopApplication);
    }

    /**
     * 商户基础信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(){
        //累计发货量

        //待发货量

        //待结算订单量

        //待结算货款
        return responseJson();
    }

    /**
     * 产品列表
     * @param ProductListValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productList(ProductListValidate $request){
        $validated = $request->validated();
        $user = User::query()->where('id',auth()->id())->first();
        if ($user->is_shop <= 0){
            return responseValidateError('非商户无法操作');
        }
        $query = Product::query()->where(['shop_id'=>auth()->id()])->select(['id','name','main_picture','price','sales','stock','carts','status','state']);
        if (isset($validated['status']) && !empty($validated['status'])){
            switch ($validated['status']){
                case -1:
                    $query->where('status',$validated['status']);
                    break;
                case 0:
                    $query->where('state',0);
                    break;
                case 1:
                    $query->where('state',1)->where('status',0);
                    break;
                case 2:
                    $query->where('state',1)->where('status',1);
                    break;
                case 3:
                    $query->where('status',2);
                    break;
            }
        }
        $total = $query->count();
        $list = $query->offset(($validated['page']-1)*$validated['size'])
            ->limit($validated['size'])
            ->orderBy('id','desc')
            ->get();
        return responseJson(compact('total','list'));
    }


    /**
     * 上传或修改产品
     * @param PushProductValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pushProduct(PushProductValidate $request){
        $validated = $request->validated();
        $user = User::query()->where('id',auth()->id())->first();
        if ($user->is_shop <= 0){
            return responseValidateError('非商户无法操作');
        }
        $attrs = explode('|',$validated['attr']);
        if (count($attrs) > 4){
            return responseValidateError('最多可填四个属性');
        }
        $attr = [];
        foreach ($attrs as $item){
            $temp = explode(',',$item);
            $attr[$temp[0]] = $temp[1];
        }
        //查询类型是否存在
        if (ProductsCategory::query()->where('id',$validated['category_id'])->where('status',1)->count() <= 0){
            return responseValidateError('所选类型不合法');
        }
        //商品主图
        if (Image::query()->where('id',$validated['main_picture'])->where('user_id',auth()->id())->count() <= 0){
            return responseValidateError('主图上传不合法');
        }
        //商品其他图片
        $picture = explode(',',$validated['picture']);
        if (count($picture) > 5){
            return responseValidateError('最多只可上传5张图');
        }
        if (Image::query()->whereIn('id',$picture)->where('user_id',auth()->id())->count() < count($picture)){
            return responseValidateError('其他图上传不合法');
        }
        //判断是否是修改
        $isPay = false;
        if (isset($validated['product_id']) && $validated['product_id']>0){
            $model = Product::query()->where('id',$validated['product_id'])->where('shop_id',$user->id)->first();
            if (empty($model)){
                return responseValidateError('未找到该产品');
            }
            $validated['status'] = 0;
            if ($model->is_pay == 2){
                $isPay = true;
            }
        }else{
            $model = new Product();
        }

        if ($isPay === false){
            //消费账户余额不足
            $balance = UsersCoin::getAmount($user->id,2);
            $totalCcc = bcmul(config('shop_product_price'),MainCurrencyExchange::getRate('usdt','ccc'),2);
            if ($balance < $totalCcc){
                return responseValidateError('钱包账户余额<'.$totalCcc.'无法提交审核');
            }
        }
        $validated['content'] = htmlspecialchars($validated['content']);

        DB::beginTransaction();
        try {
            $model->shop_id = $user->id;
            $model->category_id = $validated['category_id'];
            $model->name = $validated['name'];
            $model->main_picture = $validated['main_picture'];
            $model->picture = $validated['picture'];
            $model->price = $validated['price'];
            $model->stock = $validated['stock'];
            $model->state = 0;
            $model->status = $validated['status'];
            $model->content = $validated['content'];
            $model->is_pay = $validated['status'] == -1 ? 1 : 2;
            $model->is_audit_push = $validated['is_audit_push'];
            $model->save();

            ProductsAttr::query()->where('id',$model->id)->delete();
            $now = date('Y-m-d H:i:s');
            foreach ($attr as $key=>$item){
                ProductsAttr::query()->insert([
                    'product_id' => $model->id,
                    'name' => $key,
                    'value' => $item,
                    'created_at' => $now
                ]);
            }
            if ($validated['status'] != -1){
                $totalCcc = bcmul(config('shop_product_price'),MainCurrencyExchange::getRate('usdt','ccc'),2);
                UsersCoin::insertIncome($user->id,2,'-'.$totalCcc,1,'提交审核产品');
            }

            DB::commit();
            return responseJson();
        }catch (\Exception $e){
            DB::rollBack();
            return responseValidateError('上传产品失败'.$e->getMessage());
        }
    }

    /**
     * 产品详情
     * @param PushProductDetailValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productDetail(PushProductDetailValidate $request){
        $validated = $request->validated();
        $user = User::query()->where('id',auth()->id())->first();
        if ($user->is_shop <= 0){
            return responseValidateError('非商户无法操作');
        }
        $product = Product::query()->with(['attr'])->where(['id'=>$validated['product_id'],'shop_id'=>auth()->id()])->first();
        if (empty($product)){
            return responseValidateError('未找到产品');
        }
        return responseJson($product);
    }



    /**
     * 产品提交审核
     * @param PushProductAuditValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pushProductAudit(PushProductAuditValidate $request){
        $validated = $request->validated();
        $user = User::query()->where('id',auth()->id())->first();
        if ($user->is_shop <= 0){
            return responseValidateError('非商户无法操作');
        }
        $product = Product::query()->where(['id'=>$validated['product_id'],'shop_id'=>auth()->id()])->first();
        if (empty($product)){
            return responseValidateError('未找到产品');
        }
        if ($product->status != -1){
            return responseValidateError('该产品无需提交审核');
        }
        //消费账户余额不足
        $balance = UsersCoin::getAmount($user->id,2);
        if ($balance < config('shop_product_price')){
            return responseValidateError('钱包账户余额<'.config('shop_product_price').'无法提交审核');
        }
        DB::beginTransaction();
        try {
            $product->status = 0;
            $product->save();

            UsersCoin::insertIncome($user->id,2,'-'.config('shop_product_price'),1,'提交审核产品');

            DB::commit();
            return responseJson();
        }catch (\Exception $e){
            DB::rollBack();
            return responseValidateError('操作失败');
        }
    }

    /**
     * 上下架产品
     * @param UpDownProductValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upDownProduct(UpDownProductValidate $request){
        $validated = $request->validated();
        $user = User::query()->where('id',auth()->id())->first();
        if ($user->is_shop <= 0){
            return responseValidateError('非商户无法操作');
        }
        $product = Product::query()->where(['id'=>$validated['product_id'],'shop_id'=>auth()->id()])->first();
        if (empty($product)){
            return responseValidateError('未找到产品');
        }
        if ($product->state != 1){
            return responseValidateError('该产品未审核通过,无法上下架');
        }
        $product->status = $validated['status'];
        $product->save();
        return responseJson();
    }
}
