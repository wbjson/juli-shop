<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Transfer\TransferListValidate;
use App\Http\Validate\Transfer\TransferValidate;
use App\Models\IncomeLog;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UsersCoin;
use App\Models\Withdraw;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class TransferController extends Controller
{


    public function transfer(TransferValidate $request){
        $key = 'transfer:'.auth()->id();
        if (!Redis::setnx($key,1)){
            return responseValidateError('操作频繁');
        }
        $validated = $request->validated();
        //查找用户
        $user = auth()->user();
        $other = User::query()->where('name',$validated['address'])->first();
        if (empty($other)){
            Redis::del($key);
            return responseValidateError('未找到对应用户');
        }
        if ($user->name == $other->name){
            Redis::del($key);
            return responseValidateError('不能转账给自己');
        }
        $balance = UsersCoin::getAmount(auth()->id(),2);
        if ($balance < $validated['num']){
            Redis::del($key);
            return responseValidateError('余额不足,无法操作');
        }

        DB::beginTransaction();
        try {
            UsersCoin::insertIncome(auth()->id(),2,'-'.$validated['num'],4,'内部转账');
            UsersCoin::insertIncome($other->id,2,$validated['num'],4,'内部转账');

            Transfer::query()->insert([
                'user_id' => auth()->id(),
                'other_id' => $other->id,
                'num' => $validated['num'],
                'created_at' => date('Y-m-d H:i:s')
            ]);

            DB::commit();
            Redis::del($key);
            return responseJson();
        }catch (\Exception $e){
            DB::rollBack();
            Redis::del($key);
            return responseValidateError('操作失败'.$e->getMessage().$e->getLine());
        }
    }


    public function transferList(TransferListValidate $request){
        $validated = $request->validated();
        $query = Transfer::query()->with(['other'=>function($query){
            $query->select(['id','name']);
        }])->where('user_id',auth()->id())
            ->select(['other_id','num','created_at']);

        $total = $query->count();
        $list = $query->offset(($validated['page']-1)*$validated['size'])
            ->limit($validated['size'])
            ->orderBy('id','desc')
            ->get();
        return responseJson(compact('total','list'));
    }

}
