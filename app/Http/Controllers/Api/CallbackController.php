<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Jobs\RechargeJob;
use App\Models\IncomeLog;
use App\Models\Recharge;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CallbackController extends Controller
{

    public function recharge(Request $request)
    {
        $data = $request->post();
        Log::channel('recharge_callback')->info('收到回调', $data);
        if (
            empty($data) ||
            !isset($data['coin_token']) ||
            empty($data['from_address']) ||
            !isset($data['status']) ||
            !in_array($data['status'], [1, 2, 3])
        ) {
            Log::channel('recharge_callback')->info('参数不完整');
            exit();
        }
        if (Recharge::query()->where('hash', $data['hash'])->exists()) {
            Log::channel('recharge_callback')->info('该hash已处理');
            exit();
        }

        $user = User::query()->where('name', $data['from_address'])->exists();
        if (!$user) {
            Log::channel('recharge_callback')->info('未找到用户信息');
            exit();
        }

        if ($data['coin_token'] == 'USDT') {
            //检查一下请求有效性
            if (!isset($data['remarks']) && empty($data['remarks'])) {
                Log::channel('recharge_callback')->info('remarks未有订单信息');
                exit();
            }
            RechargeJob::dispatch($data)->onQueue('recharge');
        } else {
            if ($data['status'] == 1) {
                DB::beginTransaction();
                try {
                    $userInfo = User::query()->where('name', $data['from_address'])->first();
                    if (!$userInfo) {
                        Log::channel('recharge_callback')->info('未找到用户信息');
                        exit();
                    }
                    $beforeAmount = $userInfo->freeze_coin;
                    $userInfo->product_freeze = bcadd($userInfo->product_freeze, $data['amount'], 4);   //冻结积分累计
                    $userInfo->total_product_freeze = bcadd($userInfo->total_product_freeze, $data['amount'], 4);   //冻结积分累计
                    $userInfo->freeze_coin = bcadd($userInfo->freeze_coin, $data['amount'], 4);   //冻结积分累计
                    $userInfo->save();
                    /*修改会员等级*/
                    IncomeLog::query()->insertGetId([
                        'user_id' => $userInfo->id,
                        'amount_type' => 2,
                        'type' => 7,
                        'before' => $beforeAmount,
                        'total' => $data['amount'],
                        'after' => $userInfo->freeze_coin,
                        'remark' => '转账商家' . $data['amount'],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);

                    /*添加冻结记录*/
                    Recharge::query()->insert([
                        'user_id' => $userInfo->id,
                        'name' => $userInfo->name,
                        'type' => 2,
                        'chargeid' => 0,
                        'order_no' => 'TRAN' . date('YmdHis') . mt_rand(10000, 99999),
                        'num' => $data['amount'],
                        'hash' => $data['hash'],
                        'remarks' => $data['remarks'],
                        'status' => 2,
                        'created_at' => date('Y-m-d H:i:s'),
                        'finished_at' => date('Y-m-d H:i:s')
                    ]);
                    echo 'success';
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::channel('recharge_callback')->info($e->getLine() . '====' . $e->getMessage());
                }
            }

        }

    }


    public function withdraw(Request $request)
    {
        Log::channel('withdraw_callback')->info('收到回调', $request->post());
        Log::channel('withdraw_callback')->info('请求IP:' . request()->ip());
        $data = $request->post();
        if (empty($data) || !isset($data['address']) || empty($data['address'])) {
            Log::channel('withdraw_callback')->info('无法继续');
            exit();
        }
        exit();

        Log::channel('withdraw_callback')->info('处理成功');
    }

}
