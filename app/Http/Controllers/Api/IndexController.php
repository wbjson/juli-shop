<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\News;
use App\Models\Product;
use App\Models\ProductExhibit;
use App\Models\ProductsCategory;

class IndexController extends Controller
{
    //首页接口
    public function index()
    {
        /*Banner*/
        $bannerList = Banner::query()
            ->where('status', 1)
            ->where('type', 1)
            ->select('id', 'name', 'banner', 'link')
            ->orderByRaw('sort')
            ->get();        //Banner

        $newInfo = News::query()
            ->where('status', 1)
            ->orderByRaw('sort')
            ->select('id', 'title', 'content')
            ->first();  //公告

        $columnList = ProductsCategory::query()
            ->where('status', 1)
            ->where('level', 1)
            ->orderByRaw('sortorder')
            ->select('id', 'name', 'icon', 'sortorder')
            ->limit(20)
            ->get();        //栏目

        /*推荐商品*/
        $recordGoodsId = ProductExhibit::query()
            ->where('type', 1)
            ->where('status', 1)
            ->orderByRaw('orders')
            ->limit(20)
            ->pluck('product_id');

        $query_str = request()->post('query_name', '');
        $goodsList = Product::query()
            ->where('status', 1)
            ->where(function ($query) use ($recordGoodsId) {
                if (!$recordGoodsId->isEmpty()) {
                    $query->whereIn('id', $recordGoodsId);
                }
            })
            ->where(function ($query) use ($query_str) {
                if (!empty($query_str)) {
                    $query->where('name', 'like', '%' . $query_str . '%');
                }
            })
            ->orderByRaw('sort')
            ->select(['id', 'name', 'sales', 'main_picture', 'price', 'sales'])
            ->limit(30)
            ->get();
        return responseJson(compact('bannerList', 'newInfo', 'columnList', 'goodsList'));
    }

    /**
     * @return void
     * 商品列表
     */
    public function productList()
    {
        $query_str = request()->post('query_str');
        $sortname = request()->post('sortname');
        $sort = request()->post('sort', 'asc');
        $cid = request()->post('cateid', '0');
        $type = request()->post('type', '0');
        $query = Product::query()
            ->where('status', 1)
            ->where('type', 1)
            ->select(['id', 'name', 'main_picture', 'price', 'sales']);
        $goodsList = $query
            ->where(function ($query) use ($query_str) {
                if (!empty($query_str)) {
                    $query->where('name', 'like', $query_str);
                }
            })
            ->where(function ($query) use ($sortname, $sort) {
                if (!empty($sortname)) {
                    $query->orderby($sortname, $sort);
                }
            })
            ->where(function ($query) use ($cid) {
                if ($cid > 0) {
                    $query->where('category_id', $cid);
                }
            })
            ->where(function ($query) use ($type) {
                if ($type > 0) {
                    $query->where('type', $type);
                }
            })
            ->select(['id', 'name', 'main_picture', 'price', 'sales', 'type'])
            ->limit(16)
            ->get();
        return responseJson($goodsList);
    }


    /**
     * @return void
     * 商品推荐列表
     */
    public function orderRecomList()
    {
        $type = request()->post('type', 1);
        /*推荐商品*/
        $recordGoods = ProductExhibit::query()
            ->with(['product' => function ($query) {
                $query->select('id', 'category_id', 'main_picture', 'name', 'price');
            }])
            ->where('type', $type)
            ->where('status', 1)
            ->select('id', 'product_id')
            ->orderByRaw('orders')
            ->get();
        return responseJson($recordGoods);
    }
}
