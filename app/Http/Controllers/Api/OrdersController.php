<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Orders\AddressUpdateOrderValidate;
use App\Http\Validate\Orders\AppraiseOrderValidate;
use App\Http\Validate\Orders\AuditReturnsOrderValidate;
use App\Http\Validate\Orders\CancelOrderValidate;
use App\Http\Validate\Orders\GenerateOrderValidate;
use App\Http\Validate\Orders\OrderListValidate;
use App\Http\Validate\Orders\PayOrderValidate;
use App\Http\Validate\Orders\ReturnsApplyOrderValidate;
use App\Http\Validate\Orders\ReturnsDetailsOrderValidate;
use App\Http\Validate\Orders\ReturnsOrderValidate;
use App\Http\Validate\Orders\ShipOrderValidate;
use App\Http\Validate\Orders\ShopOrderListValidate;
use App\Http\Validate\Orders\SubmitOrderValidate;
use App\Http\Validate\Orders\SubmitReturnsOrderValidate;
use App\Models\Cart;
use App\Models\IncomeLog;
use App\Models\Order;
use App\Models\OrdersAppraise;
use App\Models\OrdersDetail;
use App\Models\OrdersReturnsApply;
use App\Models\Product;
use App\Models\User;
use App\Models\UsersAddress;
use App\Models\UsersCoin;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class OrdersController extends Controller
{

    /**
     * 下单
     * @param GenerateOrderValidate $request
     * @return JsonResponse
     */
    /**
     * 下单
     * @param GenerateOrderValidate $request
     * @return JsonResponse
     */
    public function generate(GenerateOrderValidate $request)
    {
        $validated = $request->validated();
        //查询地址是否有效
        $address = UsersAddress::query()
            ->where('user_id', auth()->id())
            ->where('id', $validated['address_id'])
            ->first();
        if (empty($address)) {
            return responseValidateError('未找到该地址');
        }

        $user = User::query()->where('id', auth()->id())->first();

        $goodsNum = 0;
        $order_money = 0;
        foreach ($validated['product_id'] as $key => $item) {
            $product = Product::query()->where('id', $item)->first();
            if (empty($product)) {
                return responseValidateError('未找到该产品');
            }
            if ($product->status != 1) {
                return responseValidateError('产品' . $product->name . '已下架,不能下单');
            }
            if ($product->stock < $validated['product_count'][$key]) {
                return responseValidateError('产品' . $product->name . '库存不足');
            }

            $moneyNum = bcmul($product->price, $validated['product_count'][$key], 2);
            $order_money = bcadd($order_money, $moneyNum, 2);
            $goodsNum = $goodsNum + $validated['product_count'][$key];
        }

        if ($order_money <= 0) {
            return responseValidateError('订单金额不正确');
        }

        //查询产品是否存在
        DB::beginTransaction();
        try {
            $orderData = [];
            $orderData['order_no'] = date('YmdHis') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
            $orderData['user_id'] = $user->id;
            $orderData['order_money'] = $order_money;       //订单总余额数量
            $orderData['goods_num'] = $goodsNum;       //商品数量
            $orderData['type'] = $product->type;       //商品数量
            $orderData['shopping_id'] = $validated['address_id'];
            $orderData['get_user_name'] = $address->name;
            $orderData['get_user_phone'] = $address->phone;
            $orderData['province'] = $address->province;
            $orderData['city'] = $address->city;
            $orderData['district'] = $address->district;
            $orderData['address'] = $address->address;
            $orderData['created_at'] = date('Y-m-d H:i:s');
            if (!empty($validated['pay_remark'])) {
                $orderData['pay_remarks'] = $validated['pay_remark'];
            }
            $orderId = Order::query()->insertGetId($orderData);


            $goodsCount = 0;
            foreach ($validated['product_id'] as $key => $item) {
                $product = Product::query()->with(['shop'])->where('id', $item)->first();
                $moneyNum = bcmul($product->price, $validated['product_count'][$key], 2);
                $detailData = [];
                $detailData['order_id'] = $orderId;
                $detailData['user_id'] = $user->id;
                $detailData['product_id'] = $product->id;
                $detailData['product_name'] = $product->name;
                $detailData['product_img'] = $product->main_picture;
                $detailData['product_type'] = $product->type;
                $detailData['product_count'] = $validated['product_count'][$key];
                $detailData['product_price'] = $product->price;
                $detailData['order_price'] = $moneyNum;
                $detailData['type'] = $product->type;
                $detailData['shopping_id'] = $validated['address_id'];
                $detailData['created_at'] = date('Y-m-d H:i:s');
                OrdersDetail::query()->insertGetId($detailData);
                $goodsCount = $goodsCount + $detailData['product_count'];

                $isinCar = Cart::query()->where('user_id', $user->id)->where('product_id', $product->id)->first();
                if (!empty($isinCar)) {
                    $isinCar->delete();
                }
            }
            $orderDetail = OrdersDetail::query()
                ->where('order_id', $orderId)
                ->select('product_id', 'product_name', 'product_img', 'product_count', 'product_price', 'order_price')
                ->get();

            DB::commit();
            $order_money = $order_money * 1;
            return responseJson(compact('orderId', 'order_money', 'orderDetail', 'goodsCount'));
        } catch (Exception $e) {
            DB::rollBack();
            return responseValidateError('下单失败' . $e->getMessage() . $e->getLine());
        }
    }

    /**
     * 支付订单
     * @param PayOrderValidate $request
     * @return JsonResponse
     */
    public function pay(PayOrderValidate $request)
    {
        $key = "payOrder" . auth()->id();
        if (!Redis::setnx($key, 1)) {
            Redis::del($key);
            return responseValidateError('操作频繁');
        }
        //查询订单
        $validated = $request->validated();
        $orderId = $validated['order_id'];
        $orderInfo = Order::query()
            ->where('id', $orderId)
            ->where('user_id', auth()->id())
            ->where('order_status', 0)
            ->first();
        if (empty($orderInfo)) {
            Redis::del($key);
            return responseValidateError('未找到对应的订单');
        }
        if ($orderInfo->order_money <= 0) {
            Redis::del($key);
            return responseValidateError('订单不正确');
        }
        $user = User::query()->where('id', $orderInfo->user_id)->first();

        if ($validated['type'] == 1) {
            if ($user->amount < $orderInfo->order_money) {
                Redis::del($key);
                return responseValidateError('余额不足,无法支付');
            }

            DB::beginTransaction();
            try {
                $orderInfo->pay_price = $orderInfo->order_money;
                $orderInfo->order_status = 1;
                $orderInfo->payment_type = 1;
                $orderInfo->pay_time = date('Y-m-d H:i:s');
                $orderInfo->save();

                $orderDetail = OrdersDetail::query()
                    ->where('order_id', $orderInfo->id)
                    ->where('user_id', $user->id)
                    ->get();
                foreach ($orderDetail as $item) {

                    $item->order_status = 1;
                    $item->save();
                    Product::query()->where('id', $item->product_id)->increment('sales', $item->product_count);
                    Product::query()->where('id', $item->product_id)->decrement('stock', $item->product_count);
                }

                /*扣除余额*/
                $beforeCoin = $user->amount;        //操作前积分
                $user->amount = bcsub($user->amount, $orderInfo->order_money, 2);       //扣除积分
                if ($user->activate != 1) {
                    $user->activate = 1;
                }
                $user->save();

                IncomeLog::query()->insertGetId([
                    'user_id' => $user->id,
                    'amount_type' => 2,
                    'type' => 8,
                    'before' => $beforeCoin,
                    'total' => -$orderInfo->order_money,
                    'after' => $user->amount,
                    'remark' => '支付订单',
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                DB::commit();
                Redis::del($key);
                return responseJson();
            } catch (Exception $e) {
                DB::rollBack();
                Redis::del($key);
                return responseValidateError('支付失败');
            }
        }
    }

    /**
     * 订单列表
     * @param OrderListValidate $request
     * @return JsonResponse
     */
    public function orderList(OrderListValidate $request)
    {
        $validated = $request->validated();
        $query = Order::query()->with(['shopping'])
            ->with(['getaddress' => function ($query) {
                $query->select(['id', 'name', 'phone', 'province', 'city', 'district', 'address']);
            }])
            ->with(['detaillist' => function ($query) {
                $query->select(['order_id', 'product_id', 'product_name', 'product_img', 'product_count', 'product_price', 'order_price']);
            }])
            ->where(['user_id' => auth()->id()])
            ->select(['id', 'order_no', 'type',
                'order_status', 'order_money', 'pay_time', 'delivery_time',
                'ship_company', 'goods_num', 'ship_no', 'shopping_id', 'pay_remark', 'created_at']);
        if (strlen($validated['status']) > 0) {
            if ($validated['status'] >= 0) {
                $query->where('order_status', $validated['status']);
            }
        }
        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->simplePaginate($validated['size'], '*', 'page', $validated['page']);
        $list = $list->items();

        return responseJson(compact('total', 'list'));
    }

    /**
     * 取消订单
     * @param CancelOrderValidate $request
     * @return JsonResponse
     */
    public function cancel(CancelOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where('id', $validated['order_id'])->where('user_id', auth()->id())->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if ($order->order_status != 0) {
            return responseValidateError('订单不能取消');
        }
        DB::beginTransaction();
        try {
            Order::query()->where('id', $validated['order_id'])->delete();
            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError();
        }
    }

    /**
     * 修改地址
     * @param AddressUpdateOrderValidate $request
     * @return JsonResponse
     */
    public function addressUpdate(AddressUpdateOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where(['user_id' => auth()->id(), 'id' => $validated['order_id']])->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if ($order->order_status >= 2) {
            return responseValidateError('订单已不允许修改地址');
        }

        $address = UsersAddress::query()
            ->where('user_id', auth()->id())
            ->where('id', $validated['address_id'])
            ->first();
        if (empty($address)) {
            return responseValidateError('未找到该地址');
        }


        $address->shopping_id = $validated['address_id'];
        $order->save();
        return responseJson();
    }


    /**
     * 确认收获
     * @param SubmitOrderValidate $request
     * @return JsonResponse
     */
    public function submit(SubmitOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where(['id' => $validated['order_id'], 'user_id' => auth()->id()])->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if ($order->order_status != 2) {
            return responseValidateError('订单不允许该操作');
        }

        DB::beginTransaction();
        try {
            $order->order_status = 3;
            $order->save();

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }
    }

    /**
     * 订单评价
     * @param AppraiseOrderValidate $request
     * @return JsonResponse
     */
    public function appraise(AppraiseOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where(['id' => $validated['order_id'], 'user_id' => auth()->id()])->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if ($order->order_status != 3 || $order->is_appraise != 0) {
            return responseValidateError('订单不允许该操作');
        }

        DB::beginTransaction();
        try {
            $order->is_appraise = 1;
            $order->save();

            $validated['order_id'] = $order->id;
            $validated['created_at'] = date('Y-m-d H:i:s');
            OrdersAppraise::query()->insert($validated);

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }
    }


    /**
     * 申请退货退款
     * @param ReturnsApplyOrderValidate $request
     * @return JsonResponse
     */
    public function returnsApply(ReturnsApplyOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where(['id' => $validated['order_id'], 'user_id' => auth()->id()])->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if ($order->order_status != 2) {
            return responseValidateError('订单不允许该操作');
        }
        if (OrdersReturnsApply::query()->whereIn('status', [0, 1, 2, 3])->where('order_id', $order->id)->count() > 0) {
            return responseValidateError('已申请售后无需重复申请');
        }
        DB::beginTransaction();
        try {
            $order->order_status = -1;
            $order->save();
            OrdersReturnsApply::query()->insert([
                'order_id' => $validated['order_id'],
                'return_no' => date('YmdHis') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT),
                'user_id' => auth()->id(),
                'state' => $validated['state'],
                'product_status' => $validated['product_status'],
                'why' => $validated['why'],
                'created_at' => date('Y-m-d H:i:s')
            ]);

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败' . $e->getMessage());
        }
    }


    /**
     * @return void
     * 订单详情
     */
    public function orderDetail()
    {
        $orderId = request()->post('order_id', 0);
        if ($orderId <= 0) {
            return responseValidateError('订单不存在');
        }
        $orderInfo = Order::query()
            ->with(['getaddress' => function ($query) {
                $query->select(['id', 'name', 'phone', 'province', 'city', 'district', 'address']);
            }])
            ->where('id', $orderId)
            ->select(['id', 'order_no', 'type',
                'order_status', 'order_money', 'pay_time', 'delivery_time',
                'ship_company', 'ship_no', 'goods_num', 'shopping_id', 'pay_remark', 'created_at'])
            ->first();
        if (empty($orderInfo)) {
            return responseValidateError('订单不存在');
        }
        $orderInfo['order_detail'] = OrdersDetail::query()
            ->where('order_id', $orderId)
            ->select('product_id', 'product_name', 'product_img', 'product_count', 'product_price', 'order_price')
            ->get();

        return responseJson($orderInfo);
    }


    /**
     * 退货退款详情
     * @param ReturnsDetailsOrderValidate $request
     * @return JsonResponse|void
     */
    public function returnsDetail(ReturnsDetailsOrderValidate $request)
    {
        $validated = $request->validated();
        $order = Order::query()->where(['id' => $validated['order_id']])->where(function ($query) {
            $query->where('user_id', auth()->id())->orWhere('shop_id', auth()->id());
        })->first();
        if (empty($order)) {
            return responseValidateError('订单不存在');
        }
        if (!in_array($order->order_status, [-1, -2, -3])) {
            return responseValidateError('订单不允许该操作');
        }
        $apply = OrdersReturnsApply::query()->where('order_id', $validated['order_id'])->orderByDesc('id')->first();
        if (empty($apply)) {
            return responseValidateError('未找到申请记录');
        }
        return responseJson($apply);
    }


    /**
     * 填写退货退款信息
     * @param ReturnsOrderValidate $request
     * @return JsonResponse
     */
    public function returns(ReturnsOrderValidate $request)
    {
        $validated = $request->validated();
        $returns = OrdersReturnsApply::query()->where('id', $validated['return_id'])->where('user_id', auth()->id())->first();
        if (empty($returns)) {
            return responseValidateError('未找到该申请记录');
        }
        if ($returns->status != 1) {
            return responseValidateError('请等待商家审核');
        }
        $ship_company = isset($validated['ship_company']) && !empty($validated['ship_company']) ? $validated['ship_company'] : '';
        $ship_no = isset($validated['ship_no']) && !empty($validated['ship_no']) ? $validated['ship_no'] : '';

        if ($returns->state == 1 && (empty($ship_company) || empty($ship_no))) {
            return responseValidateError('请填写退货快递信息');
        }
        DB::beginTransaction();
        try {
            $returns->status = 2;
            $returns->ship_company = $ship_company;
            $returns->ship_no = $ship_no;
            $returns->save();

            Order::query()->where('id', $returns->order_id)->update([
                'order_status' => -2
            ]);

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }

    }


    /**
     * 商家端订单列表
     * @param ShopOrderListValidate $request
     * @return JsonResponse
     */
    public function shopOrderList(ShopOrderListValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where('id', auth()->id())->first();
        if ($user->is_shop <= 0) {
            return responseValidateError('非商户无法操作');
        }
        $query = Order::query()->with(['shopping'])
            ->where(['shop_id' => auth()->id()])->select(['id', 'order_no', 'product_id', 'product_name', 'product_img', 'supplier_name',
                'order_status', 'product_count', 'product_price', 'product_amount_total', 'order_amount_total', 'pay_remarks',
                'payment_type', 'shopping_id', 'pay_time', 'delivery_time', 'is_appraise', 'ship_company', 'ship_no', 'created_at']);
        if (strlen($validated['status']) > 0) {
            if ($validated['status'] == 3) {
                $query->where('order_status', 3)->where('is_appraise', 0);
            } elseif ($validated['status'] == 4) {
                $query->whereIn('order_status', [-1, -2, -3]);
            } else {
                $query->where('order_status', $validated['status']);
            }
        }
        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->simplePaginate($validated['size'], '*', 'page', $validated['page']);
        $list = $list->items();

        return responseJson(compact('total', 'list'));
    }

    /**
     * 发货
     * @param ShipOrderValidate $request
     * @return JsonResponse
     */
    public function ship(ShipOrderValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where('id', auth()->id())->first();
        if ($user->is_shop <= 0) {
            return responseValidateError('非商户无法操作');
        }
        $order = Order::query()->where('id', $validated['order_id'])->where(['shop_id' => $user->id])->first();
        if (empty($order)) {
            return responseValidateError('未找到该订单');
        }
        if ($order->order_status != 1) {
            return responseValidateError('该订单无需发货');
        }
        DB::beginTransaction();
        try {
            $order->order_status = 2;
            $order->delivery_time = date('Y-m-d H:i:s');
            $order->ship_company = $validated['ship_company'];
            $order->ship_no = $validated['ship_no'];
            $order->save();

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }
    }

    /**
     * 审批退货申请
     * @param AuditReturnsOrderValidate $request
     * @return JsonResponse
     */
    public function auditReturns(AuditReturnsOrderValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where('id', auth()->id())->first();
        if ($user->is_shop <= 0) {
            return responseValidateError('非商户无法操作');
        }
        $return = OrdersReturnsApply::query()->where('id', $validated['return_id'])->first();
        if (empty($return)) {
            return responseValidateError('未找到该申请');
        }
        $shopId = Order::query()->where('id', $return->order_id)->value('shop_id');
        if ($shopId != $user->id) {
            return responseValidateError('非法请求');
        }
        if ($return->status != 0) {
            return responseValidateError('该申请无需审核');
        }
        DB::beginTransaction();
        try {
            $return->status = $validated['status'];
            if ($validated['status'] == 1) {
                $return->username = $validated['username'];
                $return->phone = $validated['phone'];
                $return->address = $validated['address'];
            } else {
                $return->audit_why = $validated['audit_why'];
            }
            $return->audit_time = date('Y-m-d H:i:s');
            $return->save();

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }
    }

    /**
     * 商家确认收货并退款
     * @param SubmitReturnsOrderValidate $request
     * @return JsonResponse
     */
    public function submitReturns(SubmitReturnsOrderValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where('id', auth()->id())->first();
        if ($user->is_shop <= 0) {
            return responseValidateError('非商户无法操作');
        }
        $return = OrdersReturnsApply::query()->where('id', $validated['return_id'])->first();
        if (empty($return)) {
            return responseValidateError('未找到该申请');
        }
        $order = Order::query()->where('id', $return->order_id)->first();
        if ($order->shop_id != $user->id) {
            return responseValidateError('非法请求');
        }
        if ($return->status != 2) {
            return responseValidateError('该申请无需审核');
        }
        DB::beginTransaction();
        try {
            $return->status = 3;
            $return->save();

            $order->order_status = -3;
            $order->save();

            UsersCoin::insertIncome($user->id, 2, $order->order_amount_total, 3, '退货退款');

            DB::commit();
            return responseJson();
        } catch (Exception $e) {
            DB::rollBack();
            return responseJsonAsServerError('操作失败');
        }
    }

}
