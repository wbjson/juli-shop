<?php


namespace App\Http\Controllers\Api;


use App\Enums\SmsEnum;
use App\Http\Controllers\Controller;
use App\Http\Validate\Basic\SendSmsValidate;
use App\Jobs\UpdateUserNode;
use App\Models\Banner;
use App\Models\Bulletin;
use App\Models\Card;
use App\Models\GroupCategory;
use App\Models\Order;
use App\Models\ShopCategory;
use App\Models\User;
use App\Units\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class BasicController extends Controller
{

    /**
     * 上传文件
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $file = $request->file('image');
        $maxSize = $request->post('max_size', 0);
        if (empty($file)) {
            return responseValidateError('请选择文件');
        }
        $imageUpload = new ImageUpload();
        $result = $imageUpload->save($file, 'upload', 'images', $maxSize);
        if (!is_array($result)) {
            return responseValidateError($result);
        }
        return responseJson($result);
    }

    /**
     * 返回商铺类别
     * @return \Illuminate\Http\JsonResponse
     */
    public function shopCategory()
    {
        return responseJson(ShopCategory::query()->select(['id', 'name'])->get());
    }

    /**
     * 返回社区类别
     * @return \Illuminate\Http\JsonResponse
     */
    public function groupCategory()
    {
        return responseJson(GroupCategory::query()->select(['id', 'name'])->get());
    }

    public function banner()
    {
        $list = Banner::query()->where('status', 1)
//            ->where('lang', request()->header('lang', 'zh_CN'))
            ->orderBy('sort', 'desc')
            ->select(['name', 'banner'])
            ->get();

        foreach ($list as $v) {
            $v->banner = assertUrl($v->banner, 'admin');
        }

        return responseJson($list);
    }


    public function bulletin()
    {

        $list = Bulletin::query()->where('status', 1)->where('lang', request()->header('lang', 'zh_CN'))->orderBy('sort', 'desc')->select(['title', 'content'])->get();

        return responseJson($list);

    }

    //首页行情数据
    public function base()
    {
        $lang = request()->header('lang', 'zh_CN');
        return responseJson([
            'video' => $lang == 'zh_CN' ? assertUrl(setting('project_video'), 'admin') : assertUrl(setting('project_video_en'), 'admin'),
            'desc' => $lang == 'zh_CN' ? setting('project_desc') : setting('project_desc_en'),
            'rule' => $lang == 'zh_CN' ? setting('rule_text') : setting('rule_text_en'),
            'contract_address' => setting('contract_address'),
            'hole_address' => setting('hole_address')
        ]);
    }

    /**
     * @return void
     * 提现配置
     */
    public function withdrawConfig(){
        return responseJson([
            'withdraw_fee'=>config('withdraw_rate'),
            'min_withdraw'=>config('min_withdraw'),
            'yesterday_order'=>Order::query()->where('order_status','>=',1)->whereDate('created_at',date('Y-m-d-1'))->count(),
        ]);

    }


    public function cardList()
    {
        $list = Card::query()->select(['name', 'img', 'rate', 'lottery_rate'])->get();
        foreach ($list as $item) {
            $item->img = assertUrl($item->img, 'admin');
        }
        return responseJson($list);
    }


    /**
     * 发送短信
     * @param SendSmsValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSms(SendSmsValidate $request)
    {
        $validated = $request->validated();
        if (!in_array($validated['event'], ['register','findPassword'])) {
            return responseValidateError('非法请求');
        }

        $smsIplimit = 'sms:' . request()->ip();
        //限制IP
        if (Redis::get($smsIplimit)) {
            return responseValidateError('发送过快');
        }

        if ($validated['event'] == 'register' && User::query()->where('phone', $validated['phone'])->exists()) {
            return responseValidateError('该手机号已注册,无法发送');
        }

        if ($validated['event'] == 'findPassword' && !User::query()->where('phone', $validated['phone'])->exists()) {
            return responseValidateError('该手机号未注册,无法发送');
        }

        if (logic('sms')->sendsms($validated['phone'], $validated['event']) === true) {
            Redis::setex($smsIplimit, 60, 1);
            return responseJson();
        }
        return responseValidateError('验证码发送失败');
    }


    public function titleInfo(){
        $type = request()->post('type',1);
        $titleInfo = Bulletin::query()
            ->where('type',$type)
            ->select('id','title','content','created_at')
            ->first();
        return responseJson($titleInfo);
    }
}
