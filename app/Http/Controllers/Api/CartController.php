<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Cart\AddCartValidate;
use App\Http\Validate\Cart\CheckedCartValidate;
use App\Http\Validate\Cart\DestroyCartValidate;
use App\Http\Validate\Cart\UpdateNumCartValidate;
use App\Models\Card;
use App\Models\Cart;
use App\Models\Product;

class CartController extends Controller
{

    /**
     * 购物车商品列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartList()
    {
        $list = Cart::query()->with(['product' => function ($query) {
            $query->select(['id', 'name', 'main_picture', 'price', 'status', 'state']);
        }])->where('user_id', auth()->id())->select(['id', 'product_id', 'quantity', 'is_checked'])->orderByDesc('id')->get();
        return responseJson($list);
    }

    /**
     * 添加购物车
     * @param AddCartValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCart(AddCartValidate $request)
    {
        $validated = $request->validated();
        $product = Product::query()->where('id', $validated['product_id'])->first();
        if (empty($product)) {
            return responseValidateError('未找到产品');
        }
        if ($product->status != 1) {
            return responseValidateError('该产品已下架无法添加购物车');
        }


        //判断下，如果已添加购物车，加数量即可
        $cars = Cart::query()->where('user_id', auth()->id())->where('product_id', $product->id)->first();
        if (!empty($cars)) {
            $cars->quantity = $cars->quantity + 1;
            $cars->save();
        } else {
            Cart::query()->insert([
                'user_id' => auth()->id(),
                'product_id' => $product->id,
                'type' => $product->type,
                'quantity' => $validated['quantity'],
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return responseJson();
    }

    /**
     * 删除购物车
     * @param DestroyCartValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyCart(DestroyCartValidate $request)
    {
        $validated = $request->validated();
        $cardId = explode(',', $validated['cart_id']);
        $cart = Cart::query()->whereIn('id', $cardId)->where('user_id', auth()->id())->first();
        if (!empty($cart)) {
            Cart::query()->whereIn('id', $cardId)->where('user_id', auth()->id())->delete();
        }
        return responseJson();
    }

    /**
     * 增加或减少购物车商品数量
     * @param UpdateNumCartValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateNumCart(UpdateNumCartValidate $request)
    {
        $validated = $request->validated();
        $cart = Cart::query()->where('id', $validated['cart_id'])->where('user_id', auth()->id())->first();
        if (!empty($cart)) {
            if ($cart->quantity == 1 && $validated['type'] == 2) {
                return responseJson();
            }
            $cart->quantity = $validated['type'] == 1 ? $cart->quantity + 1 : $cart->quantity - 1;
            $cart->save();
        }
        return responseJson();
    }

    /**
     * 选中或取消选中商品
     * @param CheckedCartValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkedCart(CheckedCartValidate $request)
    {
        $validated = $request->validated();
        $cart = Cart::query()->where('id', $validated['cart_id'])->where('user_id', auth()->id())->first();
        if (!empty($cart)) {
            $cart->is_checked = $validated['type'];
            $cart->save();
        }
        return responseJson();
    }

}
