<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Validate\Recharge\RechargeListValidate;
use App\Http\Validate\Withdraw\InputValidate;
use App\Http\Validate\Withdraw\RedemptionValidate;
use App\Models\IncomeLog;
use App\Models\InvestPledgeLog;
use App\Models\SignApi;
use App\Models\UserBank;
use App\Models\Withdraw;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class WithdrawController extends Controller
{


    public function index(InputValidate $request)
    {
        $user = auth()->user();
        $validated = $request->validated();
        if ($user->amount < $validated['num']) {
            return responseValidateError('余额不足');
        }
        if ($validated['num'] < config('min_withdraw')) {
            return responseValidateError('最低提现' . config('min_withdraw') . '起');
        }
        if (Withdraw::query()->where('user_id', auth()->id())->whereDate('created_at', date('Y-m-d'))->count() >= config('every_day_withdraw')) {
            return responseValidateError('每日只能提现' . config('every_day_withdraw') . '次');
        }
        $key = 'Money_withdraw:' . $user->id;
        // 在控制器中，给这个key上锁10秒钟，如果不主动释放，10秒钟后服务器会自动释放
        $lock = Cache::lock($key, 10);
        $isset_cache = $lock->get();
        if (!$isset_cache) {
            return responseValidateError('请勿重复提交');
        }

        /*提现银行卡信息*/

        $withNo = 'W' . date('YmdHis') . mt_rand(1000000, 99999000);;
        DB::beginTransaction();
        try {
            //出金手续费
            $withdrawFee = config('withdraw_rate');
            $feeAmount = bcmul($validated['num'], $withdrawFee, 2) / 100;
            $aAmount = bcsub($validated['num'], $feeAmount, 2);

            $withdraw = new Withdraw();
            $withdraw->no = $withNo;
            $withdraw->user_id = $user->id;
            $withdraw->type = 1;
            $withdraw->address = $validated['address'];
            $withdraw->num = $validated['num'];
            $withdraw->fee = $withdrawFee;
            $withdraw->fee_amount = $feeAmount;
            $withdraw->ac_amount = $aAmount;
            $withdraw->created_at = date('Y-m-d H:i:s');
            $withdraw->updated_at = date('Y-m-d H:i:s');
            $withdraw->save();

            $afterAmount = $user->amount;
            $beforeAmount = bcsub($user->amount, $validated['num'], 2);
            $user->amount = $beforeAmount;
            $user->save();

            IncomeLog::query()->insert([
                'user_id' => $user->id,
                'amount_type' => 1,
                'type' => 10,
                'after' => $beforeAmount,
                'total' => '-' . $validated['num'],
                'before' => $afterAmount,
                'remark' => '提现到收益',
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            return responseJson();
        } catch (\Exception $e) {
            Cache::lock($key)->forceRelease();
            DB::rollBack();
            return responseJsonAsServerError($e->getMessage() . $e->getLine());
        }
    }


    public function list(RechargeListValidate $request)
    {
        $validated = $request->validated();
        $query = Withdraw::query()->where('user_id', auth()->id())
            ->select(['type', 'num', 'address', 'fee', 'fee_amount', 'ac_amount', 'status', 'created_at']);

        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->orderBy('id', 'desc')
            ->get();
        return responseJson(compact('total', 'list'));
    }


    /**
     * 赎回质押
     * @param RedemptionValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function redemption(RedemptionValidate $request)
    {
        Redis::del('redemption:key:' . auth()->id());
        if (!Redis::setnx('redemption:key:' . auth()->id(), 1)) {
            return responseValidateError('操作频繁');
        }
        $validated = $request->validated();
        $model = InvestPledgeLog::query()->with(['invest', 'invest.coin'])
            ->where(['user_id' => auth()->id(), 'id' => $validated['id'], 'status' => 1])->first();
        if (empty($model)) {
            Redis::del('redemption:key:' . auth()->id());
            return responseValidateError('未找到该质押记录');
        }
        $user = auth()->user();
        DB::beginTransaction();
        try {
            $model->status = 2;
            $model->save();
            try {
                $http = new Client();
                $data = [
                    'address' => $user->name,
                    'amount' => $model->total_num,
                    'contract_address' => $model->invest->coin->contract_address,
                    'notify_url' => '',
                    'remarks' => '赎回质押'
                ];
                $response = $http->post('http://127.0.0.1:9090/v1/bnb/withdraw', [
                    'form_params' => $data
                ]);

                $result = json_decode($response->getBody()->getContents(), true);
                if (!isset($result['code']) || $result['code'] != 200) {
                    DB::rollBack();
                    Redis::del('redemption:key:' . $user->id);
                    Log::channel('withdraw')->info('提交赎回申请失败' . var_export($data, true) . '---' . var_export($result, true));
                    return responseValidateError('赎回失败');
                }
            } catch (\Exception $e) {
                Log::channel('withdraw')->info('提交赎回申请失败' . var_export($data, true) . '---' . var_export($result, true));
                DB::rollBack();
                Redis::del('redemption:key:' . $user->id);
                return responseValidateError('赎回失败');
            }
            DB::commit();
            Redis::del('redemption:key:' . $user->id);
            return responseJson([]);
        } catch (\Exception $e) {
            DB::rollBack();
            Redis::del('redemption:key:' . $user->id);
            return responseValidateError('赎回失败' . $e->getMessage());
        }


    }

}
