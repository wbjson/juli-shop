<?php

namespace App\Http\Controllers\Api;

use App\Http\Validate\Address\AddressDefaultRequest;
use App\Http\Validate\Address\AddressDestroyRequest;
use App\Http\Validate\Address\AddressDetailRequest;
use App\Http\Validate\Address\AddressInsertRequest;
use App\Http\Validate\Address\AddressUpdateRequest;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{

    public function index()
    {
        $addresses = auth()->user()->addresses;

        // Provincial and municipal regions
        foreach ($addresses as &$v){
            $v->path = $v->format();
        }
        return responseJson($addresses);
    }

    public function show(AddressDetailRequest $request)
    {
        $address = Address::query()->where('user_id',auth()->id())
            ->where('id',$request->input('id'))
            ->select('id','name','phone','province','city','district','address','is_default')
            ->first();
        return responseJson($address);
    }

    public function store(AddressInsertRequest $request)
    {
        $addressesData = $this->getFormatRequest($request);
        $user = auth()->user();
        if ($request->input('is_default')==1){
            Address::query()->where('user_id',$user->id)->update([
                'is_default' => 0
            ]);
        }
        $user->addresses()->create($addressesData);
        return responseJson();
    }


    public function update(AddressUpdateRequest $request)
    {
        $user = auth()->user();
        $address = Address::query()->where('id',$request->input('id'))->where('user_id',$user->id)->first();
        if (empty($address)){
            return responseJsonAsBadRequest('非法操作');
        }
        if ($request->input('is_default')==1){
            Address::query()->where('user_id',$user->id)->where('id','!=',$request->input('id'))->update([
                'is_default' => 0
            ]);
        }
        $addressesData = $this->getFormatRequest($request);

        $address->update($addressesData);
        return responseJson();
    }

    public function destroy(AddressDestroyRequest $request)
    {
        $user = auth()->user();
        $address = Address::query()->where('id',$request->input('id'))->where('user_id',$user->id)->first();
        if (empty($address)){
            return responseJsonAsBadRequest('非法操作');
        }
        $address->delete();
        return responseJson();
    }


    public function setDefaultAddress(AddressDefaultRequest $request)
    {
        $address = Address::query()->where('id',$request->input('id'))->where('user_id',auth()->id())->first();
        if (empty($address)) {
            return responseJsonAsBadRequest('非法操作');
        }

        Address::query()->where('user_id', $address->user_id)->update(['is_default' => 0]);
        $address->is_default = 1;

        if ($address->save()) {
            return responseJson();
        }

        return responseValidateError('请稍后再试！');
    }

    /**
     * 获取默认地址
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDefaultAddress(){
        $address = Address::query()->where(['user_id'=>auth()->id(),'is_default'=>1])->first();
        if (empty($address)){
            $address = Address::query()->where(['user_id'=>auth()->id()])->first();;
        }
        if(empty($address)){
            return responseValidateError('请设置收货地址');
        }
        return responseJson($address);
    }


    protected function getFormatRequest(Request $request)
    {
        return $request->only(['name', 'phone', 'province', 'city','district','address','is_default']);
    }


    public function getCities(Request $request)
    {
        return DB::table('cities')->where('province_id', $request->input('province_id'))->get();
    }
}
