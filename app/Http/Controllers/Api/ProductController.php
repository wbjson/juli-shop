<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Validate\Product\ProductDetailValidate;
use App\Http\Validate\Product\ProductExhibitValidate;
use App\Http\Validate\Product\ProductListValidate;
use App\Models\BrowHistory;
use App\Models\CollectGood;
use App\Models\Product;
use App\Models\ProductExhibit;
use App\Models\ProductsCategory;
use App\Models\UserCollect;

class ProductController extends Controller
{

    /**
     * 推荐列表
     * @param ProductExhibitValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exhibit(ProductExhibitValidate $request)
    {
        $validated = $request->validated();
        $query = ProductExhibit::query()->with(['img' => function ($query) {
            $query->select(['id', 'path']);
        }])->where('type', $validated['type'])->select(['product_id', 'img_id']);

        //判断是不是随机,如果是随机，随机生成几个ID
        if (isset($validated['is_range']) && $validated['is_range'] > 0) {
            $exhibitIds = ProductExhibit::query()->pluck('id')->toArray();
            shuffle($exhibitIds);
            $query->whereIn('id', $exhibitIds);
        }
        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->where('status', 1)
            ->orderBy('orders', 'desc')
            ->get();
        return responseJson(compact('total', 'list'));
    }

    /**
     * 商品类型列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function productCategory()
    {
        $cid = request()->post('cateid', '0');

        $cateList = ProductsCategory::query()
            ->where('status', 1)
            ->where('level', 1)
            ->where(function ($query) use ($cid) {
                if ($cid > 0) {
                    $query->where('id', $cid);
                }
            })
            ->orderByDesc('sortorder')
            ->select(['id', 'name', 'icon'])
            ->get();
        if (!$cateList->isEmpty()) {
            foreach ($cateList as $v) {
                $v->child_cate = ProductsCategory::query()
                    ->where('parent_id', $cid)
                    ->select(['id', 'name', 'icon'])
                    ->orderByDesc('sortorder')
                    ->get();
            }
        }

        return responseJson($cateList);
    }

    /**
     * @return void
     * 商品分类详情
     */
    public function categoryDetail()
    {
        $cid = request()->post('cateid', 1);
        $query_str = request()->post('query_name', '');

        $cateInfo = ProductsCategory::query()
            ->where('id', $cid)
            ->select(['id', 'name', 'icon'])
            ->orderByRaw('sort')
            ->first();

        $cateList = ProductsCategory::query()
            ->where('status', 1)
            ->where('level', 2)
            ->where(function ($query) use ($cid) {
                if ($cid > 0) {
                    $query->where('parent_id', $cid);
                }
            })
            ->orderByRaw('sort')
            ->select(['id', 'name', 'parent_id', 'icon'])
            ->get();

        if ($cateList->isEmpty()) {
            $cateidArr = [$cid];
        } else {
            $cateidArr = ProductsCategory::query()->where('parent_id', $cid)->pluck('id');
            if ($cateidArr->isEmpty()) {
                $cateidArr = [$cid];
            }
        }

        $goodsList = Product::query()
            ->where('status', 1)
            ->whereIn('category_id', $cateidArr)
            ->where(function ($query) use ($query_str) {
                if (!empty($query_str)) {
                    $query->where('name', 'like', '%' . $query_str . '%');
                }
            })
            ->orderByRaw('sort')
            ->select(['id', 'name', 'sales', 'type', 'main_picture', 'price', 'type'])
            ->limit(40)
            ->get();
        return responseJson(compact('cateInfo', 'cateList', 'goodsList'));
    }


    /**
     * 产品列表
     * @param ProductListValidate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productList(ProductListValidate $request)
    {
        $validated = $request->validated();
        $query = Product::query()
            ->where('status', 1)
            ->where('type', 1)
            ->select(['id', 'name', 'main_picture', 'price', 'sales']);

        if (isset($validated['category_id']) && $validated['category_id'] > 0) {
            $query->where('category_id', $validated['category_id']);
        }
        if (isset($validated['query_str']) && strlen($validated['query_str'])) {
            $query->where('name', 'like', $validated['query_str']);
        }
        $total = $query->count();
        $list = $query->offset(($validated['page'] - 1) * $validated['size'])
            ->limit($validated['size'])
            ->where('status', 1)
            ->where('type', 1)
            ->orderBy('id', 'desc')
            ->get();
        return responseJson(compact('total', 'list'));
    }


    public function detail(ProductDetailValidate $request)
    {
        $userId = auth()->id();
        $validated = $request->validated();
        $product = Product::query()
            ->where('id', $validated['product_id'])
            ->select('id', 'name', 'main_picture', 'type', 'picture', 'price', 'sales', 'stock', 'remark', 'content')
            ->where('status', 1)
            ->first();
        if (empty($product)) {
            return responseValidateError('请选择正确的商品');
        }
        $product->iscollect = UserCollect::query()
            ->where('user_id', $userId)
            ->where('product_id', $validated['product_id'])
            ->count();
        /*添加浏览记录*/
        if (!empty($product)) {
            $isset = BrowHistory::query()
                ->where('product_id', $product->id)
                ->where('datestr', date('Y-m-d'))
                ->exists();
            if (!$isset && auth()->id() > 0) {
                BrowHistory::query()->insertGetId([
                    'product_id' => $product->id,
                    'user_id' => auth()->id(),
                    'product_img' => $product->main_picture,
                    'datestr' => date('Y-m-d'),
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            }

        }


        return responseJson($product);
    }

    /*积分商品*/
    public function pointGoods(){
        $goodsList = Product::query()
            ->where('status', 1)
            ->orderByRaw('sort')
            ->select(['id', 'name', 'sales', 'type', 'main_picture', 'price', 'type'])
            ->limit(80)
            ->get();
        return responseJson($goodsList);
    }
}
