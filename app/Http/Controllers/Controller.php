<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
        $lang = request()->header('lang','zh_CN');
        App::setLocale($lang);
        $dayStart = strtotime('00:00:00');
        $dayOperation = User::query()->where('id',auth()->id())->value('line_date');
        if($dayOperation < $dayStart){
            User::query()->where('id',auth()->id())->update(['line_date'=>time()]);
        }
    }

}
