<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (env('SIGN_ENABLE')===true){
            $now = time();
            $time = $request->post('time',0);
            $sign = $request->post('sign','');
            if (empty($time) || abs($now-$time)>300 || empty($sign)){
                return responseJsonAsServerError('请求已过期');
            }

            $params = $request->except(['sign','image']);
            ksort($params);
            $str = '';
            foreach($params as $k => $v) {
                if (is_array($v)){
                    continue;
                }
                // $v 为 array 递归拼接
                $str .= $k . $v;
            }
            $str .= env('SIGN_KEY', '1234567890');
            $sign = md5($str);

            if ($sign != $request->post('sign')) {
                return responseJsonAsServerError('非法请求');
            }
        }

        return $next($request);
    }
}
