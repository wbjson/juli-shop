<?php

namespace App\Http\Middleware;

use App\Helpers\ApiResponse;
use App\Helpers\BaseCode;
use Closure;
use Illuminate\Http\Request;

class CheckUserLogin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->check()){
            return responseJson([],406,'请登录');
        }

        return $next($request);
    }
}
