<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckCallback
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //判断IP
        $trustIp = [
            '47.242.37.129','172.18.0.1','127.0.0.1'
        ];
        if (!in_array($request->ip(),$trustIp)){
//            exit('非信任请求');
        }
        return $next($request);
    }
}
