<?php


namespace App\Http\Validate\Address;


use App\Http\Validate\BaseValidate;

class AddressDetailRequest extends BaseValidate
{



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
        ];
    }

}
