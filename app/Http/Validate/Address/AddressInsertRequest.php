<?php


namespace App\Http\Validate\Address;


use App\Http\Validate\BaseValidate;
use App\Rules\PhoneNumber;

class AddressInsertRequest extends BaseValidate
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone'=>['required', new PhoneNumber()],
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'address' => 'required',
            'is_default' => 'required|in:0,1'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '收货人名字不能为空',
            'phone.regex' => '手机号码格式不正确',
            'province.required' => '省区不能为空',
            'city.required' => '城市不能为空',
            'district.required' => '区/镇不能为空',
            'address.required' => '详细收货地址不能为空',
        ];
    }

}
