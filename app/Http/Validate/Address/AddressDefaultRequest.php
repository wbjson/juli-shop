<?php


namespace App\Http\Validate\Address;


use App\Http\Validate\BaseValidate;

class AddressDefaultRequest extends BaseValidate
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => '请选择对应收货地址',
        ];
    }

}
