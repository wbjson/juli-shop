<?php


namespace App\Http\Validate\Address;


use App\Http\Validate\BaseValidate;

class AddressUpdateRequest extends BaseValidate
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'   => 'required|integer',
            'name' => 'required',
            'phone' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'address' => 'required',
            'is_default' => 'required|in:0,1'
        ];
    }


}
