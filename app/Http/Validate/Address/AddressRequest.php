<?php

namespace App\Http\Validate\Address;


use App\Http\Validate\BaseValidate;
use App\Rules\PhoneNumber;

class AddressRequest extends BaseValidate
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone'=>['required', new PhoneNumber()],
            'province_id' => 'required',
            'city_id' => 'required',
            'detail_address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '收货人名字不能为空',
            'phone.regex' => '手机号码格式不正确',
            'province_id.required' => '省区不能为空',
            'city_id.required' => '城市不能为空',
            'detail_address.required' => '详细收货地址不能为空',
        ];
    }
}
