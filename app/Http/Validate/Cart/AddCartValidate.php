<?php

namespace App\Http\Validate\Cart;

use App\Http\Validate\BaseValidate;

class AddCartValidate extends BaseValidate
{


    public function rules(){
        return [
            'product_id' => 'required|integer',
            'quantity' => 'required|integer',
        ];
    }
}
