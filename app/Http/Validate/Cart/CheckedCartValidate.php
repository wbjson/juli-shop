<?php

namespace App\Http\Validate\Cart;

use App\Http\Validate\BaseValidate;

class CheckedCartValidate extends BaseValidate
{


    public function rules(){
        return [
            'cart_id' => 'required|integer',
            'type' => 'required|in:0,1'
        ];
    }
}
