<?php

namespace App\Http\Validate\Cart;

use App\Http\Validate\BaseValidate;

class UpdateNumCartValidate extends BaseValidate
{


    public function rules(){
        return [
            'cart_id' => 'required|integer',
            'type' => 'required|in:1,2'
        ];
    }
}
