<?php

namespace App\Http\Validate\Cart;

use App\Http\Validate\BaseValidate;

class DestroyCartValidate extends BaseValidate
{


    public function rules(){
        return [
            'cart_id' => 'required|string',
        ];
    }
}
