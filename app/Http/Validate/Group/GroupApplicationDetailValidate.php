<?php

namespace App\Http\Validate\Group;

use App\Http\Validate\BaseValidate;

class GroupApplicationDetailValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'id' => 'required|integer'
        ];

    }

}
