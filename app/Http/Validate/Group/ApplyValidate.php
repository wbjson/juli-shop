<?php

namespace App\Http\Validate\Group;

use App\Http\Validate\BaseValidate;

class ApplyValidate extends BaseValidate
{


    public function rules(){

        return [
            'group_id' => 'required|integer',
            'country' => 'requiredIf:group_id,1,2,3,4,5|max:100',
            'province' => 'requiredIf:group_id,2,3,4,5|max:50',
            'city' => 'requiredIf:group_id,3,4,5|max:50',
            'district' => 'requiredIf:group_id,4,5|max:50',
            'address' => 'requiredIf:group_id,5|max:100',
        ];

    }

}
