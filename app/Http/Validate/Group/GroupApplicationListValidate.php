<?php

namespace App\Http\Validate\Group;

use App\Http\Validate\BaseValidate;

class GroupApplicationListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];

    }

}
