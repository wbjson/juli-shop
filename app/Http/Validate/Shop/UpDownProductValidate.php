<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class UpDownProductValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'product_id' => 'required|integer',
            'type' => 'required|in:0,1'
        ];

    }

}
