<?php


namespace App\Http\Validate\Shop;


use App\Http\Validate\BaseValidate;

class ProductListValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'status' => 'nullable|in:-1,0,1,2,3',
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];
    }

}
