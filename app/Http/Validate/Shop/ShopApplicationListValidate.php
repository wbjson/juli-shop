<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class ShopApplicationListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];

    }

}
