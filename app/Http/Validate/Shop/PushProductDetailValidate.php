<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class PushProductDetailValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'product_id' => 'required|integer',
        ];

    }

}
