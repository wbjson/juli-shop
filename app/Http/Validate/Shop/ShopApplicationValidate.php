<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class ShopApplicationValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'name' => 'required|max:100',
            'username' => 'required|max:100',
            'phone' => 'required|max:50',
            'shop_category_id' => 'required|integer',
            'legal_card1' => 'required|integer',
            'legal_card2' => 'required|integer',
            'company_logo' => 'required|integer',
            'company_zhi' => 'required|integer'
        ];

    }

}
