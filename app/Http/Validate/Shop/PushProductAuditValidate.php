<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class PushProductAuditValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'product_id' => 'required|integer',
        ];

    }

}
