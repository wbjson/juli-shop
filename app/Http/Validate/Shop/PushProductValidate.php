<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class PushProductValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'product_id' => 'nullable|integer',
            'category_id' => 'required|integer',
            'name' => 'required|max:100',
            'main_picture' => 'required|integer',
            'attr' => 'required',
            'picture' => 'required',
            'stock' => 'required|integer',
            'price' => 'required|regex:/^\d+(?:\.\d{1,2})?$/',
            'content' => 'required',
            'status' => 'required|in:0,-1',
            'is_audit_push' => 'required|in:1,2'
        ];

    }

}
