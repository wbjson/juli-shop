<?php

namespace App\Http\Validate\Shop;

use App\Http\Validate\BaseValidate;

class ShopApplicationDetailValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'id' => 'required|integer'
        ];

    }

}
