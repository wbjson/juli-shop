<?php


namespace App\Http\Validate\Recharge;

use App\Http\Validate\BaseValidate;

class PayOrderValidate extends BaseValidate
{

    public function rules(){
        return [
            'order_id'=>'required|integer',
            'num' => 'required',
            'hash' => 'required',
            'remarks' => 'required',
        ];
    }

}
