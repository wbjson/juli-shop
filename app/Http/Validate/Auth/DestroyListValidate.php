<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class DestroyListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];

    }

}
