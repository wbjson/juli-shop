<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class SetRemarkValidate extends BaseValidate
{


    public function rules(){

        return [
            'user_id' => 'required|integer',
            'remark' => 'required|max:100'
        ];

    }

}
