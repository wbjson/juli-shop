<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class UseCardValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'type' => 'required|in:destroy,lp',
            'id' => 'required|integer',
        ];

    }

}
