<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;
use App\Models\Country;
use App\Rules\PhoneNumber;

class RegisterValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'nickname' => 'required',
            'phone'=>['required', new PhoneNumber()],
            'password' => 'required|min:6|max:16',
            're_password' => 'required|same:password',
//            'trade_password' => 'required|min:6|max:16',
//            're_trade_password' => 'required|same:trade_password',
            'code' => 'required',
            'invite_code' => 'required',
            'platform' => 'required|in:1,2'
        ];

    }

    public function messages()
    {
        return [
            'nickname.required' => '请输入昵称',
            'phone.required' => '请输入手机号',
            'phone.regex' => '请输入正确的手机号码',
            'password.min' => '密码长度小于6位数',
            'password.required' => '请输入登陆密码',
            're_password.same' => '登陆密码不一致',
            'code.required' => '请输入验证码',
            'invite_code.required' => '请输入邀请码',
        ];
    }
}
