<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;
use App\Models\Country;
use Illuminate\Validation\Rule;

class LoginValidate extends BaseValidate
{

    public function rules(){

        return [
            'phone' => 'required',
            'password' => 'required',
            'platform' => 'required|in:1,2'
        ];
    }

}
