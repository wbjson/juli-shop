<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class UsersDestroyListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'type' => 'required|in:1,2',
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];

    }

}
