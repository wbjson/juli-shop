<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class SetTradePasswordValidate extends BaseValidate
{

    public function rules()
    {

        return [
            'old_password' => 'nullable|string',
            'password' => 'required|min:6|max:16',
            're_password' => 'required|same:password',
        ];

    }

    public function messages()
    {
        return [
            'password.required' => '交易密码必填',
            'password.min' => '密码长度小于6位数',
            're_password.required' => '确认交易密码必填',
            're_password.same' => '两次输入密码不一致',
        ];
    }
}
