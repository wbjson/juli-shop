<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class FindPasswordValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'phone' => 'required',
            'code' => 'required',
            'password' => 'required|min:6|max:16',
            're_password' => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => '请输入手机号',
            'code.required' => '请输入邀请码',
            'password.min' => '密码长度小于6位数',
            'password.required' => '请输入登陆密码',
            're_password.same' => '登陆密码不一致',
        ];
    }
}
