<?php

namespace App\Http\Validate\Auth;

use App\Http\Validate\BaseValidate;

class GetMachineCoinValidate extends BaseValidate
{

    public function rules(){
        return [
            'id' => 'required|integer',
            'num' => 'required|regex:/^\d+(?:\.\d{1,9})?$/',
            'type' => 'required|in:1,2'
        ];
    }

}
