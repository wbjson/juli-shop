<?php


namespace App\Http\Validate\Auth;


use App\Http\Validate\BaseValidate;

class UpdatePasswordValidate extends BaseValidate
{

    public function rules(){

        return [
            'password' => 'required',
            're_password' => 'required|same:password',
        ];

    }

}
