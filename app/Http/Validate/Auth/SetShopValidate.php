<?php

namespace App\Http\Validate\Auth;

use App\Http\Validate\BaseValidate;

class SetShopValidate extends BaseValidate
{

    public function rules(){
        return [
            'destroy_rate' => 'required|integer',
            'send_rate' => 'required|integer'
        ];
    }

}
