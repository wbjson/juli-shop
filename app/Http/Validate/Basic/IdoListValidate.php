<?php


namespace App\Http\Validate\Basic;


use App\Http\Validate\BaseValidate;

class IdoListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
        ];

    }

}
