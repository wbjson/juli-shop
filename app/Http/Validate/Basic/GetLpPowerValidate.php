<?php

namespace App\Http\Validate\Basic;

use App\Http\Validate\BaseValidate;

class GetLpPowerValidate extends BaseValidate
{


    public function rules(){
        return [
            '_id' => 'required',
            'lp_num' => 'required',
            'day' => 'required',
            'card_id' => 'required'
        ];
    }

}
