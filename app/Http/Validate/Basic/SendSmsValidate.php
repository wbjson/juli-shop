<?php

namespace App\Http\Validate\Basic;

use App\Http\Validate\BaseValidate;
use App\Rules\PhoneNumber;

class SendSmsValidate extends BaseValidate
{


    public function rules(){
        return [
            'phone'=>['required', new PhoneNumber()],
            'event' => 'required'
        ];
    }

}
