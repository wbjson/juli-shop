<?php

namespace App\Http\Validate\Transfer;

use App\Http\Validate\BaseValidate;

class TransferValidate extends BaseValidate
{


    public function rules(){
        return [
            'address' => 'required',
            'num' => 'required|regex:/^\d+(?:\.\d{1,2})?$/',
        ];
    }

}
