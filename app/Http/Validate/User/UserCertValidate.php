<?php


namespace App\Http\Validate\User;

use App\Http\Validate\BaseValidate;

class UserCertValidate extends BaseValidate
{

    public function rules(){
        return [
            'real_name'=>'required|string',
            'idcard_no'=>'required|string',
        ];
    }

    public function messages()
    {
        return [
            'real_name.required' => '真实姓名必填',
            'idcard_no.required' => '身份证号必填',
        ];
    }
}
