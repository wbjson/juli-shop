<?php

namespace App\Http\Validate\User;


use App\Http\Validate\BaseValidate;

class UserBankRequest extends BaseValidate
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable|integer',
            'bank_id' => 'required|integer',
            'bank_user' => 'required|string',
            'bank_phone' => 'regex:/^1[34578][0-9]{9}$/',
            'bank_no' => 'required|string',
            'is_default' => 'nullable|integer',

        ];
    }

    public function messages()
    {
        return [
            'bank_id.required' => '请选择银行信息',
            'bank_user.required' => '请输入开户名',
            'bank_no.required' => '请输入银行卡号',
            'bank_phone.required' => '请输入开户手机号',
            'bank_phone.regex' => '手机号码格式不正确',
        ];
    }
}
