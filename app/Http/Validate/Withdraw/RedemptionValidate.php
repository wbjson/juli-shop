<?php

namespace App\Http\Validate\Withdraw;

use App\Http\Validate\BaseValidate;

class RedemptionValidate extends BaseValidate
{

    public function rules(){
        return [
            'id' => 'required',
        ];

    }

}
