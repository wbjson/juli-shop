<?php

namespace App\Http\Validate\Withdraw;

use App\Http\Validate\BaseValidate;

class InputValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'address' => 'required',
            'num' => 'required|regex:/^\d+(?:\.\d{1,2})?$/',
        ];
    }

}
