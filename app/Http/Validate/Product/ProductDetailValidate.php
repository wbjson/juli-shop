<?php


namespace App\Http\Validate\Product;


use App\Http\Validate\BaseValidate;

class ProductDetailValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'product_id' => 'required|integer',
        ];
    }

}
