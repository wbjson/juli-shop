<?php


namespace App\Http\Validate\Product;


use App\Http\Validate\BaseValidate;

class ProductExhibitValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
            'type' => 'required|in:1,2',
            'is_range' => 'nullable'
        ];
    }

}
