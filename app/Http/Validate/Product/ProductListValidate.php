<?php


namespace App\Http\Validate\Product;


use App\Http\Validate\BaseValidate;

class ProductListValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
            'category_id' => 'nullable',
            'query_name' => 'nullable'
        ];
    }

}
