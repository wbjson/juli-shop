<?php


namespace App\Http\Validate\Product;


use App\Http\Validate\BaseValidate;

class ProductCollectValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'product_id' => 'required|integer',
            'type' => 'required|integer|in:1,2',
        ];
    }

}
