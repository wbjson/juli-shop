<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class OrderListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'status' => 'nullable',
            'page' => 'required|integer',
            'size' => 'required|integer'
        ];

    }


}
