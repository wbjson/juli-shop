<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class GenerateOrderValidate extends BaseValidate
{


    public function rules()
    {
        return [
            'address_id' => 'required|integer',

            'product_id' => 'required|array|min:1',
            'product_id.*' => 'required|integer|distinct',

            'product_count' => 'required|array|min:1',
            'product_count.*' => 'required|integer',

            'pay_remark' => 'nullable|string',

        ];

    }


}
