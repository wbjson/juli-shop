<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class ShopOrderListValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'status' => 'nullable|in:1,2,3,4',
            'page' => 'required|integer',
            'size' => 'required|integer'
        ];

    }


}
