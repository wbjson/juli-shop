<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class ReturnsDetailsOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'order_id' => 'required|integer',
        ];

    }


}
