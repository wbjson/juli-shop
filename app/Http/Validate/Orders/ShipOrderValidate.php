<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class ShipOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'order_id' => 'required|integer',
            'ship_company' => 'required|max:100',
            'ship_no' => 'required|max:200',
        ];

    }


}
