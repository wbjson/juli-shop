<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class SubmitReturnsOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'return_id' => 'required|integer',
        ];

    }


}
