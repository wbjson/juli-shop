<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class AppraiseOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'order_id' => 'required|integer',
            'info' => 'required|string',
            'level' => 'required|in:-1,0,1',
            'desc_star' => 'required|in:1,2,3,4,5',
            'logistics_star' => 'required|in:1,2,3,4,5',
            'attitude_star' => 'required|in:1,2,3,4,5',
        ];

    }


}
