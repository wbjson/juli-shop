<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class ReturnsApplyOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'order_id' => 'required|integer',
            'product_status' => 'required|in:0,1',
            'state' => 'required|in:0,1',
            'why' => 'required|max:255',
        ];

    }


}
