<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class ReturnsOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'return_id' => 'required|integer',
            'ship_company' => 'nullable|max:200',
            'ship_no' => 'nullable|max:200',
        ];

    }


}
