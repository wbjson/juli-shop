<?php

namespace App\Http\Validate\Orders;

use App\Http\Validate\BaseValidate;

class AuditReturnsOrderValidate extends BaseValidate
{


    public function rules()
    {

        return [
            'return_id' => 'required|integer',
            'status' => 'required|in:1,-1',
            'audit_why' => 'requiredIf:status,-1|max:200',
            'username' => 'requiredIf:status,1|max:200',
            'phone' => 'requiredIf:status,1|max:200',
            'address' => 'requiredIf:status,1|max:200',
        ];

    }


}
