<?php

namespace App\Console\Commands;

use App\Models\LoseSign;
use App\Models\User;
use Illuminate\Console\Command;

class SignEpmtyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:signEmpty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清空签到可领取金额';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userList = User::query()->where('sign_money', '>', 0)->pluck('id');
        if ($userList->isEmpty()) {
            return;
        }

        foreach ($userList as $v) {
            $signNum = User::query()->where('id', $v)->value('sign_money');
            LoseSign::query()->insertGetId([
                'user_id' => $v,
                'num' => $signNum,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
        User::query()->whereIn('id', $userList)->where('sign_money', '>', 0)->update(['sign_money' => 0]);

    }
}
