<?php

namespace App\Console\Commands;

use App\Models\IncomeLog;
use App\Models\LevelConfig;
use App\Models\ReleaseOrder;
use App\Models\ShareSet;
use App\Models\SpeedOrder;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReleaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:coinRelease';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每日积分释放';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dateYmd = date('Ymd');
        $list = ReleaseOrder::query()
            ->where('status', 1)
            ->where('reset_num', '>', 0)
//            ->where('release_date', '<', $dateYmd)
            ->orderByDesc('user_id')
            ->get();
        if ($list->isEmpty()) {
            exit();
        }
        $releaseRate = config('release_rate');
        $maxeLevel = LevelConfig::query()->max('level');
        foreach ($list as $v) {
            $awardNum = bcmul($v->total_num, $releaseRate, 2) / 100;
            if ($awardNum >= $v->reset_num) {
                $awardNum = $v->reset_num;
            }
            if ($awardNum <= 0) {
                continue;
            }

            DB::beginTransaction();
            try {
                $userInfo = User::query()
                    ->where('id', $v->user_id)
                    ->select('id', 'phone', 'level', 'amount', 'parent_id', 'coin_money', 'static_income', 'total_income', 'sign_money', 'speed_date')
                    ->first();
                if ($userInfo->coin_money <= 0) {
                    continue;
                }
                if ($userInfo->coin_money <= $awardNum) {
                    $awardNum = $userInfo->coin_money;
                }
                $beforeAmount = $userInfo->sign_money;
                $userInfo->coin_money = bcsub($userInfo->coin_money, $awardNum, 2);
                $userInfo->sign_money = bcadd($userInfo->sign_money, $awardNum, 2);
                $userInfo->static_income = bcadd($userInfo->static_income, $awardNum, 2);
                $userInfo->save();

                IncomeLog::query()->insertGetId([
                    'user_id' => $userInfo->id,
                    'from_id' => $userInfo->id,
                    'amount_type' => 5,
                    'type' => 6,
                    'before' => $beforeAmount,
                    'total' => $awardNum,
                    'after' => $userInfo->sign_money,
                    'remark' => '冻结积分' . $v->total_num . '释放到可签到领取金额',
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                if ($userInfo->parent_id > 0) {
                    /*推荐收益*/
                    if ($userInfo->level < $maxeLevel) {
                        $dateYmd = date('Ymd');
                        $releaseOrder = SpeedOrder::query()
                            ->where('user_id', $userInfo->id)
                            ->where('start_date', '<=', $dateYmd)
                            ->where('speed_date', '>=', $dateYmd)
                            ->exists();
                        if ($releaseOrder) {
                            ShareSet::share_coin_award($userInfo->id, $awardNum);
                        }
                    }
                }

                $profitedNum = bcadd($v->released_num, $awardNum, 2);
                $resetNum = bcsub($v->reset_num, $awardNum, 2);

                $updateData = [];
                $updateData['released_num'] = $profitedNum;
                $updateData['reset_num'] = $resetNum;
                $updateData['release_date'] = $dateYmd;
                if ($awardNum >= $v->reset_num) {
                    $updateData['status'] = 2;
                }

                ReleaseOrder::query()->where('id', $v->id)->where('user_id', $v->user_id)->update($updateData);
                DB::commit();
            } catch (\Exception $e) {
                Log::channel('coin-release')->info('释放失败' . $e->getLine() . '====' . $e->getMessage());
                DB::rollBack();
            }
        }

    }
}
