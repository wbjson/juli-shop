<?php

namespace App\Console\Commands;

use App\Models\Config;
use App\Models\CurrencyExchange;
use App\Models\InvestLp;
use App\Models\LpMiningPool;
use App\Models\MainCurrency;
use App\Models\MainCurrencyExchange;
use App\Units\CoinPriceHelper;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class SearchPriceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:search_price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '自动更新币种价格';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currencyExchange = MainCurrencyExchange::query()->with(['from','to'])->where('is_search',1)->get();
        foreach ($currencyExchange as $item){
            $price = $this->searchPrice($item->from->contract_address,$item->to->contract_address);
            if ($price!==false && $price!=0){
                $item->rate = $price;
                $item->save();
            }

            $fanPrice = $this->searchPrice($item->to->contract_address,$item->from->contract_address);
            if ($price!==false && $price!=0){
                $item->fan_rate = $fanPrice;
                $item->save();
            }
        }


        //查询单个LP价格
        $investList = LpMiningPool::query()->where('search_status',1)->get();
        $client = new Client();
        foreach ($investList as $item){
            $response = $client->post('http://127.0.0.1:9090/v1/bnb/lpInfo',[
                'form_params' => [
                    'contract_address' => $item->lp_address,
                ]
            ]);
            $lpResponse = json_decode($response->getBody()->getContents(),true);
            $per = bcmul(bc_mul(1/($lpResponse['data']['totalSupply']/pow(10,18)),$lpResponse['data']['reserve1']/pow(10,18),9),2,9);
            $item->lp_rate = $per;
            $item->save();
        }
    }



    public function searchPrice($token1,$token2){
        $qry = ['query'=>'{
  ethereum(network: bsc) {
    dexTrades(
      options: {desc: ["block.height","tradeIndex"], limit: 1}
      exchangeName: {in: ["Pancake", "Pancake v2"]}
      baseCurrency: {is: "'.$token1.'"}
      quoteCurrency: {is: "'.$token2.'"}
      date: {after: "2022-01-02"}
    ) {
      transaction {
        hash
      }
      tradeIndex
      smartContract {
        address {
          address
        }
        contractType
        currency {
          name
        }
      }
      tradeIndex
      block {
        height
      }
      baseCurrency {
        symbol
        address
      }
      quoteCurrency {
        symbol
        address
      }
      quotePrice

    }
  }
}'];
        try {
            $client = new Client();
            $response = $client->post('https://graphql.bitquery.io',[
                'headers' => [
                    'X-API-KEY' => 'BQYvhnv04csZHaprIBZNwtpRiDIwEIW9',
                ],
                'json' => $qry
            ]);
            $result = json_decode($response->getBody()->getContents(),true);
            $price = empty($result['data']['ethereum']['dexTrades']) ? 0 : $result['data']['ethereum']['dexTrades'][0]['quotePrice'];
            return sprintf('%.10f',$price);
        }catch (\Exception $e){
            return false;
        }
    }
}
