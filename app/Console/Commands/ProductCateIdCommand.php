<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\ProductsCategory;
use Illuminate\Console\Command;

class ProductCateIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getProductCateOne';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '获取商品一级分类';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dataList = Product::query()->where('category_one','<=',0)->orderByRaw('id')->get();
        if($dataList->isEmpty()){
            return;
        }
        foreach ($dataList as $item){
            $cateOne = ProductsCategory::query()->where('id',$item->category_id)->value('parent_id');
            $item->category_one = $cateOne;
            $item->save();
        }

    }
}
