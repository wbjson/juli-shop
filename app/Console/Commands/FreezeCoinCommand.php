<?php

namespace App\Console\Commands;

use App\Models\IncomeLog;
use App\Models\User;
use Illuminate\Console\Command;

class FreezeCoinCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:unfreeze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '解冻股东收益';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dayDate = date('Ymd');
        $dayNo = date('d');
        if ($dayNo != 1) {
            return;
        }
        $dataList = User::query()
            ->where('freeze_coin', '>', 0)
//            ->where('unfreeze_date', '<', $dayDate)
            ->orderBy('id')
            ->get();
        if ($dataList->isEmpty()) {
            return true;
            exit();
        }

        $nextDate = date("Ymd", strtotime("+30 day"));
        foreach ($dataList as $v) {
            $unfreezeNum = $v->freeze_coin;

            $beforeCoin = $v->coin_money;
            $afterCoin = bcadd($beforeCoin, $unfreezeNum, 2);
            User::query()->where('id', $v->id)->update(['coin_money' => $afterCoin, 'freeze_coin' => 0, 'unfreeze_date' => $nextDate]);


            IncomeLog::query()->insertGetId([
                'user_id' => $v->id,
                'from_id' => $v->id,
                'amount_type' => 2,
                'type' => 9,
                'before' => $beforeCoin,
                'total' => $unfreezeNum,
                'after' => $afterCoin,
                'remark' => '股东收益积分解冻' . $unfreezeNum,
                'created_at' => date('Y-m-d H:i:s')
            ]);

        }
    }
}
