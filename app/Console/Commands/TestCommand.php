<?php

namespace App\Console\Commands;

use App\Jobs\UpdateDynamicPowerJob;
use App\Models\BlackHole;
use App\Models\Config;
use App\Models\IncomeLog;
use App\Models\InvestDestroyLog;
use App\Models\InvestLpLog;
use App\Models\InvestPledgeLog;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '测试';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //更新所有用户 质押算力，LP算力，销毁算力,业绩
        //更新所有用户的动态算力
        User::query()->where('id','>',0)->update([
            'performance' => 0
        ]);

        $userList = User::query()->orderByDesc('id')->get();
        foreach ($userList as $user){
            $parentId = [];
            $lpPower =  InvestLpLog::query()->where('user_id',$user->id)->where('status',1)->sum('power');
            $destroyAcPower = InvestDestroyLog::query()->where('user_id',$user->id)->where('status',1)->sum('power');

            $user->lp_power = $lpPower;
            $user->destroy_power = $destroyAcPower;
            $user->total_static_power = bc_add($lpPower,$destroyAcPower);

            $parentId = [];
            if (!empty($user->path)){
                $parentId = array_reverse(explode('-',trim($user->path,'-')));
            }
            $parentId = array_merge([$user->id],$parentId);
            //给所有上级加业绩
            User::query()->whereIn('id',$parentId)->increment('performance',$user->total_static_power);
            $user->save();
        }
        $userList = User::query()->orderByDesc('id')->pluck('id')->toArray();
        UpdateDynamicPowerJob::dispatch($userList);
    }
}
