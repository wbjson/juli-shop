<?php


namespace App\Logic;


use App\Models\IncomeLog;
use App\Models\LpMiningPool;
use App\Models\LpMiningPoolLog;
use App\Models\Order;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Models\Recharge;
use App\Models\ShareSet;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RechargeLogic
{

    /**
     * 检测需要调用什么方法
     * @param $data
     */
    public function checkMethod($data)
    {
        $this->orderHandle($data);
    }

    /**
     * 购买商品
     * @param $data
     * @return void
     */
    private function orderHandle($data)
    {
        Log::channel('recharge_callback')->info('开始处理支付订单请求');
        $remarks = $this->parseRemark($data['remarks']);
        $orderIds = $remarks[0];
        if ($orderIds <= 0) {
            Log::channel('recharge_callback')->info('未找到订单ID');
            exit();
        }

        $order = Order::query()
            ->where('id', $orderIds)
            ->where('order_status', '0')
            ->exists();
        if (!$order) {
            Log::channel('recharge_callback')->info('该订单不存在');
            exit();
        }


        DB::beginTransaction();
        try {

            $order = Order::query()
                ->where('id', $orderIds)
                ->where('order_status', '0')
                ->first();

            $order->total_usdt = $data['amount'];
            $order->order_status = 1;
            $order->payment_type = 1;
            $order->pay_time = date('Y-m-d H:i:s');
            $order->save();

            $orderList = OrdersDetail::query()
                ->where('order_id', $order->id)
                ->get();

            $coinPrice = config('lvy_price');

            if (!empty($orderList)) {
                foreach ($orderList as $v) {
                    Product::query()->where('id', $v->product_id)->increment('sales', $v->product_count);
                    Product::query()->where('id', $v->product_id)->decrement('stock', $v->product_count);

                    $user = User::query()->where('id', $v->user_id)->first();

                    if ($v->product_type == 1) {
                        $coinAmount = bcdiv($v->usdt_price, $coinPrice, 4);       //获得冻结LVY
                        $user->myperfor = bcadd($user->myperfor, $v->usdt_price, 4);   //我的USDT业绩
                        if ($user->share_level <= 0) {
                            $user->share_level = 1;
                        }
                        if ($user->activate <= 0) {
                            $user->activate = 1;
                        }
                        $user->save();

                        if ($user->parent_id > 0) {
                            /*推荐等级*/
                            ShareSet::share_level($user->id,$v->usdt_price);
                            /*推荐收益*/
                            ShareSet::share_award($user->id,$v->usdt_price,$coinAmount);
                        }
                    }

                    OrdersDetail::query()->where('id', $v->id)->update(['order_status' => 1]);
                }
            }

            $user = User::query()->where('name', $data['from_address'])->first();
            if($orderList[0]->product_type == 1){
                $coinAmount = 600;       //获得冻结LVY
            }else{
                $coinAmount = bcdiv($data['amount'], $coinPrice, 4);       //获得冻结LVY
            }

            $beforeAmount = $user->freeze_coin;
            $user->freeze_coin = bcadd($user->freeze_coin, $coinAmount, 4);   //冻结积分累计
            $user->save();

            IncomeLog::query()->insertGetId([
                'user_id' => $user->id,
                'amount_type' => 2,
                'type' => 1,
                'before' => $beforeAmount,
                'total' => $coinAmount,
                'after' => $user->freeze_coin,
                'remark' => '购买商品' . $data['amount'] . 'USDT',
                'created_at' => date('Y-m-d H:i:s'),
            ]);


            Recharge::query()->insert([
                'user_id' => $user->id,
                'name' => $user->name,
                'type' => 1,
                'chargeid' => $order->id,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'num' => $data['amount'],
                'hash' => $data['hash'],
                'remarks' => $data['remarks'],
                'status' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'finished_at' => date('Y-m-d H:i:s')
            ]);
            DB::commit();
            Log::channel('recharge_callback')->info('结束处理支付订单请求');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理支付订单请求失败' . $e->getLine() . $e->getMessage());
        }
    }

    /**
     * 解析参数
     * @param $remarks
     * @return false|string[]
     */
    private function parseRemark($remarks)
    {
        return explode('-', $remarks);
    }

    /**
     * LP矿池  lp@矿池ID@数量
     * @param $data
     */
    private function lpHandle($data)
    {
        Log::channel('recharge_callback')->info('开始处理LP矿池质押请求');
        $remarks = $this->parseRemark($data['remarks']);
        //方案ID
        $pool = LpMiningPool::query()->where('id', $remarks[1])->first();
        if (empty($pool)) {
            Log::channel('recharge_callback')->info('无法找到矿池');
            exit;
        }
        $user = User::query()->where('name', $data['to_address'])->first();
        DB::beginTransaction();
        try {
            $power = bcmul($data['amount'], $pool->per, 5);
            LpMiningPoolLog::query()->insert([
                'user_id' => $user->id,
                'pool_id' => $pool->id,
                'lp_num' => $data['amount'],
                'rate' => $pool->per,
                'power' => $power,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            Recharge::query()->insert([
                'user_id' => $user->id,
                'type' => 2,
                'order_no' => 'R' . date('YmdHis') . mt_rand(10000, 99999),
                'coin' => strtoupper($data['coin_token']),
                'other_coin' => strtolower($data['coin_token1']),
                'nums' => $data['amount'],
                'other_nums' => $data['amount1'],
                'hash' => $data['hash'],
                'status' => 2,
                'finish_time' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $pool->total = bcadd($pool->total, $data['amount'], 5);
            $pool->save();
            $user->lp_num = bcadd($user->lp_num, $data['amount'], 2);
            $user->save();
            Db::commit();
            Log::channel('recharge_callback')->info('处理LP矿池质押完成');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::channel('recharge_callback')->info('处理LP矿池质押请求失败' . $e->getMessage() . $e->getLine());
        }
    }

}
