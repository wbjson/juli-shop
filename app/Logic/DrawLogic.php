<?php


namespace App\Logic;


class DrawLogic {

    public function getAwardresult($prizeArr,$index='probability')
    {
        $result = '';
        $randArr = [];
        $i = 1;
        foreach($prizeArr as $item){
            $randArr[$i] = $item[$index]*10;
            $i++;
        }
        //概率数组的总概率精度
        $proSum = array_sum($randArr);
        //概率数组循环
        foreach ($randArr as $key => $val) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $val) {
                $result = $key;
                break;
            } else {
                $proSum -= $val;
            }
        }
        return $prizeArr[$result-1]['id'];
    }

}
