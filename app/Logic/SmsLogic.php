<?php

namespace App\Logic;

use App\Http\Validate\Basic\PushJobValidate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class SmsLogic
{


    /**
     * 检查验证码是否正确
     * @param $phone
     * @param $event
     * @param $code
     * @return bool
     */
    public function checkCode($phone,$event,$code){
        if (env('APP_DEBUG')===true && $code==999888){
            return true;
        }
        if (Redis::get($phone.':'.$event)==$code){
            return true;
        }
        return false;
    }

    /**
     * 发送短信
     * @param $phone
     * @param $event
     * @return bool|mixed
     */
    public function sendSms($phone,$event){
//        $code = mt_rand(100000,999999);
//        $url = "http://121.201.57.213/sms.aspx";
//        $data = [
//            'action'   => 'send',
//            'userid'   => '1111',
//            'account'  => 'yongdong',
//            'password' => '123456',
//            'mobile'   => $phone,
//            'content'  => $this->getMessage($event,$code),
//        ];
//        $url = "http://112.74.47.72/smsJson.aspx";
//        $data = [
//            'action'   => 'send',
//            'userid'   => '41',
//            'account'  => 'baobeiw',
//            'password' => 'baobeiw1',
//            'mobile'   => $phone,
//            'content'  => $this->getMessage($event,$code),
//        ];
//        $this_header = [
//            "content-type: application/x-www-form-urlencoded;charset=UTF-8"
//        ];
//        $result = $this->curlPost($url,$data,$this_header);
//        $result = json_decode($result,true);
//        if ($result['returnstatus'] == 'Success') {
//            Redis::setex($phone.':'.$event,300,$code);
//            Log::channel('sms')->info('短信发送成功'.var_export($data,true).var_export($result,true));
//            return true;
//        } else {
//            Log::channel('sms')->info('短信发送失败'.var_export($data,true).var_export($result,true));
//            return '发送失败';
//        }
        $code = mt_rand(1000,9999);
        $jsonArr = ['code'=>$code];
        $url = "http://v.juhe.cn/sms/send";
        $params = array(
            'key'   => '7498b61e1a8888b90eeeff7d3214b118', //您申请的APPKEY
            'mobile'    => $phone, //接受短信的用户手机号码
            'tpl_id'    => '256854', //您申请的短信模板ID，根据实际情况修改
            'vars' => json_encode($jsonArr,true) //模板变量键值对的json类型字符串，根据实际情况修改
        );
        $paramstring = http_build_query($params);
        $content = $this->juheCurl($url, $paramstring);

        if ($content) {
            $result = json_decode($content, true);
            if ($result && is_array($result) && isset($result['error_code'])) {
                if ($result['error_code']==0) {
                    Redis::setex($phone.':'.$event,300,$code);
                    Log::channel('sms')->info('短信发送成功'.var_export($params,true).var_export($result,true));
                    return true;
                } else {
                    Log::channel('sms')->info('短信发送失败1'.var_export($params,true).var_export($result,true));
                    return '发送失败';
                }
            } else {
                Log::channel('sms')->info('短信发送失败2'.var_export($params,true).var_export($result,true));
                return '发送失败';
            }
        } else {
            Log::channel('sms')->info('短信发送失败3'.var_export($params,true));
            return '发送失败';
        }
    }

    /**
     * 根据事件获取短信文本
     * @param $event
     * @return string|void
     */
    private function getMessage($event,$code){
        switch ($event){
            case 'register':
                return '【CoreskyChat】'.'验证码'.$code.' ，用于手机验证码登录，5分钟内有效。';
            case 'findPassword':
                return '【CoreskyChat】'.'验证码'.$code.' ,用于找回密码，5分钟内有效。';
        }
    }

    private function xmlToArray($xml)
    {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $values;
    }

    private function curlPost($url, $params = [], $headers = [])
    {
        header("Content-Type:text/html;charset=utf-8");
        $ch = curl_init();//初始化
        curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        return ($data);
    }




    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    function juheCurl($url, $params = false, $ispost = 0)
    {
        $httpInfo = array();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'JuheData');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                curl_setopt($ch, CURLOPT_URL, $url.'?'.$params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        $response = curl_exec($ch);
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        return $response;
    }

}
