<?php

namespace App\Admin\Forms;

use App\Models\Config;

use App\Models\IncomeLog;
use App\Models\Order;
use App\Models\QuickCardUseLog;
use App\Models\User;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class DeliveryOrderForm extends Form implements LazyRenderable
{

    use LazyWidget; // 使用异步加载功能
    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'];

        if(empty($input['ship_company'])){
            return $this
                ->response()
                ->error('请填写快递公司')
                ->refresh();
        }
        if(empty($input['ship_no'])){
            return $this
                ->response()
                ->error('请填写快递单号')
                ->refresh();
        }


        DB::beginTransaction();
        try {
            $order = Order::query()->where('id',$id)->first();
            if($order->order_status != 1){
                return $this
                    ->response()
                    ->error('该订单状态已修改')
                    ->refresh();
            }

            $order->ship_company = $input['ship_company'];
            $order->ship_no = $input['ship_no'];
            $order->delivery_time = date('Y-m-d H:i:s');
            $order->order_status = 2;
            $order->save();
            DB::commit();

            return $this
                ->response()
                ->success('发货成功.')
                ->refresh();

        }catch (\Exception $e){
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.'.$e->getMessage())
                ->refresh();
        }

    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->text('ship_company','快递公司')->required();
        $this->text('ship_no','快递单号')->required();
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
        ];
    }
}
