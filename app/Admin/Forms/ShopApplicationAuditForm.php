<?php

namespace App\Admin\Forms;
use App\Models\IncomeLog;
use App\Models\RechargeApply;
use App\Models\RechargeLog;
use App\Models\ShopApplication;
use App\Models\User;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class ShopApplicationAuditForm extends Form implements LazyRenderable
{

    use LazyWidget; // 使用异步加载功能

    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'];
        $model = ShopApplication::query()->where('id',$id)->first();
        if ($model->status!=0){
            return $this
                ->response()
                ->error('该数据无需重复审核')
                ->refresh();
        }
        DB::beginTransaction();
        try {
            if ($input['audit_status'] == 1){
                $user = User::query()->where('id',$model->user_id)->first();
                if ($user->is_shop > 0){
                    return $this
                        ->response()
                        ->error('该用户已是商户,无需重复审核')
                        ->refresh();
                }
                $user->is_shop = $model->shop_category_id;
                $user->save();
            }
            $model->audit_status = $input['audit_status'];
            $model->audit_remarks = $input['audit_remarks'];
            $model->save();

            DB::commit();
            return $this
                ->response()
                ->success('操作成功.')
                ->refresh();
        }catch (\Exception $e){
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.'.$e->getMessage())
                ->refresh();
        }
    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->radio('audit_status','审核结果')->options([1=>'审核成功',2=>'审核不通过'])->required();
        $this->textarea('audit_remarks','审核备注');
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [

        ];
    }
}
