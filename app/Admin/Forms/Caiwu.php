<?php

namespace App\Admin\Forms;

use App\Models\IncomeLog;
use App\Models\User;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class Caiwu extends Form implements LazyRenderable
{

    use LazyWidget;

    // 使用异步加载功能

    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'];
        if ($input['amount_type'] <= 0) {
            return $this
                ->response()
                ->error('请选择类型')
                ->refresh();
        }
        if ($input['recharge_type'] <= 0) {
            return $this
                ->response()
                ->error('请选择增减')
                ->refresh();
        }
        DB::beginTransaction();
        try {

            IncomeLog::query()->insert([
                'user_id' => $id,
                'total' => $input['recharge_type'] == 1 ? $input['amount'] : '-' . $input['amount'],
                'type' => 0,
                'amount_type' => $input['amount_type'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            if ($input['amount_type'] == 1) {
                if ($input['recharge_type'] == 1) {
                    User::query()->where('id', $id)->increment('amount', $input['amount']);
                } else {
                    User::query()->where('id', $id)->decrement('amount', $input['amount']);
                }
            }else{
                if ($input['recharge_type'] == 1) {
                    User::query()->where('id', $id)->increment('coin_money', $input['amount']);
                } else {
                    User::query()->where('id', $id)->decrement('coin_money', $input['amount']);
                }
            }
            DB::commit();

            return $this
                ->response()
                ->success('操作成功.')
                ->refresh();

        } catch (\Exception $e) {
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.' . $e->getMessage())
                ->refresh();
        }

    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->radio('amount_type', '金额类型')->options([1 => '余额']);
        $this->radio('recharge_type', '充值类型')->options([1 => '增加', 2 => '减少']);
        $this->decimal('amount', '金额');
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
        ];
    }
}
