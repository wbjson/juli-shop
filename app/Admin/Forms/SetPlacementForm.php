<?php

namespace App\Admin\Forms;

use App\Admin\Repositories\IncomeLog;
use App\Models\LevelConfig;
use App\Models\Otc;
use App\Models\Pledge;
use App\Models\User;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class SetPlacementForm extends Form implements LazyRenderable
{

    use LazyWidget; // 使用异步加载功能
    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'] ?? null;

        $model = Otc::query()->where('id',$id)->first();

        $model->status = 3;
        $model->save();

        $user = User::query()->where('id',$model->user_id)->first();
        $user->bth = bcadd($user->bth,$model->total,2);
        $user->save();
        \App\Models\IncomeLog::insertLog($user->id,$model->total,'bth',6);

        return $this
            ->response()
            ->success('设置成功')
            ->refresh();



    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->radio('status','状态')->options([2=>'待完成',3=>'已完成'])->required();
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [

        ];
    }
}
