<?php

namespace App\Admin\Forms;

use App\Models\IncomeLog;
use App\Models\User;
use App\Models\UserCert;
use App\Models\Withdraw;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class UserCertApplyAuditForm extends Form implements LazyRenderable
{

    use LazyWidget;

    // 使用异步加载功能

    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id    = $this->payload['id'];
        $model = UserCert::query()->where('id', $id)->first();
        if ($model->status != 0) {
            return $this
                ->response()
                ->error('该数据无需重复审核')
                ->refresh();
        }
        DB::beginTransaction();
        try {
            if ($input['status'] == 1) {
                User::query()->where('id', $model->user_id)->update(['cert_status'=>1]);
            }
            $model->finshed_at = date('Y-m-d H:i:s');
            $model->status     = $input['status'];
            $model->save();

            DB::commit();
            return $this
                ->response()
                ->success('操作成功.')
                ->refresh();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.' . $e->getMessage())
                ->refresh();
        }
    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->radio('status', '审核结果')->options([1 => '审核成功', 2 => '审核失败'])->required();
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
        ];
    }
}
