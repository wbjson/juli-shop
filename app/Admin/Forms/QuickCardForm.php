<?php

namespace App\Admin\Forms;

use App\Models\Config;

use App\Models\IncomeLog;
use App\Models\QuickCardUseLog;
use App\Models\User;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class QuickCardForm extends Form implements LazyRenderable
{

    use LazyWidget; // 使用异步加载功能
    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'];

        DB::beginTransaction();
        try {
            User::query()->where('id',$id)->increment('quick_num',$input['num']);

            QuickCardUseLog::query()->insert([
                'user_id' => $id,
                'num' => $input['num'],
                'type' => 0,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            DB::commit();

            return $this
                ->response()
                ->success('操作成功.')
                ->refresh();

        }catch (\Exception $e){
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.'.$e->getMessage())
                ->refresh();
        }

    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->number('num','数量');
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
        ];
    }
}
