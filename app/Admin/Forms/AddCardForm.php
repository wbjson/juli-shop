<?php

namespace App\Admin\Forms;

use App\Models\Card;
use App\Models\Config;

use App\Models\IncomeLog;
use App\Models\QuickCardUseLog;
use App\Models\User;
use App\Models\UsersCard;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;

class AddCardForm extends Form implements LazyRenderable
{

    use LazyWidget; // 使用异步加载功能
    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $id = $this->payload['id'];

        DB::beginTransaction();
        try {
            $card = Card::query()->where('id',$input['card_id'])->first();
            for ($i=0;$i<$input['num'];$i++){
                $userCard = new UsersCard();
                $userCard->user_id = $id;
                $userCard->img = $card->img;
                $userCard->name = $card->name;
                $userCard->rate = $card->rate;
                $userCard->created_at = date('Y-m-d H:i:s');
                $userCard->save();
            }
            DB::commit();

            return $this
                ->response()
                ->success('操作成功.')
                ->refresh();

        }catch (\Exception $e){
            DB::rollBack();
            return $this
                ->response()
                ->error('操作失败.'.$e->getMessage())
                ->refresh();
        }

    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->radio('card_id','卡牌')->options(Card::query()->pluck('name','id')->toArray());
        $this->number('num','数量');
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
        ];
    }
}
