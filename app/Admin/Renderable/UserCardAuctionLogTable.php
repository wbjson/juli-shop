<?php


namespace App\Admin\Renderable;


use App\Models\CardAuctionLog;
use Dcat\Admin\Grid;
use Dcat\Admin\Grid\LazyRenderable;

class UserCardAuctionLogTable extends LazyRenderable
{


    public function grid(): Grid
    {
        return Grid::make(CardAuctionLog::with(['user'])->where('auction_id',$this->payload['key']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.name','用户');
            $grid->column('amount','支付金额');
            $grid->column('price','竞拍价位');
            $grid->column('status','状态')->using([
                0 => '未开奖',
                1 => '竞拍失败',
                2 => '竞拍成功'
            ]);
            $grid->column('created_at','竞拍时间');


            $grid->disableCreateButton();
            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->model()->orderBy('id','desc');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.name','用户');
                $filter->equal('status','状态')->select([
                    0 => '未开奖',
                    1 => '竞拍失败',
                    2 => '竞拍成功'
                ]);
                $filter->between('created_at','竞拍时间')->datetime();
            });
        });
    }

}
