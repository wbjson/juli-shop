<?php

namespace App\Admin\Renderable;

use App\Models\LandPoolLog;
use App\Models\PoolLog;
use Dcat\Admin\Grid;
use Dcat\Admin\Grid\LazyRenderable;

class PoolLogTable extends LazyRenderable
{


    public function grid(): Grid
    {
        return Grid::make(PoolLog::query()->with(['user'])->where('pool_id',$this->payload['key']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.name','来源用户');
            $grid->column('amount','数量');
            $grid->column('type')->using([
                1 => '投资',
                2 => '分配'
            ]);
            $grid->column('created_at')->sortable();


            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableActions();
            $grid->model()->orderByDesc('id');
        });
    }

}
