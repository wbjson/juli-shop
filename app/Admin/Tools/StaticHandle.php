<?php

namespace App\Admin\Tools;

use Dcat\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;

class StaticHandle extends AbstractTool
{

    /**
     * 按钮样式定义，默认 btn btn-white waves-effect
     *
     * @var string
     */
    protected $style = 'btn btn-success waves-effect';


    /**
     * 按钮文本
     *
     * @return string|void
     */
    public function title()
    {
        return '执行积分释放';
    }

    /**
     *  确认弹窗，如果不需要则返回空即可
     *
     * @return array|string|void
     */
    public function confirm()
    {
        return ['您确定要执行执行积分释放吗？', '确认'];
    }

    /**
     * 处理请求
     * 如果你的类中包含了此方法，则点击按钮后会自动向后端发起ajax请求，并且会通过此方法处理请求逻辑
     *
     * @param Request $request
     */
    public function handle(Request $request)
    {
        // 你的代码逻辑
        Artisan::call('command:coinRelease');
        return $this->response()->success('执行成功')->refresh();
    }

    /**
     * 设置请求参数
     *
     * @return array|void
     */
    public function parameters()
    {
        return [

        ];
    }

}
