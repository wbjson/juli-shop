<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\MainCurrencyExchange;
use App\Models\MainCurrency;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class MainCurrencyExchangeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new MainCurrencyExchange(), function (Grid $grid) {
            $grid->number();
            $grid->column('from_coin_name');
            $grid->column('to_coin_name');
            $grid->column('rate');
            $grid->column('fan_rate','反向汇率');
            $grid->column('is_search','自动更新价格')->using([0=>'关闭',1=>'开启']);
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new MainCurrencyExchange(), function (Form $form) {
            $form->select('from_coin')->options(MainCurrency::query()->pluck('name','id'));
            $form->select('to_coin')->options(MainCurrency::query()->pluck('name','id'));
            $form->decimal('rate');
            $form->decimal('fan_rate','反向汇率');
            $form->switch('is_search','自动查询价格');
            $form->hidden('from_coin_name');
            $form->hidden('to_coin_name');

            $form->saving(function (Form $form){
                $form->from_coin_name = MainCurrency::query()->where('id',$form->from_coin)->value('name');
                $form->to_coin_name = MainCurrency::query()->where('id',$form->to_coin)->value('name');

            });
        });
    }
}
