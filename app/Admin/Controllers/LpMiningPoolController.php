<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\LpMiningPool;
use App\Models\Card;
use App\Models\MainCurrency;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;

class LpMiningPoolController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(LpMiningPool::with(['currency1','currency2']), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('apr');
            $grid->column('coin1')->using(MainCurrency::query()->pluck('name','id')->toArray());
            $grid->column('coin2')->using(MainCurrency::query()->pluck('name','id')->toArray());
            $grid->column('lp_address','LP合约地址');
            $grid->column('per','单个LP价值');
            $grid->column('is_search','自动更新价值')->switch();
            $grid->column('total','累计质押');
            $grid->column('multiple','收益倍数');
            $grid->column('created_at');

            $grid->model()->orderByDesc('id');
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
        });
    }



    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new LpMiningPool(), function (Form $form) {
            $form->text('name')->required();
            $form->rate('apr')->required();
            $form->select('coin1')->options(MainCurrency::query()->pluck('name','id'))->required();
            $form->select('coin2')->options(MainCurrency::query()->pluck('name','id'))->required();
            $form->text('lp_address','LP合约地址')->required();
            $form->number('lp_decimals','LP合约精度')->required();
            $form->decimal('per','单个LP价值')->required();
            $form->switch('is_search','自动更新价值')->required();
            $form->decimal('total','累计质押')->required();
        });
    }
}
