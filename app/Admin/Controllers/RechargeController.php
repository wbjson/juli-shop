<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Recharge;
use App\Models\Machine;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class RechargeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Recharge::with(['user']), function (Grid $grid) {
            $grid->number();
            $grid->column('order_no');
            $grid->column('user.name','用户');
            $grid->column('type','充值类型')->using([
                1 => 'IDO',
                2 => '质押挖矿'
            ]);
            $grid->column('nums','充值数量');
            $grid->column('nums','充值数量');
            $grid->column('hash','充值hash');
            $grid->column('status','状态')->using([1=>'待确认',2=>'已完成',3=>'已失败'])->label();
            $grid->column('finish_time');
//            $grid->column('order_type','充值类型')->using([1=>'首次',2=>'复购'])->label();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderBy('id','desc');
            $grid->disableCreateButton();
            $grid->disableActions();
            $grid->disableRowSelector();

            $grid->export();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.name','用户');
                $filter->equal('type','类型')->select([
                    1 => 'IDO',
                    2 => '质押挖矿'
                ]);
                $filter->equal('status','状态')->select([1=>'待确认',2=>'已完成',3=>'已失败']);
            });
        });
    }

}
