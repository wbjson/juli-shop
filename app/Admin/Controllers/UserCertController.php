<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\UserCertApplyAudit;
use App\Admin\Forms\UserCertApplyAuditForm;
use App\Admin\Repositories\UserCert;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;

class UserCertController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(\App\Models\UserCert::with(['user']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id','UID');
            $grid->column('user.phone','手机号');
            $grid->column('user.nickname','昵称');
            $grid->column('real_name');
            $grid->column('idcard_no');
            $grid->column('status','状态')->using([0=>'待审核',1=>'已通过',2=>'已拒绝']);
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid){
                if ($this->status == 0){
                    $actions->append(new UserCertApplyAudit());
                }
            });


            $grid->disableRowSelector();
            $grid->disableEditButton();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone','手机号');
                $filter->equal('user.nickname','昵称');
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new UserCert(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('real_name');
            $show->field('idcard_no');
            $show->field('status');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new UserCert(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('real_name');
            $form->text('idcard_no');
            $form->text('status');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
