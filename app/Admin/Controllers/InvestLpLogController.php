<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\InvestLpLog;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class InvestLpLogController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(InvestLpLog::with(['user']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.name','用户');
            $grid->column('main_coin','质押币种');
            $grid->column('lp_num','质押数量');
            $grid->column('status','状态')->using([
                1 => '有效',
                2 => '已赎回'
            ])->badge();
            $grid->column('income','总收益');
            $grid->column('invest_id');
            $grid->column('before_power','基础算力');
            $grid->column('power','总算力');
            $grid->column('status');
            $grid->column('created_at')->sortable();

            $grid->model()->orderByDesc('id');
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableActions();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.name','用户');
                $filter->equal('status','状态')->select([
                    1 => '有效',
                    2 => '已赎回'
                ]);
                $filter->between('created_at','创建时间')->datetime();
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new InvestLpLog(), function (Show $show) {
            $show->field('id');
            $show->field('income');
            $show->field('invest_id');
            $show->field('main_coin');
            $show->field('status');
            $show->field('user_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new InvestLpLog(), function (Form $form) {
            $form->display('id');
            $form->text('income');
            $form->text('invest_id');
            $form->text('main_coin');
            $form->text('status');
            $form->text('user_id');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
