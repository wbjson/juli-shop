<?php

namespace App\Admin\Controllers;

use App\Models\LevelConfig;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;

class LevelConfigController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new LevelConfig(), function (Grid $grid) {
            $grid->column('name');
            $grid->column('teamperfor', '团队业绩');
            $grid->column('team_vip', '团队等级数量');
            $grid->column('rate', '收益比例(%)');
            $grid->column('same_level_rate', '平级奖(%)');

            $grid->column('coin_rate', '积分商城收益(%)');
            $grid->column('coin_level_rate', '积分商城平级收益(%)');

            $grid->column('same_level_count', '平级次数');
            $grid->column('created_at');

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new LevelConfig(), function (Form $form) {
            $form->text('name')->required();
            $form->number('teamperfor', '团队业绩');
            $form->number('team_vip', '团队等级数量');
            $form->rate('rate', '收益');
            $form->rate('same_level_rate', '平级奖');

            $form->rate('coin_rate', '积分收益');
            $form->rate('coin_level_rate', '积分平级收益');

            $form->number('same_level_count', '平级次数');
        });
    }
}
