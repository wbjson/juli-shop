<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Cart;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class CartController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Cart(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id');
            $grid->column('product_id');
            $grid->column('quantity');
            $grid->column('is_checked');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Cart(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('product_id');
            $show->field('quantity');
            $show->field('is_checked');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Cart(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('product_id');
            $form->text('quantity');
            $form->text('is_checked');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
