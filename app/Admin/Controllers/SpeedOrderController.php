<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\SpeedOrder;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class SpeedOrderController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(SpeedOrder::with(['user']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id','UID');
            $grid->column('user.phone','手机号');
            $grid->column('user.nickname','昵称');
            $grid->column('day','加速天数');
            $grid->column('cycle','轮次');
            $grid->column('start_date','开始周期');
            $grid->column('end_date','结束周期');
            $grid->column('speed_date','加速之后');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();


            $grid->disableRowSelector();
            $grid->disableActions();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone','手机号');
                $filter->equal('user.nickname','昵称');
                $filter->equal('user_id','UID');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new SpeedOrder(), function (Show $show) {
            $show->field('id');
            $show->field('day');
            $show->field('speed_date');
            $show->field('speed_id');
            $show->field('user_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new SpeedOrder(), function (Form $form) {
            $form->display('id');
            $form->text('day');
            $form->text('speed_date');
            $form->text('speed_id');
            $form->text('user_id');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
