<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Order;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrderListController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Order::with(['userinfo']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('userinfo.phone','账号');
            $grid->column('userinfo.nickname','昵称');
            $grid->column('order_no','订单编号');
            $grid->column('order_money','总价');

            $grid->column('order_status','订单状态')->using([
                0=>'未付款',1=>'已付款',2=>'已发货',3=>'已签收'
            ])->label('orange');

            $grid->column('pay_time','支付时间');
            $grid->column('created_at');
            $grid->column('delivery_time');

            $grid->model()->where('order_status',0);
            $grid->model()->orderByDesc('id');

            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();
            $grid->disableEditButton();

            $grid->export()->rows(function (array $rows) {
                $statusArr = [
                    0=>'未付款',1=>'已付款',2=>'已发货',3=>'已签收'
                ];
                foreach ($rows as $index => &$row) {
                    $row['order_status'] = $statusArr[$row['order_status']];
                }
                return $rows;
            });

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('userinfo.phone','下单手机');
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Order(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('after_status');
            $show->field('delivery_time');
            $show->field('logistics_fee');
            $show->field('order_amount_total');
            $show->field('order_no');
            $show->field('order_settlement_status');
            $show->field('order_settlement_time');
            $show->field('order_status');
            $show->field('pay_remarks');
            $show->field('payment_type');
            $show->field('product_amount_total');
            $show->field('product_count');
            $show->field('product_price');
            $show->field('shopping_id');
            $show->field('supplier_name');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Order(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('after_status');
            $form->text('delivery_time');
            $form->text('logistics_fee');
            $form->text('order_amount_total');
            $form->text('order_no');
            $form->text('order_settlement_status');
            $form->text('order_settlement_time');
            $form->text('order_status');
            $form->text('pay_remarks');
            $form->text('payment_type');
            $form->text('product_amount_total');
            $form->text('product_count');
            $form->text('product_price');
            $form->text('shopping_id');
            $form->text('supplier_name');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
