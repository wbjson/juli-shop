<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\RecommendList;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class RecommendListController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new RecommendList(), function (Grid $grid) {
            $grid->column('id')->sortable();

            $grid->column('name','类型');

            $grid->column('icon','图标')->image(env('APP_URL').'/uploads/',100,100);
            $grid->column('status','状态')->switch();

            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->model()->orderByRaw('type');


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new RecommendList(), function (Show $show) {
            $show->field('id');
            $show->field('type');
            $show->field('icon');
            $show->field('status');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new RecommendList(), function (Form $form) {
            $form->display('id');
            $form->image('icon','icon')->disk('admin')->uniqueName()->maxSize(10240)->accept('jpg,png,gif,jpeg')->required()->autoUpload();

            $form->text('name','类型')->required();
            $form->switch('status','状态')->required();
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
