<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\GroupCategory;
use App\Models\LevelConfig;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class GroupCategoryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new GroupCategory(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name');
            $grid->column('rate')->display(function (){
                return $this->rate.'%';
            });
            $grid->column('lp_factor')->display(function (){
                return '≥'.$this->lp_factor;
            });
            $grid->column('grade_factor')->display(function ($grade){
                return explode(',',$grade);
            })->map(function($levelId){
                return LevelConfig::query()->where('id',$levelId)->value('name');
            })->badge();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new GroupCategory(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('rate');
            $show->field('lp_factor');
            $show->field('grade_factor');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new GroupCategory(), function (Form $form) {
            $form->text('name')->required();
            $form->rate('rate')->required();
            $form->decimal('lp_factor')->required();
            $form->checkbox('grade_factor')->options(LevelConfig::query()->pluck('name','id')->toArray())->saving(function ($value){
                return implode(',',$value);
            })->required();

        });
    }
}
