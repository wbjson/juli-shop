<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\MainCurrency;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Cache;

class MainCurrencyController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new MainCurrency(), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('coin_img')->image(env('APP_URL').'/storage',50,50);
            $grid->column('contract_address');
            $grid->column('precision');
            $grid->column('created_at')->sortable();
            $grid->column('updated_at');
            $grid->model()->orderBy('id','desc');

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
            });
        });
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new MainCurrency(), function (Form $form) {
            $form->text('name');
            $form->image('coin_img')->autoUpload()->uniqueName();
            $form->text('contract_address');
            $form->number('precision');
            $form->saving(function (Form $form){
                $form->name = strtoupper($form->name);
            });

            $form->saved(function (){
                Cache::forget('mainCurrencyList');
            });
        });
    }
}
