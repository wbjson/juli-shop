<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\WithdrawApplyAudit;
use App\Admin\Repositories\Withdraw;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;

class WithdrawController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Withdraw::with(['user']), function (Grid $grid) {
            $grid->number();
            $grid->column('no');
            $grid->column('user.phone', '手机号');
            $grid->column('user.nickname', '昵称');
            $grid->column('address', '地址');
            $grid->column('num', '提现数量');
            $grid->column('fee', '手续费率');
            $grid->column('fee_amount', '手续费');
            $grid->column('ac_amount', '实际到账');
            $grid->column('status', '状态')->using([
                1 => '待打款',
                2 => '已完成',
                3 => '已退回'
            ])->label();
//            $grid->column('finsh_time');
            $grid->column('created_at')->sortable();
            $grid->export();


            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid){
                if ($this->status == 1){
                    $actions->append(new WithdrawApplyAudit());
                }
            });

            $grid->model()->orderBy('id', 'desc');

            $grid->disableCreateButton();
            $grid->disableRowSelector();
            $grid->disableViewButton();
            $grid->disableEditButton();
            $grid->disableDeleteButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone', '用户');
                $filter->equal('user.nickname', '昵称');
                $filter->equal('bank_user', '开户名');
                $filter->equal('bank_name', '开户行');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Withdraw(), function (Show $show) {
            $show->field('id');
            $show->field('no');
            $show->field('user_id');
            $show->field('receive_address');
            $show->field('dai_num');
            $show->field('usdt_num');
            $show->field('dai_rate');
            $show->field('fee');
            $show->field('fee_amount');
            $show->field('ac_amount');
            $show->field('status');
            $show->field('finsh_time');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Withdraw(), function (Form $form) {
            $form->display('id');
            $form->text('no');
            $form->text('user_id');
            $form->text('receive_address');
            $form->text('dai_num');
            $form->text('usdt_num');
            $form->text('dai_rate');
            $form->text('fee');
            $form->text('fee_amount');
            $form->text('ac_amount');
            $form->text('status');
            $form->text('finsh_time');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
