<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\RechargeApplyAudit;
use App\Admin\Actions\Grid\ShopApplicationAudit;
use App\Admin\Repositories\ShopApplication;
use App\Models\ShopCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ShopApplicationController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(ShopApplication::with(['user','category']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.name','用户');
            $grid->column('name');
            $grid->column('category.name','商户类型');
            $grid->column('phone');
            $grid->column('username');
            $grid->column('legal_card1_path','法人身份证正面')->image('',50,50);
            $grid->column('legal_card2_path','法人身份证反面')->image('',50,50);
            $grid->column('company_logo_path','公司营业执照')->image('',50,50);
            $grid->column('company_zhi_path','公司资质')->image('',50,50);
            $grid->column('audit_status','审核状态')->using([
                0 => '待审核',
                1 => '审核通过',
                2 => '审核不通过'
            ])->badge();
            $grid->column('created_at')->sortable();

            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableEditButton();
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->model()->orderByDesc('id');


            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid){
                if ($this->status == 0){
                    $actions->append(new ShopApplicationAudit());
                }
            });

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.name','用户');
                $filter->like('username');
                $filter->equal('audit_status','审核状态')->radio([
                    0 => '待审核',
                    1 => '审核通过',
                    2 => '审核不通过'
                ]);
                $filter->equal('shop_category_id','商户类型')->select(ShopCategory::query()->pluck('name','id'));
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ShopApplication(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('name');
            $form->text('shop_category_id');
            $form->text('phone');
            $form->text('username');
            $form->text('legal_card2');
            $form->text('legal_card1');
            $form->text('company_logo');
            $form->text('company_zhi');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
