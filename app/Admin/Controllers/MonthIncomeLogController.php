<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\IncomeLog;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;

class MonthIncomeLogController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(IncomeLog::with(['user']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.phone', '用户');
            $grid->column('user.nickname', '昵称');
            $grid->column('user_id', 'UID');
            $grid->column('amount_type', '币种')->using([1 => '余额', 2 => '积分', 3 => '股东金',4=>'冻结积分']);
            $grid->column('before', '之前');
            $grid->column('total', '金额');
            $grid->column('after', '之后');
            $grid->column('type', '类型')->using([
                0 => '后台操作',
                1 => '购买商品',
                2 => '购买获赠积分',
                3 => '推广收益',
//                4 => 'VIP收益',
                5 => '股东收益',
                6 => '积分释放',
                7 => '直推收益',
                8 => '股东推荐收益',
                9 => '股东收益解冻',
                10 => '签到奖励',

            ])->label();
            $grid->column('remark', '说明');

            $grid->column('created_at')->sortable();
            $grid->model()->orderBy('id', 'desc');
            $grid->model()->whereMonth('created_at', date('m'));


            $grid->disableCreateButton();
            $grid->disableActions();
            $grid->disableRowSelector();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone', '用户名');

                $filter->equal('type', '类型')->select([
                    0 => '后台操作',
                    1 => '购买商品',
                    2 => '购买获赠积分',
                    3 => '推广收益',
//                4 => 'VIP收益',
                    5 => '股东收益',
                    6 => '积分释放',
                    7 => '直推收益',
                    8 => '股东推荐收益',
                    9 => '股东收益解冻',
                    10 => '签到奖励',
                ]);

                $filter->equal('amount_type', '资金类型')->select([
                    1 => '余额', 2 => '积分', 3 => '股东金',4=>'冻结积分'
                ]);

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new IncomeLog(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('usdb');
            $show->field('entt');
            $show->field('type');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new IncomeLog(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('usdb');
            $form->text('entt');
            $form->text('type');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
