<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Banner;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BannerController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Banner(), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('banner')->image(env('APP_URL').'/uploads/',100,100);
            $grid->column('type')->using([1=>'首页头部Banner',2=>'首页推荐Banner'])->badge();
            $grid->column('status','状态')->switch();
            $grid->column('sort')->editable(true);
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderBy('type','asc');
            $grid->model()->orderBy('id','desc');
            $grid->disableRowSelector();
            $grid->disableViewButton();
            $grid->disableRowSelector();


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
                $filter->equal('status','状态')->radio([
                    0 => '下架',
                    1 => '上架'
                ]);
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Banner(), function (Form $form) {
            $form->text('name')->required()->rules('max:250');
            $form->url('link','链接');
            $form->image('banner','Banner')->disk('admin')->uniqueName()->maxSize(10240)->accept('jpg,png,gif,jpeg')->required()->autoUpload();
            $form->radio('status','状态')->required()->options([0=>'下架',1=>'上架'])->default(1);
            $form->radio('type','类型')->required()->options([1=>'首页头部Banner',2=>'首页推荐Banner'])->default(1);
            $form->number('sort','排序')->required()->default(0);
        });
    }
}
