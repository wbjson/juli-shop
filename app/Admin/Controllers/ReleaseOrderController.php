<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ReleaseOrder;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ReleaseOrderController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(ReleaseOrder::with(['user']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id','UID');
            $grid->column('user.phone','手机号');
            $grid->column('user.nickname','昵称');
//            $grid->column('order_id');
            $grid->column('total_num','订单积分')->sortable();;
            $grid->column('released_num','已释放数量')->sortable();;
            $grid->column('reset_num','剩余数量')->sortable();;
            $grid->column('status','状态')->using([1=>'释放中',2=>'已完成'])->badge();
            $grid->column('release_date','释放时间');
            $grid->column('created_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableActions();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone', '手机号');
                $filter->equal('user.nickname', '昵称');
                $filter->equal('user_id', 'UID');
                $filter->equal('status','状态')->radio([1=>'释放中',2=>'已完成']);

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ReleaseOrder(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('order_id');
            $show->field('total_num');
            $show->field('released_num');
            $show->field('reset_num');
            $show->field('status');
            $show->field('release_date');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ReleaseOrder(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('order_id');
            $form->text('total_num');
            $form->text('released_num');
            $form->text('reset_num');
            $form->text('status');
            $form->text('release_date');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
