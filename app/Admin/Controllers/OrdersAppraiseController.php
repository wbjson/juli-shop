<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\OrdersAppraise;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrdersAppraiseController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new OrdersAppraise(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('attitude_star');
            $grid->column('desc_star');
            $grid->column('info');
            $grid->column('level');
            $grid->column('logistics_star');
            $grid->column('order_id');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new OrdersAppraise(), function (Show $show) {
            $show->field('id');
            $show->field('attitude_star');
            $show->field('desc_star');
            $show->field('info');
            $show->field('level');
            $show->field('logistics_star');
            $show->field('order_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new OrdersAppraise(), function (Form $form) {
            $form->display('id');
            $form->text('attitude_star');
            $form->text('desc_star');
            $form->text('info');
            $form->text('level');
            $form->text('logistics_star');
            $form->text('order_id');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
