<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\PointSet;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class PointSetController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new PointSet(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('start_money');
            $grid->column('end_money');
            $grid->column('rate');
            $grid->column('status','状态')->switch();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new PointSet(), function (Show $show) {
            $show->field('id');
            $show->field('start_money');
            $show->field('end_money');
            $show->field('rate');
            $show->field('status');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new PointSet(), function (Form $form) {
            $form->display('id');
            $form->text('start_money');
            $form->text('end_money');
            $form->text('rate');
            $form->text('status');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
