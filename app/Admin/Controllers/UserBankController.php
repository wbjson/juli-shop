<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\UserBank;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class UserBankController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new UserBank(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id');
            $grid->column('bank_id');
            $grid->column('bank_user');
            $grid->column('bank_name');
            $grid->column('bank_code');
            $grid->column('bank_phone');
            $grid->column('bank_no');
            $grid->column('is_default');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new UserBank(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('bank_id');
            $show->field('bank_user');
            $show->field('bank_name');
            $show->field('bank_code');
            $show->field('bank_phone');
            $show->field('bank_no');
            $show->field('is_default');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new UserBank(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('bank_id');
            $form->text('bank_user');
            $form->text('bank_name');
            $form->text('bank_code');
            $form->text('bank_phone');
            $form->text('bank_no');
            $form->text('is_default');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
