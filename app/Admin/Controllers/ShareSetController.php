<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ShareSet;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ShareSetController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new ShareSet(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('start_count','开始个数');
            $grid->column('end_count','结束个数');
            $grid->column('award_rate','收益比例(%)');
            $grid->column('status','状态')->switch();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ShareSet(), function (Show $show) {
            $show->field('id');
            $show->field('award_rate');
            $show->field('coin_release');
            $show->field('get_layer');
            $show->field('name');
            $show->field('status');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ShareSet(), function (Form $form) {
            $form->display('id');
            $form->number('start_count','直推开始个数');
            $form->number('end_count','直推结束个数');
            $form->rate('award_rate','收益比例');
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
