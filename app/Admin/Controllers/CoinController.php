<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Coin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class CoinController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Coin(), function (Grid $grid) {
            $grid->number();
            $grid->column('name','币种');
            $grid->column('rate','与USDT汇率');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderBy('id','desc');

            $grid->disableRowSelector();
            $grid->disableViewButton();
            $grid->disableDeleteButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Coin(), function (Form $form) {
            $form->text('name','币种');
            $form->decimal('rate','与USDT汇率');
        });
    }
}
