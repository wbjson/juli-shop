<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\WageMoney;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class WageMoneyController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new WageMoney(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('start','开始人数');
            $grid->column('end','结束人数');
            $grid->column('profit','工资数量');
            $grid->column('created_at');


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });


            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new WageMoney(), function (Show $show) {
            $show->field('id');
            $show->field('end');
            $show->field('profit');
            $show->field('start');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new WageMoney(), function (Form $form) {
            $form->display('id');
            $form->number('start','最小人数');
            $form->number('end','最大人数');
            $form->decimal('profit','收益数量');

        });
    }
}
