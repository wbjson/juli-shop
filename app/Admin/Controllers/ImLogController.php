<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ImLog;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ImLogController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new ImLog(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('uid');
            $grid->column('id_uid');
            $grid->column('money');
            $grid->column('type');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ImLog(), function (Show $show) {
            $show->field('id');
            $show->field('uid');
            $show->field('id_uid');
            $show->field('money');
            $show->field('type');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ImLog(), function (Form $form) {
            $form->display('id');
            $form->text('uid');
            $form->text('id_uid');
            $form->text('money');
            $form->text('type');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
