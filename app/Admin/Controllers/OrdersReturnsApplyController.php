<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\OrdersReturnsApply;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrdersReturnsApplyController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new OrdersReturnsApply(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('audit_time');
            $grid->column('audit_why');
            $grid->column('note');
            $grid->column('order_id');
            $grid->column('product_status');
            $grid->column('return_no');
            $grid->column('state');
            $grid->column('status');
            $grid->column('user_id');
            $grid->column('why');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new OrdersReturnsApply(), function (Show $show) {
            $show->field('id');
            $show->field('audit_time');
            $show->field('audit_why');
            $show->field('note');
            $show->field('order_id');
            $show->field('product_status');
            $show->field('return_no');
            $show->field('state');
            $show->field('status');
            $show->field('user_id');
            $show->field('why');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new OrdersReturnsApply(), function (Form $form) {
            $form->display('id');
            $form->text('audit_time');
            $form->text('audit_why');
            $form->text('note');
            $form->text('order_id');
            $form->text('product_status');
            $form->text('return_no');
            $form->text('state');
            $form->text('status');
            $form->text('user_id');
            $form->text('why');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
