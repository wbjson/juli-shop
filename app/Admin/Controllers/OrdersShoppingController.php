<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\OrdersShopping;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrdersShoppingController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new OrdersShopping(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('address');
            $grid->column('city');
            $grid->column('district');
            $grid->column('group_id');
            $grid->column('name');
            $grid->column('order_id');
            $grid->column('phone');
            $grid->column('province');
            $grid->column('user_id');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new OrdersShopping(), function (Show $show) {
            $show->field('id');
            $show->field('address');
            $show->field('city');
            $show->field('district');
            $show->field('group_id');
            $show->field('name');
            $show->field('order_id');
            $show->field('phone');
            $show->field('province');
            $show->field('user_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new OrdersShopping(), function (Form $form) {
            $form->display('id');
            $form->text('address');
            $form->text('city');
            $form->text('district');
            $form->text('group_id');
            $form->text('name');
            $form->text('order_id');
            $form->text('phone');
            $form->text('province');
            $form->text('user_id');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
