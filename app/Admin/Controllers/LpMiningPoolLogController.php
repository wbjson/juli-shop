<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\LpMiningPoolLog;
use App\Models\LpMiningPool;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class LpMiningPoolLogController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(LpMiningPoolLog::with(['user','invest']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.name','用户');
            $grid->column('invest.name','矿池名称');
            $grid->column('status','状态')->using([
                1 => '正常',
                2 => '已赎回'
            ]);
            $grid->column('lp_num');
            $grid->column('rate');
            $grid->column('power');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderByDesc('id');
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableDeleteButton();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.name');
                $filter->equal('pool_id')->select(LpMiningPool::query()->pluck('name','id'));
            });
        });
    }

}
