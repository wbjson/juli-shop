<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BrowHistory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BrowHistoryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BrowHistory(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('product_id');
            $grid->column('product_img');
            $grid->column('date');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BrowHistory(), function (Show $show) {
            $show->field('id');
            $show->field('product_id');
            $show->field('product_img');
            $show->field('date');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BrowHistory(), function (Form $form) {
            $form->display('id');
            $form->text('product_id');
            $form->text('product_img');
            $form->text('date');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
