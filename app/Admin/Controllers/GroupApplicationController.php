<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\GroupApplicationAudit;
use App\Admin\Repositories\GroupApplication;
use App\Models\GroupCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class GroupApplicationController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(GroupApplication::with(['group','user']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.phone','用户');
            $grid->column('group.name','社区类型');
            $grid->column('status')->using([-2 => '已失效',-1=>'审核失败',0=>'待审核',1=>'审核成功']);
            $grid->column('country');
            $grid->column('province');
            $grid->column('city');
            $grid->column('district');
            $grid->column('address');
            $grid->column('created_at')->sortable();

            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableEditButton();
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->model()->orderByDesc('id');

            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid){
                if ($this->status == 0){
                    $actions->append(new GroupApplicationAudit());
                }
            });


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone','用户');
                $filter->equal('audit_status','审核状态')->radio([
                    0 => '待审核',
                    1 => '审核通过',
                    2 => '审核不通过'
                ]);
                $filter->equal('shop_category_id','社区类型')->select(GroupCategory::query()->pluck('name','id'));

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new GroupApplication(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('status');
            $show->field('group_id');
            $show->field('country');
            $show->field('province');
            $show->field('city');
            $show->field('district');
            $show->field('address');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new GroupApplication(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('status');
            $form->text('group_id');
            $form->text('country');
            $form->text('province');
            $form->text('city');
            $form->text('district');
            $form->text('address');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
