<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\DeliveryOrder;
use App\Admin\Repositories\Order;
use App\Models\OrdersDetail;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;

class OrderController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Order::with(['userinfo', 'getaddress']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('userinfo.nickname', '昵称');
            $grid->column('userinfo.phone', '手机号');
            $grid->column('order_no');

            $grid->column('order_money', '总价');
            $grid->column('order_integral', '积分');
            $grid->column('get_user_name', '收件人');
            $grid->column('get_user_phone', '手机');
            $grid->column('get_user_info', '地址')->display(function (){
                $str = '省:'.$this->province.'/市:'.$this->city
                    .'区:'.$this->district.'-'.$this->address;
                return $str;
            });


            $grid->column('order_status', '订单状态')->using([
                0 => '未付款', 1 => '已付款', 2 => '已发货', 3 => '已签收'
            ])->label('orange');

            $grid->column('pay_time', '支付时间');
            $grid->column('created_at');
            $grid->column('delivery_time');

            $grid->model()->where('order_status', '=', 1);
            $grid->model()->orderByDesc('id');

            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid) {
                if ($this->order_status == 1) {
                    $actions->append(new DeliveryOrder());
                }
            });

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableEditButton();
            $grid->disableCreateButton();

            $grid->export()->rows(function (array $rows) {
                $statusArr = [
                    0 => '未付款', 1 => '已付款', 2 => '已发货', 3 => '已签收'
                ];
                foreach ($rows as $index => &$row) {
                    $row['order_status'] = $statusArr[$row['order_status']];
                }
                return $rows;
            });

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('userinfo.nickname', '账号');
                $filter->equal('userinfo.phone', '手机');
                $filter->equal('get_user_name', '收货人');
                $filter->equal('get_user_phone', '手机号');
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, Order::with(['userinfo', 'getaddress']), function (Show $show) {
            $show->field('id');
            $show->field('userinfo.phone', '账号');
            $show->field('order_no', '订单编号');
            $show->divider();


            $show->field('order_money', '总价')->width(2);

            $show->field('order_status', '订单状态')->gender()->using([
                0 => '未付款', 1 => '已付款', 2 => '已发货', 3 => '已签收'
            ])->label('orange');

            $show->relation('OrdersDetail', function ($model) {
                $grid = new Grid(new OrdersDetail());
                $grid->model()->where('order_id', $model->id);
                // 设置路由
                $grid->setResource('OrdersDetail');

                $linkUrl = '../product/48/edit';

                $grid->number();

                $grid->column('product_name', '商品名称');
                // 传入闭包
                $grid->column('product_id','商品URL')->link(function ($value) {
                    $type = $this->type;
                    if($type == 1){
                        $linkUrl = 'product/'.$value.'/edit';
                    }elseif ($type == 2){
                        $linkUrl = 'coinproduct/'.$value.'/edit';
                    }else{
                        $linkUrl = 'spikeproduct/'.$value.'/edit';
                    }
                    return admin_url($linkUrl);
                });

                $grid->column('product_img_path', '商品图片')->image(env('APP_URL') . '/storage/', 50, 50);
                $grid->column('product_count', '购买数量');
                $grid->column('product_price', '商品价格');



                $grid->disableActions();
                $grid->disableRowSelector();
                $grid->disableDeleteButton();
                $grid->disableViewButton();
                $grid->disableCreateButton();
                return $grid;
            });

            $show->field('getaddress.name', '收件信息')->prepend(function ($value, $original) {
                // $value 是当前字段值
                // $original 是当前字段从数据库中查询出来的原始值
                // 获取其他字段值
                $deviInfo = '收货人:' . $this->getaddress->name . '-手机:' . $this->getaddress->phone
                    . '-' . '地址:' . $this->getaddress->province . '-' . $this->getaddress->city . '-'
                    . $this->getaddress->city . '-' . $this->getaddress->district . '-' . $this->getaddress->address;
                return "[{$deviInfo}]";
            });

//            $show->field('pay_time','支付时间');
            $show->field('created_at');
            $show->field('delivery_time');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Order(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('after_status');
            $form->text('delivery_time');
            $form->text('logistics_fee');
            $form->text('order_amount_total');
            $form->text('order_no');
            $form->text('order_settlement_status');
            $form->text('order_settlement_time');
            $form->text('order_status');
            $form->text('pay_remarks');
            $form->text('payment_type');
            $form->text('product_amount_total');
            $form->text('product_count');
            $form->text('product_price');
            $form->text('shopping_id');
            $form->text('supplier_name');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
