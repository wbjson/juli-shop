<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Product;
use App\Models\Image;
use App\Models\ProductsCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;

class ProductController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Product::with(['shop', 'category']), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('main_picture_path', '主图')->image(env('APP_URL') . '/storage/', 50, 50);
            $grid->column('category.name', '所属分类')->badge();
            $grid->column('price');
            $grid->column('status')->switch();

            $grid->column('sales', '销量');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderByDesc('id');
            $grid->model()->where('type', 1);

            $grid->disableRowSelector();
//            $grid->disableDeleteButton();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->like('name');
                $filter->equal('status')->radio([0 => '下架', 1 => '上架']);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(Product::with(['attr']), function (Form $form) {
            $form->text('name')->maxLength(100)->required();
            $form->decimal('price')->required();

            $form->image('main_picture')->retainable()->removable(false)->saving(function ($value) use ($form) {
                if ($form->isEditing() && !$value) {
                    // 编辑页面，删除图片逻辑
                    Image::destroy($form->model()->img_id);
                    return;
                }
                // 新增或编辑页面上传图片
                if ($value) {
                    $model = Image::where('path', $value)->first();
                }

                if (empty($model)) {
                    $model = new Image();
                }

                $model->path = $value;
                $model->user_id = 0;

                $model->save();

                return $model->getKey();
            })->required()->disk('admin')->uniqueName()->maxSize(10240)->accept('jpg,png,gif,jpeg')->required()->autoUpload()->customFormat(function ($v) {
                if (!$v) {
                    return;
                }
                return Image::find((array)$v)->pluck('path')->toArray();
            });

            $goodCate = ProductsCategory::query()
                ->where('status', 1)
                ->where('level', 1)
                ->orderByRaw('sort')
                ->pluck('name', 'id')
                ->toArray();
            $form->width(4)->select('category_one', '一级分类')->options($goodCate)->load('category_id','api/category');
            $form->width(4)->select('category_id','二级分类');


            $form->multipleImage('picture')->saving(function ($item) use ($form) {
                $keys = [];
                foreach ($item as $value) {
                    if ($form->isEditing() && !$value) {
                        // 编辑页面，删除图片逻辑
                        Image::destroy($form->model()->img_id);
                        return;
                    }
                    // 新增或编辑页面上传图片
                    if ($value) {
                        $model = Image::where('path', $value)->first();
                    }

                    if (empty($model)) {
                        $model = new Image();
                    }

                    $model->path = $value;
                    $model->user_id = 0;
                    $model->save();
                    $keys[] = $model->getKey();
                }

                return implode(',', $keys);
            })->required()->disk('admin')->retainable()->uniqueName()->maxSize(10240)->accept('jpg,png,gif,jpeg')->autoUpload()->customFormat(function ($v) {
                if (!$v) {
                    return;
                }
                return Image::find((array)$v)->pluck('path')->toArray();
            });
            $form->radio('status')->options([0 => '下架', 1 => '上架'])->required();
            $form->number('sales')->required();
            $form->number('stock', '库存')->required();

            $form->hasMany('attr', '商品属性', function (Form\NestedForm $form) {
                $form->text('name', '属性名')->maxLength(5)->required();
                $form->text('value', '属性值')->maxLength(10)->required();
            })->useTable();

            $form->editor('content')->required()->disk('admin');

            $form->submitted(function (Form $form) {
                // 获取用户提交参数
                $category_one = $form->category_one;
                $category_id = $form->category_id;
                if($category_id <= 0){
                    $form->category_id = $category_one;
                }
//                // 中断后续逻辑
//                return $form->response()->error('服务器出错了~');
            });


            $form->disableDeleteButton();
            $form->disableViewButton();
            $form->disableCreatingCheck();
            $form->disableEditingCheck();
            $form->disableViewCheck();
        });
    }
}
