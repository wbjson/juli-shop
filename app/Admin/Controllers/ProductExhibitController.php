<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ProductExhibit;
use App\Models\Image;
use App\Models\Product;
use App\Models\RecommendList;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ProductExhibitController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(ProductExhibit::with(['product']), function (Grid $grid) {
            $grid->number();
            $grid->column('product.name','商品名称')->limit(50);
            $typeArr = [1=>'首页推荐'];
            $grid->column('type')->using($typeArr)->badge();
            $grid->column('status')->switch();
            $grid->column('orders');
            $grid->column('created_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableViewButton();

            $grid->model()->orderByRaw('type');
            $grid->model()->orderByDesc('id');

            $grid->filter(function (Grid\Filter $filter) use ($typeArr) {
                $filter->equal('type')->select($typeArr);
                $filter->equal('status')->select([0=>'禁用',1=>'启用']);
                $filter->between('created_at')->datetime();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ProductExhibit(), function (Form $form) {
            $form->select('product_id','产品名称')->options(Product::query()->pluck('name','id'))->required();
            $typeArr = [1=>'首页推荐'];

            $form->select('type')->options($typeArr)->required();
            $form->switch('status')->required();
            $form->number('orders','排序')->required();

            $form->disableDeleteButton();
            $form->disableViewButton();
            $form->disableCreatingCheck();
            $form->disableEditingCheck();
            $form->disableViewCheck();
        });
    }
}
