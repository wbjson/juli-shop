<?php

namespace App\Admin\Controllers;

use App\Models\Image;
use App\Models\ProductsCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductsCategoryController extends AdminController
{
    public function index(Content $content)
    {
        return $content->header('商品类型')->description('类型列表')->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(ProductsCategory::with(['parent']), function (Grid $grid) {
            $grid->column('name', '类型')->tree();
            $grid->column('icon', '主图')->image(env('APP_URL').'/product_category/', 50, 50);

            $grid->column('status')->switch();
            $grid->column('sort','排序');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ProductsCategory(), function (Form $form) {
            $form->select('parent_id')->options(array_merge([0 => '顶级'],
                \App\Models\ProductsCategory::query()->where('status',1)->where('level',1)->pluck('name', 'id')->toArray()));
            $form->text('name','名称')->required();

            $form->image('icon','图标')->disk('product_category')->removable(false)->retainable()->uniqueName()->maxSize(10240)
                ->accept('jpg,png,gif,jpeg')->autoUpload();

            $form->switch('status')->required()->default(1);
            $form->number('sort','排序')->required();

            $form->hidden('level');

            if ($form->isDeleting()) {
                $id = $form->getKey();
                if($id > 0){
                    $nextCate = ProductsCategory::query()->where('parent_id',$id)->count();
                    if($nextCate > 0){
                        return $form->response()->error('存在下级分类');
                    }
                }
            }


            $form->saving(function (Form $form) {
                $parent_id = $form->parent_id;
                if ($parent_id > 0) {
                    $form->level = 2;
                } else {
                    $form->level = 1;
                }
            });

            $form->saved(function (){
                Cache::forget('product_category');
            });

            $form->disableDeleteButton();
            $form->disableViewButton();
            $form->disableCreatingCheck();
            $form->disableEditingCheck();
            $form->disableViewCheck();
        });
    }


    public function category(Request $request){
        $cateId = $request->get('q');
        $cateList = ProductsCategory::query()->where('parent_id',$cateId)->where('level',2)->get(['id', 'name as text']);
        return $cateList;
    }
}
