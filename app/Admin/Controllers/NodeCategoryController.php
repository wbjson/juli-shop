<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\NodeCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class NodeCategoryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new NodeCategory(), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('type')->using([1=>'全网',2=>'全省']);
            $grid->column('rate')->display(function (){
                return $this->rate.'%';
            });
            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
//            $grid->disableCreateButton();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new NodeCategory(), function (Form $form) {
            $form->text('name')->required();
            $form->radio('type')->options([1=>'全网',2=>'全省'])->required();
            $form->rate('rate')->required();
        });
    }
}
