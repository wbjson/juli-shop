<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\OrdersDetail;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrdersDetailController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new OrdersDetail(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('order_id');
            $grid->column('user_id');
            $grid->column('shop_id');
            $grid->column('product_id');
            $grid->column('attr_id');
            $grid->column('product_name');
            $grid->column('product_img');
            $grid->column('supplier_name');
            $grid->column('order_status');
            $grid->column('product_count');
            $grid->column('product_price');
            $grid->column('shopping_id');
            $grid->column('type');
            $grid->column('logistics_fee');
            $grid->column('pay_remarks');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new OrdersDetail(), function (Show $show) {
            $show->field('id');
            $show->field('order_id');
            $show->field('user_id');
            $show->field('shop_id');
            $show->field('product_id');
            $show->field('attr_id');
            $show->field('product_name');
            $show->field('product_img');
            $show->field('supplier_name');
            $show->field('order_status');
            $show->field('product_count');
            $show->field('product_price');
            $show->field('shopping_id');
            $show->field('type');
            $show->field('logistics_fee');
            $show->field('pay_remarks');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new OrdersDetail(), function (Form $form) {
            $form->display('id');
            $form->text('order_id');
            $form->text('user_id');
            $form->text('shop_id');
            $form->text('product_id');
            $form->text('attr_id');
            $form->text('product_name');
            $form->text('product_img');
            $form->text('supplier_name');
            $form->text('order_status');
            $form->text('product_count');
            $form->text('product_price');
            $form->text('shopping_id');
            $form->text('type');
            $form->text('logistics_fee');
            $form->text('pay_remarks');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
