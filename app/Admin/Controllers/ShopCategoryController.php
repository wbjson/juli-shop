<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ShopCategory;
use App\Models\LevelConfig;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ShopCategoryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new ShopCategory(), function (Grid $grid) {
            $grid->number();
            $grid->column('name');
            $grid->column('jean_fee')->display(function (){
                return $this->jean_fee.'%';
            });
            $grid->column('destroy_pot_rate','销毁底池比例')->display(function (){
                return $this->destroy_pot_rate.'%';
            });
            $grid->column('jian_rate','见点比例')->display(function (){
                return $this->jian_rate.'%';
            });
            $grid->column('jian_every_rate','见点每个上级比例')->display(function (){
                return $this->jian_every_rate.'%';
            });

            $grid->column('apply_grade','申请条件')->display(function ($grade){
                return explode(',',$grade);
            })->map(function($levelId){
                return LevelConfig::query()->where('id',$levelId)->value('name');
            })->badge();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ShopCategory(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('jean_fee');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ShopCategory(), function (Form $form) {
            $form->text('name')->required();
            $form->hidden('jean_fee');
            $form->rate('destroy_pot_rate','销毁底池比例')->required();
            $form->rate('jian_rate','见点比例')->required();
            $form->rate('jian_every_rate','见点每个上级比例')->required();

            $form->checkbox('apply_grade','申请条件等级')->options(LevelConfig::query()->pluck('name','id')->toArray())->saving(function ($value){
                return implode(',',$value);
            })->required();

            $form->saving(function (Form $form){
                $form->jean_fee = $form->destroy_pot_rate + $form->jian_rate;
            });
        });
    }
}
