<?php

namespace App\Admin\Controllers;


use App\Admin\Metrics\AllIncome;
use App\Admin\Metrics\MonthFreeze;
use App\Admin\Metrics\MonthFreezeVip;
use App\Admin\Metrics\MonthOrder;
use App\Admin\Metrics\MonthOrderCoin;
use App\Admin\Metrics\MonthOrderPoint;
use App\Admin\Metrics\MonthPoint;
use App\Admin\Metrics\MonthPointConsume;
use App\Admin\Metrics\MonthPointOutput;
use App\Admin\Metrics\MonthRelease;
use App\Admin\Metrics\MonthWithdraw;
use App\Admin\Metrics\TodayDestroyNum;
use App\Admin\Metrics\TodayFreeze;
use App\Admin\Metrics\TodayFreezeVip;
use App\Admin\Metrics\TodayLpNum;
use App\Admin\Metrics\TodayOrder;
use App\Admin\Metrics\TodayOrderCoin;
use App\Admin\Metrics\TodayOrderPoint;
use App\Admin\Metrics\TodayPledgeNum;
use App\Admin\Metrics\TodayPoint;
use App\Admin\Metrics\TodayRelease;
use App\Admin\Metrics\TodayUsers;
use App\Admin\Metrics\TodayWithdraw;
use App\Admin\Metrics\TotalDestroyNum;
use App\Admin\Metrics\TotalDestroyPower;
use App\Admin\Metrics\TotalDynamicPower;
use App\Admin\Metrics\TotalFreeze;
use App\Admin\Metrics\TotalFreezeVip;
use App\Admin\Metrics\TotalLoseSign;
use App\Admin\Metrics\TotalLpNum;
use App\Admin\Metrics\TotalLpPower;
use App\Admin\Metrics\TotalOrder;
use App\Admin\Metrics\TotalOrderCoin;
use App\Admin\Metrics\TotalOrderPoint;
use App\Admin\Metrics\TotalPledgeNum;
use App\Admin\Metrics\TotalPledgePower;
use App\Admin\Metrics\TotalPoint;
use App\Admin\Metrics\TotalPointConsume;
use App\Admin\Metrics\Totalpool;
use App\Admin\Metrics\TotalRelease;
use App\Admin\Metrics\TotalStaticPower;
use App\Admin\Metrics\TotalUsers;
use App\Admin\Metrics\TotalWithdraw;
use App\Admin\Metrics\TotalYeji;
use App\Admin\Metrics\Wait;
use App\Admin\Metrics\YesterdayLoseSign;
use App\Http\Controllers\Controller;
use Dcat\Admin\Layout\Column;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('主页')
            ->description('')
            ->body(function (Row $row) {
                $row->column(10, function (Column $column) {
                    $column->row(function (Row $row) {
                        //总注册
                        $row->column(2, new TotalUsers());
                        //今日注册
                        $row->column(2, new TodayUsers());
                    });

//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayOrder());        //今日报单金额
//                        $row->column(2, new MonthOrder());        //当月报单金额
//                        $row->column(2, new TotalOrder());        //总报单金额
//
//
//
//                        $row->column(2, new TodayOrderCoin());        //今日报单赠送积分
//                        $row->column(2, new MonthOrderCoin());        //当月报单赠送积分
//                        $row->column(2, new TotalOrderCoin());        //总报单赠送积分
//
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayOrderPoint());        //今日金额
//                        $row->column(2, new MonthOrderPoint());        //今日金额
//                        $row->column(2, new TotalOrderPoint());        //总金额
//
//                        $row->column(2, new TodayPoint());        //今日积分金额
//                        $row->column(2, new MonthPoint());        //今日积分金额
//                        $row->column(2, new TotalPoint());        //总积分金额
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayFreezeVip());
//                        $row->column(2, new MonthFreezeVip());
//                        $row->column(2, new TotalFreezeVip());        //总冻结股东金
//
//
//
//
//                        $row->column(2, new TodayFreeze());         //月冻结
//                        $row->column(2, new MonthFreeze());         //月冻结
//                        $row->column(2, new TotalFreeze());        //总冻结积分
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayFreezeVip());
//                        $row->column(2, new MonthFreezeVip());
//                        $row->column(2, new TotalFreezeVip());        //总冻结股东金
//
//
//                        $row->column(2, new TodayFreeze());         //月冻结
//                        $row->column(2, new MonthFreeze());         //月冻结
//                        $row->column(2, new TotalFreeze());        //总冻结积分
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayRelease());        //今日释放金额
//                        $row->column(2, new MonthRelease());        //今日释放金额
//                        $row->column(2, new TotalRelease());        //今日释放金额
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new TodayWithdraw());        //当日提现
//                        $row->column(2, new MonthWithdraw());        //当月提现金额
//                        $row->column(2, new TotalWithdraw());        //今日释放金额
//                    });
//
//                    $column->row(function (Row $row) {
//                        $row->column(2, new YesterdayLoseSign());        //昨日未签到金额
//                        $row->column(2, new TotalLoseSign());        //总未签到金额
//
//                    });
                    /*本月积分产出 商城积分消耗*/
//                    $column->row(function (Row $row) {
//                        $row->column(2, new MonthPointOutput());        //本月积分产出
//                        $row->column(2, new MonthPointConsume());        //本月积分消耗
//
//                        $row->column(2, new TotalPointConsume());        //本月积分消耗
//                        $row->column(2, new TotalOrderPoint());        //本月积分消耗
//                    });
                });

            });
    }
}
