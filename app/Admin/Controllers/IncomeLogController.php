<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\IncomeLog;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;

class IncomeLogController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(IncomeLog::with(['user']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.phone', '用户');
            $grid->column('user.nickname', '昵称');
            $grid->column('user_id', 'UID');
            $grid->column('amount_type', '资金类型')->using([1 => '余额'])->badge('red');
            $grid->column('before', '之前');
            $grid->column('total', '金额');
            $grid->column('after', '之后');
            $grid->column('type', '类型')->using([
                0 => '后台操作',
                5 => '发送红包',
                6 => '收入红包',
                7 => '退回红包',
                11 => '提现收益',
                12 => '提现退回',
            ])->label();
            $grid->column('remark', '说明');

            $grid->column('created_at')->sortable();
            $grid->model()->orderBy('id', 'desc');


            $grid->disableCreateButton();
            $grid->disableActions();
            $grid->disableRowSelector();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone', '用户名');

                $filter->equal('type', '类型')->select([
                    0 => '后台操作',
                    5 => '发送红包',
                    6 => '收入红包',
                    7 => '退回红包',
                    10 => '提现',
                    11 => '提现退回',
                ]);

                $filter->equal('amount_type', '资金类型')->select([
                    1 => '余额'
                ]);

            });
        });
    }

}
