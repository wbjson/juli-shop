<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\Caiwu;
use App\Admin\Renderable\UserPaymentTable;
use App\Admin\Repositories\User;
use App\Admin\Tools\AgentHandle;
use App\Admin\Tools\StaticHandle;
use App\Admin\Tools\WeekHandle;
use App\Models\LevelConfig;
use App\Models\UsersCard;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;

class UserController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(User::with(['parent']), function (Grid $grid) {
            $grid->number();
            $grid->column('id', 'UID');
            $grid->column('phone', '手机号');
            $grid->column('nickname', '昵称');
            $grid->column('parent.phone', '上级');
            $grid->column('parent.nickname', '上级昵称');
            $grid->column('code', '邀请码');
            $grid->column('zhi_num');
            $grid->column('group_num');
            $grid->column('amount', '余额');
            $grid->column('status', '状态')->switch('', true);
            $grid->column('created_at', '注册时间');

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

            $grid->model()->orderBy('id', 'desc');

            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid) {
                $actions->append(new Caiwu());
            });


            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('phone', '手机号');
                $filter->equal('id', 'UID');
                $filter->equal('nickname', '昵称');
                $filter->equal('status', '状态')->radio([0 => '禁用', 1 => '有效']);
                $filter->between('created_at', '注册时间')->datetime();
            });
        });
    }


    protected function form()
    {
        return Form::make(User::with(['parentinfo']), function (Form $form) {
            $form->display('phone', '手机号');
            $form->text('nickname', '昵称');

            $form->display('parentinfo.phone', '当前上级');
            $form->text('parent_name', '要修改的上级手机号');

            $form->display('code', '邀请码');

//            $form->text('password', '登陆密码');
//            $form->text('trade_password', '交易密码');


            $form->saving(function (Form $form) {
                $password = $form->password;
                if (!isset($password) || empty($password)) {
                    $form->deleteInput('password');
                }

                $trade_password = $form->trade_password;
                if (!isset($trade_password) || empty($trade_password)) {
                    $form->deleteInput('trade_password');
                }
            });

            $form->disableDeleteButton();

            $form->saved(function (Form $form) {
                $id = $form->getKey();
                $parentName = $form->parent_name;
                if (!empty($parentName)) {
                    $parantInfo = \App\Models\User::query()
                        ->where('phone', $parentName)
                        ->select('id', 'parent_id', 'path', 'deep')
                        ->first();
                    if (empty($parantInfo)) {
                        return $form->response()->error('未找到该账号');
                    }

                    $userInfo = \App\Models\User::query()->where('id', $id)
                        ->select('id', 'parent_id', 'path', 'deep')
                        ->first();
                    if ($userInfo->parent_id != $parantInfo->id) {
                        /*找到我的下级会员*/
                        $groupUser = \App\Models\User::query()
                            ->where('path', 'LIKE', '%' . $userInfo->id . '%')
                            ->select('id', 'parent_id', 'path', 'deep')
                            ->orderBy('id', 'asc')
                            ->pluck('id');

                        DB::beginTransaction();
                        try {
                            //只修改自己信息
                            $userInfo->parent_id = $parantInfo->id;
                            $userInfo->path = empty($parantInfo->path) ? '-' . $parantInfo->id . '-' : $parantInfo->path . $parantInfo->id . '-';
                            $userInfo->deep = $parantInfo->deep + 1;
                            $userInfo->parent_name = '';
                            $userInfo->save();

                            if (!$groupUser->isEmpty()) {
                                foreach ($groupUser as $v) {
                                    $PathUser = \App\Models\User::query()->where('id', $v)
                                        ->select('id', 'parent_id', 'path', 'deep')
                                        ->first();
                                    $parantInfo = \App\Models\User::query()
                                        ->where('id', $PathUser->parent_id)
                                        ->select('id', 'parent_id', 'path', 'deep')
                                        ->first();
                                    $PathUser->parent_id = $parantInfo->id;
                                    $PathUser->path = empty($parantInfo->path) ? '-' . $parantInfo->id . '-' : $parantInfo->path . $parantInfo->id . '-';
                                    $PathUser->deep = $parantInfo->deep + 1;
                                    $PathUser->parent_name = '';
                                    $PathUser->save();
                                }
                            }
                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollBack();
                            return responseJsonAsServerError($e->getMessage() . $e->getLine());
                        }

                    }
                }

            });

        });
    }

}
