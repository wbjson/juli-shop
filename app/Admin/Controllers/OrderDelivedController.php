<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\AddCard;
use App\Admin\Actions\Grid\DeliveryOrder;
use App\Admin\Actions\Grid\WithdrawApplyAudit;
use App\Admin\Forms\WithdrawApplyAuditForm;
use App\Admin\Repositories\Order;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class OrderDelivedController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Order::with(['userinfo','getaddress']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('userinfo.nickname','昵称');
            $grid->column('userinfo.phone','手机');

            $grid->column('order_no','订单编号');

            $grid->column('order_money','总价');
            $grid->column('order_integral', '积分');

            $grid->column('getaddress.phone','收货信息')->display(function (){
                $str = '收货人:'.$this->getaddress->name.'/手机:'.$this->getaddress->phone
                    .'地址:'.$this->getaddress->province.'-'.$this->getaddress->city.'-'
                    .$this->getaddress->city.'-'.$this->getaddress->district
                ;
                return $str;
            });


            $grid->column('order_status','订单状态')->using([
                0=>'未付款',1=>'已付款',2=>'已发货',3=>'已签收'
            ])->label('orange');

            $grid->column('pay_time','支付时间');
            $grid->column('created_at');
            $grid->column('delivery_time','发货时间');

            $grid->model()->where('order_status','>=',2);
            $grid->model()->orderByDesc('id');

            $grid->actions(function (Grid\Displayers\Actions $actions) use (&$grid){
                if ($this->order_status == 1){
                    $actions->append(new DeliveryOrder());
                }
            });

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableEditButton();
            $grid->disableCreateButton();

            $grid->export()->rows(function (array $rows) {
                $statusArr = [
                    0=>'未付款',1=>'已付款',2=>'已发货',3=>'已签收'
                ];
                foreach ($rows as $index => &$row) {
                    $row['order_status'] = $statusArr[$row['order_status']];
                }
                return $rows;
            });

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('userinfo.phone','下单手机');
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, Order::with(['userinfo','getaddress']), function (Show $show) {
            $show->field('id');
            $show->field('userinfo.phone','下单地址');
            $show->field('order_no','订单编号');
            $show->divider();


            $show->field('order_money','总价')->width(2);

            $show->field('order_status','订单状态')->gender()->using([
                0=>'未付款',1=>'已付款',2=>'已发货',3=>'已签收'
            ])->label('orange');

            $show->field('getaddress.phone','收件信息')->prepend(function ($value, $original) {
                // $value 是当前字段值
                // $original 是当前字段从数据库中查询出来的原始值

                // 获取其他字段值
                $deviInfo = '收货人:'.$this->getaddress->name.'-手机:'.$this->getaddress->phone
                    .'-'.'地址:'.$this->getaddress->province.'-'.$this->getaddress->city.'-'
                    .$this->getaddress->city.'-'.$this->getaddress->district.'-'.$this->getaddress->address
                ;
                return "[{$deviInfo}]";
            });

//            $show->field('pay_time','支付时间');
            $show->field('created_at');
            $show->field('delivery_time','发货时间');

        });
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Order(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('after_status');
            $form->text('delivery_time');
            $form->text('logistics_fee');
            $form->text('order_amount_total');
            $form->text('order_no');
            $form->text('order_settlement_status');
            $form->text('order_settlement_time');
            $form->text('order_status');
            $form->text('pay_remarks');
            $form->text('payment_type');
            $form->text('product_amount_total');
            $form->text('product_count');
            $form->text('product_price');
            $form->text('shopping_id');
            $form->text('supplier_name');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
