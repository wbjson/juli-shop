<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Lp;
use App\Admin\Repositories\Pledge;
use App\Models\InvestLpLog;
use App\Models\MainCurrency;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Widgets\Tab;

class InvestLpController extends AdminController
{

    public function index(Content $content)
    {
        $tab = Tab::make();
        $tab->add('质押记录',$this->pledgeListgrid());
        $tab->add('矿池管理',$this->grid());
        return $content->header('LP矿池')->description('LP矿池管理')->body($tab->withCard());
    }

    protected function pledgeListgrid(){
        return Grid::make(InvestLpLog::with(['user','invest','card']), function (Grid $grid) {
            $grid->number();
            $grid->column('user.phone','用户');
            $grid->column('invest.name','矿池');
            $grid->column('lp_num','质押数量');
            $grid->column('exchange_rate','兑U汇率')->display(function (){
                return $this->exchange_rate;
            });
            $grid->column('exchange_num','兑U数量');
            $grid->column('power','总算力');
            $grid->column('income','总收益');
            $grid->column('status','状态')->using([
                1 => '有效',
                2 => '已赎回'
            ])->badge();

            $grid->column('created_at')->sortable();
            $grid->model()->orderByDesc('id');
            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableActions();

            $grid->export()->rows(function (array $rows) {
                $statusArr = [
                    1 => '有效',
                    2 => '已赎回'
                ];
                foreach ($rows as $index => &$row) {
                    $row['status'] = $statusArr[$row['status']];
                }
                return $rows;
            });

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('user.phone','用户');
                $filter->equal('status','状态')->select([
                    1 => '有效',
                    2 => '已赎回'
                ]);
                $filter->between('created_at','创建时间')->datetime();
            });
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(InvestLp::with(['mainCoinDb','otherCoinDb']), function (Grid $grid) {
            $grid->number();
            $grid->column('mainCoinDb.name','主流币');
            $grid->column('otherCoinDb.name','母币');
            $grid->column('income')->display(function ($income){
                return $income.'%';
            });
            $grid->column('total');
            $grid->column('lp_address','LP合约地址');
            $grid->column('lp_decimals','LP精度');
            $grid->column('lp_rate','LP兑u汇率');
            $grid->column('search_status','自动更新')->switch();
            $grid->column('sort','排序')->editable();
            $grid->column('status')->switch(true);
            $grid->column('created_at');

            $grid->model()->orderByDesc('id');
            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
//            $grid->disableCreateButton();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(InvestLp::with(['mainCoinDb','otherCoinDb']), function (Form $form) {
            $form->select('main_coin')->options(MainCurrency::query()->pluck('name','id')->toArray())->required();
            $form->select('other_coin')->options(MainCurrency::query()->pluck('name','id')->toArray())->required();
            $form->hidden('name');
            $form->rate('income')->required();
            $form->text('lp_address','LP合约地址')->required();
            $form->number('lp_decimals','LP精度')->required();
            $form->decimal('lp_rate','LP兑u汇率');
            $form->switch('search_status','自动更新');
            $form->number('sort','排序')->default(0)->required();
            $form->switch('status','状态')->default(1)->required();
            $form->number('total')->default(0)->required();
            $form->saving(function (Form $form){
                $form->name =  MainCurrency::query()->where('id',$form->main_coin)->value('name').'/'.MainCurrency::query()->where('id',$form->other_coin)->value('name');
            });

        });
    }
}
