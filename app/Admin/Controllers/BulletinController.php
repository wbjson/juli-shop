<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Bulletin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BulletinController extends AdminController
{
    public function index(Content $content)
    {
        return $content->header('协议讯息')->description('协议讯息')->body($this->grid());
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Bulletin(), function (Grid $grid) {
            $grid->number();
            $grid->column('title');
            $grid->column('content')->display('点击查看') // 设置按钮名称
            ->modal(function ($modal) {
                // 设置弹窗标题
                $modal->title($this->title);
                // 自定义图标
                return $this->content;
            });
            $grid->column('type')->using([
                1=>'关于我们',2=>'规则中心',3=>'服务器协议',4=>'注册协议'
            ]);

            $grid->column('status')->switch();
            $grid->column('sort')->editable();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderBy('id','desc');
            $grid->disableRowSelector();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
                $filter->equal('status')->radio([
                    0 => '下架',
                    1 => '上架'
                ]);

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Bulletin(), function (Form $form) {
            $form->text('title')->required();
            $form->radio('type')->options([1=>'关于我们',2=>'规则中心',3=>'服务器协议',4=>'注册协议'])->required();
            $form->editor('content')->required()->disk('admin')->height('600');
            $form->radio('status','状态')->required()->options([0=>'下架',1=>'上架'])->default(1);
            $form->number('sort','排序')->required()->default(0);
        });
    }
}
