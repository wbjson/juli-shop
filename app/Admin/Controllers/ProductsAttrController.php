<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ProductsAttr;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ProductsAttrController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new ProductsAttr(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('shop_id');
            $grid->column('product_id');
            $grid->column('name');
            $grid->column('value');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ProductsAttr(), function (Show $show) {
            $show->field('id');
            $show->field('shop_id');
            $show->field('product_id');
            $show->field('name');
            $show->field('value');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ProductsAttr(), function (Form $form) {
            $form->display('id');
            $form->text('shop_id');
            $form->text('product_id');
            $form->text('name');
            $form->text('value');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
