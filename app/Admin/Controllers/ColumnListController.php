<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\ColumnList;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ColumnListController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new ColumnList(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name','栏目名称');
            $grid->column('type','分类')->using([1=>'首页',2=>'商城'])->badge();
            $grid->column('icon','图标')->image(env('APP_URL').'/uploads/',100,100);
            $grid->column('sort','排序');
            $grid->column('status','状态')->switch();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();


            $grid->model()->orderBy('type','asc');
            $grid->model()->orderBy('id','desc');
            $grid->disableRowSelector();
            $grid->disableViewButton();
            $grid->disableRowSelector();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new ColumnList(), function (Show $show) {
            $show->field('id');
            $show->field('icon');
            $show->field('link');
            $show->field('name');
            $show->field('sort');
            $show->field('status');
            $show->field('type');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new ColumnList(), function (Form $form) {
            $form->text('name','名称')->required()->rules('max:250');
            $form->url('link','URL');
            $form->image('icon','图标')->disk('admin')->uniqueName()->maxSize(10240)->accept('jpg,png,gif,jpeg')->required()->autoUpload();
            $form->radio('type','类型')->required()->options([1=>'首页',2=>'商城栏目'])->default(1);
            $form->radio('status','状态')->required()->options([0=>'下架',1=>'上架'])->default(1);
            $form->number('sort','排序')->required()->default(0);
        });
    }
}
