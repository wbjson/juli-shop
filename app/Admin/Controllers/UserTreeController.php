<?php


namespace App\Admin\Controllers;

use App\Admin\Repositories\User;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Layout\Content;

class UserTreeController extends AdminController
{

    public function index(Content $content)
    {
        return $content->header('推荐树')->description('推荐树管理')->body($this->grid());
    }


    protected function grid()
    {
        return Grid::make(User::with([]), function (Grid $grid) {
            $grid->column('phone', '手机号')->tree();
            $grid->column('id', 'UID');
            $grid->column('nickname', '昵称');
            $grid->column('code', '邀请码');
            $grid->column('amount', '余额');
            $grid->column('zhi_num', '直推人数');
            $grid->column('group_num', '团队人数');
            $grid->column('created_at', '注册时间');

            $grid->disableRowSelector();
            $grid->disableActions();
            $grid->disableCreateButton();

            $grid->model()->orderBy('id', 'desc');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('phone', '手机号');
                $filter->equal('nickname', '昵称');
                $filter->equal('status', '状态')->radio([0 => '禁用', 1 => '有效']);
                $filter->between('created_at', '注册时间')->datetime();
            });
        });
    }

}
