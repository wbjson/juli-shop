<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\SpeedList;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class SpeedListController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new SpeedList(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('num','商城消费金额');
            $grid->column('type','类型')->using([1=>'首单',2=>'加速'])->badge();
            $grid->column('day','加速天数');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->disableRowSelector();
            $grid->disableDeleteButton();
            $grid->disableViewButton();
            $grid->disableCreateButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new SpeedList(), function (Show $show) {
            $show->field('id');
            $show->field('day');
            $show->field('num');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new SpeedList(), function (Form $form) {
            $form->display('id');
            $form->number('day','天数');
            $form->number('num','数量');

        });
    }
}
