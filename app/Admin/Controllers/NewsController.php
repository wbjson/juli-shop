<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\News;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class NewsController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new News(), function (Grid $grid) {
            $grid->number();
            $grid->column('title');
            $grid->column('content')->display('点击查看') // 设置按钮名称
            ->modal(function ($modal) {
                // 设置弹窗标题
                $modal->title($this->title);
                // 自定义图标
                return $this->content;
            });

            $grid->column('status','状态')->switch();
            $grid->column('sort')->editable();
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->model()->orderBy('id','desc');
            $grid->disableRowSelector();
            $grid->disableViewButton();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('name');
                $filter->equal('status')->radio([
                    0 => '下架',
                    1 => '上架'
                ]);

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new News(), function (Show $show) {
            $show->field('id');
            $show->field('content');
            $show->field('sort');
            $show->field('status');
            $show->field('title');
            $show->field('type');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new News(), function (Form $form) {
            $form->text('title')->required();
            $form->editor('content')->required()->disk('admin')->height('600');
            $form->radio('status','状态')->required()->options([0=>'下架',1=>'上架'])->default(1);
            $form->number('sort','排序')->required()->default(0);
        });
    }
}
