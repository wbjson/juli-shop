<?php

namespace App\Admin\Repositories;

use App\Models\QuickCardLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class QuickCardLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
