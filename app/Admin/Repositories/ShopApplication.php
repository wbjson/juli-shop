<?php

namespace App\Admin\Repositories;

use App\Models\ShopApplication as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ShopApplication extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
