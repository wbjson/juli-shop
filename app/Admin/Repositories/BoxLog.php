<?php

namespace App\Admin\Repositories;

use App\Models\BoxLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BoxLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
