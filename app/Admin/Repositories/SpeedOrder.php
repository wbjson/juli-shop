<?php

namespace App\Admin\Repositories;

use App\Models\SpeedOrder as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SpeedOrder extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
