<?php

namespace App\Admin\Repositories;

use App\Models\SpeedList as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SpeedList extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
