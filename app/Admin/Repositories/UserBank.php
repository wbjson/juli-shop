<?php

namespace App\Admin\Repositories;

use App\Models\UserBank as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class UserBank extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
