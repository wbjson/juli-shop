<?php

namespace App\Admin\Repositories;

use App\Models\GroupApplication as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class GroupApplication extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
