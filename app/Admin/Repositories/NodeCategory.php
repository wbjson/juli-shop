<?php

namespace App\Admin\Repositories;

use App\Models\NodeCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class NodeCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
