<?php

namespace App\Admin\Repositories;

use App\Models\ProductsAttr as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ProductsAttr extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
