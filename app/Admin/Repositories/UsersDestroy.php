<?php

namespace App\Admin\Repositories;

use App\Models\UsersDestroy as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class UsersDestroy extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
