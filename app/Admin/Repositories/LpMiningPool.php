<?php

namespace App\Admin\Repositories;

use App\Models\LpMiningPool as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class LpMiningPool extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
