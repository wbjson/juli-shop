<?php

namespace App\Admin\Repositories;

use App\Models\Card as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Card extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
