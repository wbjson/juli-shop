<?php

namespace App\Admin\Repositories;

use App\Models\SignLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SignLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
