<?php

namespace App\Admin\Repositories;

use App\Models\QuickCardUseLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class QuickCardUseLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
