<?php

namespace App\Admin\Repositories;

use App\Models\CardAuctionLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class CardAuctionLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
