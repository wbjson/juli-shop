<?php

namespace App\Admin\Repositories;

use App\Models\CardSell as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class CardSell extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
