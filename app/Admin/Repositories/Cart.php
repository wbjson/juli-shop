<?php

namespace App\Admin\Repositories;

use App\Models\Cart as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Cart extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
