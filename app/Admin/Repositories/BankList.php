<?php

namespace App\Admin\Repositories;

use App\Models\BankList as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BankList extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
