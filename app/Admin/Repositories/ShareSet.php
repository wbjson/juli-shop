<?php

namespace App\Admin\Repositories;

use App\Models\ShareSet as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ShareSet extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
