<?php

namespace App\Admin\Repositories;

use App\Models\LoseSign as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class LoseSign extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
