<?php

namespace App\Admin\Repositories;

use App\Models\RecommendList as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class RecommendList extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
