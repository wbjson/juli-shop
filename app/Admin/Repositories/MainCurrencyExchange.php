<?php

namespace App\Admin\Repositories;

use App\Models\MainCurrencyExchange as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class MainCurrencyExchange extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
