<?php

namespace App\Admin\Repositories;

use App\Models\UsersCard as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class UsersCard extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
