<?php

namespace App\Admin\Repositories;

use App\Models\BrowHistory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BrowHistory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
