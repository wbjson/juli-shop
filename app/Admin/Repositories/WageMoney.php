<?php

namespace App\Admin\Repositories;

use App\Models\WageMoney as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class WageMoney extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
