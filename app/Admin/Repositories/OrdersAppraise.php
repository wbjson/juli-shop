<?php

namespace App\Admin\Repositories;

use App\Models\OrdersAppraise as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class OrdersAppraise extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
