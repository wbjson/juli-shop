<?php

namespace App\Admin\Repositories;

use App\Models\UsersRemark as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class UsersRemark extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
