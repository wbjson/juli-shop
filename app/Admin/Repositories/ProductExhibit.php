<?php

namespace App\Admin\Repositories;

use App\Models\ProductExhibit as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ProductExhibit extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
