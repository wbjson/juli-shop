<?php

namespace App\Admin\Repositories;

use App\Models\AllPerformanceRate as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class AllPerformanceRate extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
