<?php

namespace App\Admin\Repositories;

use App\Models\GroupCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class GroupCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
