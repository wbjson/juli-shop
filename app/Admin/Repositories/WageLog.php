<?php

namespace App\Admin\Repositories;

use App\Models\WageLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class WageLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
