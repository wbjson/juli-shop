<?php

namespace App\Admin\Repositories;

use App\Models\ProductsCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ProductsCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
