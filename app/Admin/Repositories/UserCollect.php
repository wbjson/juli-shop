<?php

namespace App\Admin\Repositories;

use App\Models\UserCollect as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class UserCollect extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
