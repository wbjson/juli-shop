<?php

namespace App\Admin\Repositories;

use App\Models\OrdersReturnsApply as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class OrdersReturnsApply extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
