<?php

namespace App\Admin\Repositories;

use App\Models\OrdersDetail as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class OrdersDetail extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
