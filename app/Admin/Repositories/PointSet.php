<?php

namespace App\Admin\Repositories;

use App\Models\PointSet as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class PointSet extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
