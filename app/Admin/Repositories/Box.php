<?php

namespace App\Admin\Repositories;

use App\Models\Box as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Box extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
