<?php

namespace App\Admin\Repositories;

use App\Models\ShopCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ShopCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
