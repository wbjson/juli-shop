<?php

namespace App\Admin\Repositories;

use App\Models\ImLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class ImLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
