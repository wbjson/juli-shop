<?php

namespace App\Admin\Repositories;

use App\Models\OnlineLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class OnlineLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
