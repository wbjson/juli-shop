<?php

namespace App\Admin\Repositories;

use App\Models\Pool as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Pool extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
