<?php

namespace App\Admin\Repositories;

use App\Models\PoolLog as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class PoolLog extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
