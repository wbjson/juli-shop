<?php

namespace App\Admin\Actions\Grid;


use Dcat\Admin\Grid\RowAction;
use Dcat\Admin\Widgets\Modal;

class AddCard extends RowAction
{
    /**
     * @return string
     */
	protected $title = '新增加成卡';



	public function render()
    {
        // 实例化表单类并传递自定义参数
        $form = \App\Admin\Forms\AddCardForm::make()->payload(['id' => $this->getKey()]);

        return Modal::make()->lg()->title($this->title)->body($form)->button($this->title);
    }

}
