<?php

namespace App\Admin\Actions\Grid;

use App\Admin\Forms\ShopApplicationAuditForm;
use Dcat\Admin\Grid\RowAction;
use Dcat\Admin\Widgets\Modal;

class ShopApplicationAudit extends RowAction
{
    /**
     * @return string
     */
	protected $title = '审核';


	public function render()
    {
        // 实例化表单类并传递自定义参数
        $form = ShopApplicationAuditForm::make()->payload(['id' => $this->getKey()]);

        return Modal::make()->lg()->title($this->title)->body($form)->button($this->title);
    }

}
