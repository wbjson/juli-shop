<?php

namespace App\Admin\Actions\Grid;


use Dcat\Admin\Grid\RowAction;
use Dcat\Admin\Widgets\Modal;

class Caiwu extends RowAction
{
    /**
     * @return string
     */
	protected $title = '财务';



	public function render()
    {
        // 实例化表单类并传递自定义参数
        $form = \App\Admin\Forms\Caiwu::make()->payload(['id' => $this->getKey()]);

        return Modal::make()->lg()->title($this->title)->body($form)->button($this->title);
    }

}
