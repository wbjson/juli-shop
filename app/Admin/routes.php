<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {


    $router->get('/', 'HomeController@index');
    $router->resource('users', 'UserController');
    ##用户树
    $router->resource('tree', 'UserTreeController');

    $router->resource('recharge', 'RechargeController');

    $router->resource('banner', 'BannerController');
    $router->resource('shareset', 'ShareSetController');

    $router->resource('income', 'IncomeLogController');

    $router->resource('monthincome', 'MonthIncomeLogController');

    $router->resource('exchange', 'ExchangeOrderController');

    $router->resource('bulletin', 'BulletinController');

    $router->resource('news', 'NewsController');

    $router->resource('column', 'ColumnListController');

    $router->resource('withdraw', 'WithdrawController');

    $router->resource('pledge', 'PledgeController');

    $router->resource('redemption','RedemptionController');
    //销毁矿池管理
    $router->resource('invest_pledge','InvestPledgeController');
    //LP矿池管理
    $router->resource('invest_lp','InvestLpController');

    //主流币管理
    $router->resource('main_currency','MainCurrencyController');
    $router->resource('main_currency_exchange','MainCurrencyExchangeController');

    //黑洞管理
    $router->resource('black','BlackHoleController');
    //动态收益管理
    $router->resource('dynamic_rate','DynamicRateController');

    $router->resource('level_config','LevelConfigController');

    $router->resource('pool','PoolController');

    $router->resource('ido','IdoController');
    $router->resource('ido_log','IdoLogController');

    //推荐列表
    $router->resource('recommend','RecommendListController');
    //推荐管理
    $router->resource('exhibit','ProductExhibitController');
    //商品类别管理
    $router->resource('product_category','ProductsCategoryController');
    //商品管理
    $router->resource('product','ProductController');
    //产品管理
    $router->resource('coinproduct','CoinProductController');
    ##秒杀商品
    $router->resource('spikeproduct','SpikeProductController');

    //商户申请列表
    $router->resource('shop_category','ShopCategoryController');
    //商户类型配置
    $router->resource('shop_application','ShopApplicationController');

    //社区申请列表
    $router->resource('apply_application','GroupApplicationController');
    //社区类型配置
    $router->resource('group_category','GroupCategoryController');

    //节点配置
    $router->resource('node_category','NodeCategoryController');


    $router->resource('order','OrderController');
    $router->resource('orderlist','OrderListController');

    $router->resource('payedorder','PayedController');

    $router->resource('sendorder','OrderDelivedController');

    $router->resource('pointset','PointSetController');
    $router->resource('usercert','UserCertController');
    $router->resource('releaseorder','ReleaseOrderController');


    $router->resource('speedlist','SpeedListController');
    $router->resource('speedorder','SpeedOrderController');

    $router->resource('wageset','WageMoneyController');

    $router->prefix('api')->group(function ($router) {
        $router->get('category', 'ProductsCategoryController@category');
    });

    //禁用插件管理，防止webshell
//    $router->any('auth/extensions',function (){
//        die();
//    });
});
