<?php

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

require __DIR__ . '/response.php';
require __DIR__ . '/calculator.php';

/**
 * 根据路径生成一个图片标签
 *
 * @param string       $url
 * @param string $disk
 * @param int    $width
 * @param int    $height
 * @return string
 */
function image($url, $disk = 'public', int $width = 50, int $height = 50) : string
{
    if (is_null($url) || empty($url)) {

        $url = get404Image();
    } else {

        $url = assertUrl($url, $disk);
    }

    return "<img width='{$width}' height='{$height}' src='{$url}' />";
}

function assertUrl($url, $disk = 'public')
{
    static $driver  = null;

    if (is_null($url) || empty($url)) {

        return get404Image();
    }

    if (is_null($driver)) {
        $driver = Storage::disk($disk);
    }

    if (! \Illuminate\Support\Str::startsWith($url, 'http')) {
        $url = $driver->url($url);
    }

    return $url;
}

function get404Image()
{
    return asset('images/404.jpg');
}


/**
 * 把字符串变成固定长度
 *
 * @param     $str
 * @param     $length
 * @param     $padString
 * @param int $padType
 * @return bool|string
 */
function fixStrLength($str, $length, $padString = '0', $padType = STR_PAD_LEFT)
{
    if (strlen($str) > $length) {
        return substr($str, strlen($str) - $length);
    } elseif (strlen($str) < $length) {
        return str_pad($str, $length, $padString, $padType);
    }

    return $str;
}


/**
 * 价格保留两位小数
 *
 * @param $price
 * @return float|int
 */
function ceilTwoPrice($price)
{
    return round($price, 2);
}


/**
 * 或者设置的配置项
 *
 * @param $key
 * @param null $default
 * @return mixed|null
 */
function setting($key, $default = null)
{
    $val = \Illuminate\Support\Facades\Cache::get('config:'.$key);
    if (is_null($val)) {

        $val = \App\Models\Config::query()->where('key', $key)->value('value');
        if (is_null($val)) {
            return $default;
        }

        \Illuminate\Support\Facades\Cache::put('config:'.$key, $val);
    }

    return $val;
}

/**
 * 生成系统日志
 *
 * @param       $description
 * @param array $input
 */
function createSystemLog($description, $input = [])
{
    $operate = new \Encore\Admin\Auth\Database\OperationLog();
    $operate->path = config('app.url');
    $operate->method = 'GET';
    $operate->ip = '127.0.0.1';
    $operate->input = json_encode($input);
    $operate->description = $description;
    $operate->save();
}



function getWallet($userId){
    try {
        $url = env('DAPP_GET_COIN_ADDRESS',null);
        if (empty($url)){
            return null;
        }
        $client = new Client();
        $response = $client->post($url,[
            'form_params' => [
                'userName' => $userId,
                'coinToken' => env('DAPP_COIN_TOKEN',null),
                'mainChain' => env('DAPP_MAIN_CHAIN',null)
            ]
        ]);
        $response = $response->getBody();
        Log::channel('account')->info($userId.'获取到内容'.$response);
        $response = json_decode($response,true);
        return $response['obj']['address'];
    }catch (\Exception $e){
        Log::channel('account')->info($userId.'遇到错误'.$e->getMessage().$e->getLine());
        return null;
    }
}


function getRandStr($length){
    //字符组合
    $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $len = strlen($str)-1;
    $randstr = '';
    for ($i=0;$i<$length;$i++) {
        $num=mt_rand(0,$len);
        $randstr .= $str[$num];
    }
    return $randstr;
}



/**
 * @param $email
 * @return string
 * 隐藏邮箱手机号
 */
function mail_hidden($str)
{
    if (empty($str)){
        return $str;
    }

    if (strpos($str, '@')) {
        $email_array = explode("@", $str);

        if (strlen($email_array[0]) <= 2) {
            $prevfix = substr_replace($email_array[0], '*', 1, 1);
            $rs = $prevfix . $email_array[1];
//                $prevfix = substr($str, 0, 1); //邮箱前缀
//                $count = 0;
//                $str = preg_replace('/([\d\w+_-]{0,100})@/', '*@', $str, -1, $count);
//                $rs = $prevfix . $str;
        } else if (strlen($email_array[0]) < 5) {
            $prevfix = substr_replace($email_array[0], '**', 1, 1);
            $rs = $prevfix . $email_array[1];
        } else {
            $prevfix = substr_replace($email_array[0], '***', 3, 1);
            $rs = $prevfix . $email_array[1];
        }

    } else {
        $pattern = '/(1[3458]{1}[0-9])[0-9]{4}([0-9]{4})/i';
        if (preg_match($pattern, $str)) {
            $rs = preg_replace($pattern, '$1****$2', $str); // substr_replace($name,'****',3,4);
        } else {
            $rs = substr($str, 0, 3) . "***" . substr($str, -1);
        }
    }
    return $rs;
}


function hiddenAddress($str){
    if (empty($str)) return '';
    return substr($str, 0, 4) . "*********" . substr($str, -4);
}



function logic($name){
    static $logic;
    if (!isset($logic[$name])){
        $path = '\\App\Logic\\'.ucfirst($name).'Logic';
        $logic[$name] = new $path;
    }
    return $logic[$name];
}
