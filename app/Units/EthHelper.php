<?php


namespace App\Units;

use BitWasp\Bitcoin\Crypto\Random\Random;
use BitWasp\Bitcoin\Key\Factory\HierarchicalKeyFactory;
use BitWasp\Bitcoin\Mnemonic\Bip39\Bip39Mnemonic;
use BitWasp\Bitcoin\Mnemonic\Bip39\Bip39SeedGenerator;
use BitWasp\Bitcoin\Mnemonic\MnemonicFactory;

class EthHelper
{

    /**
     * 创建随机助记词
     * @return string
     * @throws \BitWasp\Bitcoin\Exceptions\RandomBytesFailure
     */
    public function createMnemonic(){
        $random = new Random();
        // 生成随机数(initial entropy)
        $entropy = $random->bytes(Bip39Mnemonic::MIN_ENTROPY_BYTE_LEN);
        $bip39 = MnemonicFactory::bip39();
        // 通过随机数生成助记词
//        $bip39->entropyToMnemonic()
//
        return $bip39->entropyToMnemonic($entropy);
    }

    /**
     * 根据助记词获取种子
     * @param $mnemonic  string 助记词
     * @return array
     * @throws \Exception
     */
    public function getSpeedByMnemonic($mnemonic){
        $seedGenerator = new Bip39SeedGenerator();
        $seed = $seedGenerator->getSeed($mnemonic);
        $hdFactory = new HierarchicalKeyFactory();
        $master = $hdFactory->fromEntropy($seed);
        $hardened = $master->derivePath("44'/60'/0'/0/0");
        $privateKey = $hardened->getPrivateKey()->getHex();
        return [
            'seed' => $seed->getHex(),
            'mnemonic' => $mnemonic,
            'privateKey' => $privateKey
        ];
    }

    /**
     * 导出私钥和公钥
     * @param $seed
     * @return array
     * @throws \Exception
     */
    public function importKey($seed){
        $hdFactory = new HierarchicalKeyFactory();
        $master = $hdFactory->fromEntropy($seed);
        $hardened = $master->derivePath("44'/60'/0'/0/0");
        $publicKey = $hardened->getPublicKey()->getHex();
        $privateKey = $hardened->getPrivateKey()->getHex();

        $bip39 = MnemonicFactory::bip39();
        return compact('publicKey','privateKey');
    }


}
