<?php

/**
 * A helper file for Dcat Admin, to provide autocomplete information to your IDE
 *
 * This file should not be included in your code, only analyzed by your IDE!
 *
 * @author jqh <841324345@qq.com>
 */
namespace Dcat\Admin {
    use Illuminate\Support\Collection;

    /**
     * @property Grid\Column|Collection created_at
     * @property Grid\Column|Collection detail
     * @property Grid\Column|Collection id
     * @property Grid\Column|Collection name
     * @property Grid\Column|Collection type
     * @property Grid\Column|Collection updated_at
     * @property Grid\Column|Collection version
     * @property Grid\Column|Collection is_enabled
     * @property Grid\Column|Collection extension
     * @property Grid\Column|Collection icon
     * @property Grid\Column|Collection order
     * @property Grid\Column|Collection parent_id
     * @property Grid\Column|Collection uri
     * @property Grid\Column|Collection input
     * @property Grid\Column|Collection ip
     * @property Grid\Column|Collection method
     * @property Grid\Column|Collection path
     * @property Grid\Column|Collection user_id
     * @property Grid\Column|Collection menu_id
     * @property Grid\Column|Collection permission_id
     * @property Grid\Column|Collection http_method
     * @property Grid\Column|Collection http_path
     * @property Grid\Column|Collection slug
     * @property Grid\Column|Collection role_id
     * @property Grid\Column|Collection value
     * @property Grid\Column|Collection avatar
     * @property Grid\Column|Collection password
     * @property Grid\Column|Collection remember_token
     * @property Grid\Column|Collection username
     * @property Grid\Column|Collection bank_code
     * @property Grid\Column|Collection bank_name
     * @property Grid\Column|Collection status
     * @property Grid\Column|Collection banner
     * @property Grid\Column|Collection lang
     * @property Grid\Column|Collection link
     * @property Grid\Column|Collection sort
     * @property Grid\Column|Collection datestr
     * @property Grid\Column|Collection product_id
     * @property Grid\Column|Collection product_img
     * @property Grid\Column|Collection content
     * @property Grid\Column|Collection is_checked
     * @property Grid\Column|Collection quantity
     * @property Grid\Column|Collection element
     * @property Grid\Column|Collection help
     * @property Grid\Column|Collection key
     * @property Grid\Column|Collection rule
     * @property Grid\Column|Collection tab
     * @property Grid\Column|Collection connection
     * @property Grid\Column|Collection exception
     * @property Grid\Column|Collection failed_at
     * @property Grid\Column|Collection payload
     * @property Grid\Column|Collection queue
     * @property Grid\Column|Collection uuid
     * @property Grid\Column|Collection address
     * @property Grid\Column|Collection audit_remarks
     * @property Grid\Column|Collection city
     * @property Grid\Column|Collection country
     * @property Grid\Column|Collection district
     * @property Grid\Column|Collection group_id
     * @property Grid\Column|Collection province
     * @property Grid\Column|Collection grade_factor
     * @property Grid\Column|Collection lp_factor
     * @property Grid\Column|Collection rate
     * @property Grid\Column|Collection im_uid
     * @property Grid\Column|Collection money
     * @property Grid\Column|Collection remark
     * @property Grid\Column|Collection uid
     * @property Grid\Column|Collection after
     * @property Grid\Column|Collection amount_type
     * @property Grid\Column|Collection before
     * @property Grid\Column|Collection from_id
     * @property Grid\Column|Collection total
     * @property Grid\Column|Collection coin_level_rate
     * @property Grid\Column|Collection coin_rate
     * @property Grid\Column|Collection level
     * @property Grid\Column|Collection same_level_count
     * @property Grid\Column|Collection same_level_rate
     * @property Grid\Column|Collection team_vip
     * @property Grid\Column|Collection teamperfor
     * @property Grid\Column|Collection num
     * @property Grid\Column|Collection fan_rate
     * @property Grid\Column|Collection from_coin
     * @property Grid\Column|Collection from_coin_name
     * @property Grid\Column|Collection is_search
     * @property Grid\Column|Collection to_coin
     * @property Grid\Column|Collection to_coin_name
     * @property Grid\Column|Collection line_date
     * @property Grid\Column|Collection delivery_time
     * @property Grid\Column|Collection get_user_name
     * @property Grid\Column|Collection get_user_phone
     * @property Grid\Column|Collection give_coin
     * @property Grid\Column|Collection goods_num
     * @property Grid\Column|Collection is_appraise
     * @property Grid\Column|Collection logistics_fee
     * @property Grid\Column|Collection order_integral
     * @property Grid\Column|Collection order_money
     * @property Grid\Column|Collection order_no
     * @property Grid\Column|Collection order_settlement_status
     * @property Grid\Column|Collection order_settlement_time
     * @property Grid\Column|Collection order_status
     * @property Grid\Column|Collection pay_price
     * @property Grid\Column|Collection pay_remark
     * @property Grid\Column|Collection pay_remarks
     * @property Grid\Column|Collection pay_time
     * @property Grid\Column|Collection payment_type
     * @property Grid\Column|Collection pool_coin
     * @property Grid\Column|Collection ship_company
     * @property Grid\Column|Collection ship_no
     * @property Grid\Column|Collection shopping_id
     * @property Grid\Column|Collection total_usdt
     * @property Grid\Column|Collection attitude_star
     * @property Grid\Column|Collection desc_star
     * @property Grid\Column|Collection info
     * @property Grid\Column|Collection logistics_star
     * @property Grid\Column|Collection order_id
     * @property Grid\Column|Collection attr_id
     * @property Grid\Column|Collection indirect_num
     * @property Grid\Column|Collection order_price
     * @property Grid\Column|Collection product_count
     * @property Grid\Column|Collection product_name
     * @property Grid\Column|Collection product_price
     * @property Grid\Column|Collection product_type
     * @property Grid\Column|Collection share_num
     * @property Grid\Column|Collection shop_id
     * @property Grid\Column|Collection supplier_name
     * @property Grid\Column|Collection usdt_price
     * @property Grid\Column|Collection image
     * @property Grid\Column|Collection price
     * @property Grid\Column|Collection total_price
     * @property Grid\Column|Collection audit_time
     * @property Grid\Column|Collection audit_why
     * @property Grid\Column|Collection phone
     * @property Grid\Column|Collection product_status
     * @property Grid\Column|Collection return_no
     * @property Grid\Column|Collection state
     * @property Grid\Column|Collection why
     * @property Grid\Column|Collection point_pool
     * @property Grid\Column|Collection end_money
     * @property Grid\Column|Collection start_money
     * @property Grid\Column|Collection carts
     * @property Grid\Column|Collection category_id
     * @property Grid\Column|Collection category_one
     * @property Grid\Column|Collection coin_price
     * @property Grid\Column|Collection end_time
     * @property Grid\Column|Collection is_audit_push
     * @property Grid\Column|Collection is_pay
     * @property Grid\Column|Collection main_picture
     * @property Grid\Column|Collection picture
     * @property Grid\Column|Collection sales
     * @property Grid\Column|Collection start_time
     * @property Grid\Column|Collection stock
     * @property Grid\Column|Collection sortorder
     * @property Grid\Column|Collection img_id
     * @property Grid\Column|Collection orders
     * @property Grid\Column|Collection chargeid
     * @property Grid\Column|Collection coin_num
     * @property Grid\Column|Collection finished_at
     * @property Grid\Column|Collection hash
     * @property Grid\Column|Collection remarks
     * @property Grid\Column|Collection to_address
     * @property Grid\Column|Collection cycle
     * @property Grid\Column|Collection finshed_at
     * @property Grid\Column|Collection release_date
     * @property Grid\Column|Collection released_num
     * @property Grid\Column|Collection reset_num
     * @property Grid\Column|Collection speed_num
     * @property Grid\Column|Collection total_num
     * @property Grid\Column|Collection award_rate
     * @property Grid\Column|Collection end_count
     * @property Grid\Column|Collection start_count
     * @property Grid\Column|Collection audit_status
     * @property Grid\Column|Collection company_logo
     * @property Grid\Column|Collection company_zhi
     * @property Grid\Column|Collection legal_card1
     * @property Grid\Column|Collection legal_card2
     * @property Grid\Column|Collection shop_category_id
     * @property Grid\Column|Collection apply_grade
     * @property Grid\Column|Collection destroy_rate
     * @property Grid\Column|Collection jian_rate
     * @property Grid\Column|Collection pot_rate
     * @property Grid\Column|Collection sign_date
     * @property Grid\Column|Collection day
     * @property Grid\Column|Collection end_date
     * @property Grid\Column|Collection speed_date
     * @property Grid\Column|Collection speed_id
     * @property Grid\Column|Collection start_date
     * @property Grid\Column|Collection other_id
     * @property Grid\Column|Collection bank_id
     * @property Grid\Column|Collection bank_no
     * @property Grid\Column|Collection bank_phone
     * @property Grid\Column|Collection bank_user
     * @property Grid\Column|Collection is_default
     * @property Grid\Column|Collection idcard_no
     * @property Grid\Column|Collection real_name
     * @property Grid\Column|Collection activate
     * @property Grid\Column|Collection amount
     * @property Grid\Column|Collection cert_status
     * @property Grid\Column|Collection code
     * @property Grid\Column|Collection coin_money
     * @property Grid\Column|Collection deep
     * @property Grid\Column|Collection group_num
     * @property Grid\Column|Collection is_shop
     * @property Grid\Column|Collection myperfor
     * @property Grid\Column|Collection nickname
     * @property Grid\Column|Collection parent_name
     * @property Grid\Column|Collection regip
     * @property Grid\Column|Collection share_income
     * @property Grid\Column|Collection share_status
     * @property Grid\Column|Collection sign_money
     * @property Grid\Column|Collection total_income
     * @property Grid\Column|Collection total_recharge
     * @property Grid\Column|Collection total_withdraw
     * @property Grid\Column|Collection trade_password
     * @property Grid\Column|Collection zhi_num
     * @property Grid\Column|Collection is_valid
     * @property Grid\Column|Collection wage_month
     * @property Grid\Column|Collection end
     * @property Grid\Column|Collection profit
     * @property Grid\Column|Collection start
     * @property Grid\Column|Collection ac_amount
     * @property Grid\Column|Collection fee
     * @property Grid\Column|Collection fee_amount
     * @property Grid\Column|Collection finsh_time
     * @property Grid\Column|Collection no
     *
     * @method Grid\Column|Collection created_at(string $label = null)
     * @method Grid\Column|Collection detail(string $label = null)
     * @method Grid\Column|Collection id(string $label = null)
     * @method Grid\Column|Collection name(string $label = null)
     * @method Grid\Column|Collection type(string $label = null)
     * @method Grid\Column|Collection updated_at(string $label = null)
     * @method Grid\Column|Collection version(string $label = null)
     * @method Grid\Column|Collection is_enabled(string $label = null)
     * @method Grid\Column|Collection extension(string $label = null)
     * @method Grid\Column|Collection icon(string $label = null)
     * @method Grid\Column|Collection order(string $label = null)
     * @method Grid\Column|Collection parent_id(string $label = null)
     * @method Grid\Column|Collection uri(string $label = null)
     * @method Grid\Column|Collection input(string $label = null)
     * @method Grid\Column|Collection ip(string $label = null)
     * @method Grid\Column|Collection method(string $label = null)
     * @method Grid\Column|Collection path(string $label = null)
     * @method Grid\Column|Collection user_id(string $label = null)
     * @method Grid\Column|Collection menu_id(string $label = null)
     * @method Grid\Column|Collection permission_id(string $label = null)
     * @method Grid\Column|Collection http_method(string $label = null)
     * @method Grid\Column|Collection http_path(string $label = null)
     * @method Grid\Column|Collection slug(string $label = null)
     * @method Grid\Column|Collection role_id(string $label = null)
     * @method Grid\Column|Collection value(string $label = null)
     * @method Grid\Column|Collection avatar(string $label = null)
     * @method Grid\Column|Collection password(string $label = null)
     * @method Grid\Column|Collection remember_token(string $label = null)
     * @method Grid\Column|Collection username(string $label = null)
     * @method Grid\Column|Collection bank_code(string $label = null)
     * @method Grid\Column|Collection bank_name(string $label = null)
     * @method Grid\Column|Collection status(string $label = null)
     * @method Grid\Column|Collection banner(string $label = null)
     * @method Grid\Column|Collection lang(string $label = null)
     * @method Grid\Column|Collection link(string $label = null)
     * @method Grid\Column|Collection sort(string $label = null)
     * @method Grid\Column|Collection datestr(string $label = null)
     * @method Grid\Column|Collection product_id(string $label = null)
     * @method Grid\Column|Collection product_img(string $label = null)
     * @method Grid\Column|Collection content(string $label = null)
     * @method Grid\Column|Collection is_checked(string $label = null)
     * @method Grid\Column|Collection quantity(string $label = null)
     * @method Grid\Column|Collection element(string $label = null)
     * @method Grid\Column|Collection help(string $label = null)
     * @method Grid\Column|Collection key(string $label = null)
     * @method Grid\Column|Collection rule(string $label = null)
     * @method Grid\Column|Collection tab(string $label = null)
     * @method Grid\Column|Collection connection(string $label = null)
     * @method Grid\Column|Collection exception(string $label = null)
     * @method Grid\Column|Collection failed_at(string $label = null)
     * @method Grid\Column|Collection payload(string $label = null)
     * @method Grid\Column|Collection queue(string $label = null)
     * @method Grid\Column|Collection uuid(string $label = null)
     * @method Grid\Column|Collection address(string $label = null)
     * @method Grid\Column|Collection audit_remarks(string $label = null)
     * @method Grid\Column|Collection city(string $label = null)
     * @method Grid\Column|Collection country(string $label = null)
     * @method Grid\Column|Collection district(string $label = null)
     * @method Grid\Column|Collection group_id(string $label = null)
     * @method Grid\Column|Collection province(string $label = null)
     * @method Grid\Column|Collection grade_factor(string $label = null)
     * @method Grid\Column|Collection lp_factor(string $label = null)
     * @method Grid\Column|Collection rate(string $label = null)
     * @method Grid\Column|Collection im_uid(string $label = null)
     * @method Grid\Column|Collection money(string $label = null)
     * @method Grid\Column|Collection remark(string $label = null)
     * @method Grid\Column|Collection uid(string $label = null)
     * @method Grid\Column|Collection after(string $label = null)
     * @method Grid\Column|Collection amount_type(string $label = null)
     * @method Grid\Column|Collection before(string $label = null)
     * @method Grid\Column|Collection from_id(string $label = null)
     * @method Grid\Column|Collection total(string $label = null)
     * @method Grid\Column|Collection coin_level_rate(string $label = null)
     * @method Grid\Column|Collection coin_rate(string $label = null)
     * @method Grid\Column|Collection level(string $label = null)
     * @method Grid\Column|Collection same_level_count(string $label = null)
     * @method Grid\Column|Collection same_level_rate(string $label = null)
     * @method Grid\Column|Collection team_vip(string $label = null)
     * @method Grid\Column|Collection teamperfor(string $label = null)
     * @method Grid\Column|Collection num(string $label = null)
     * @method Grid\Column|Collection fan_rate(string $label = null)
     * @method Grid\Column|Collection from_coin(string $label = null)
     * @method Grid\Column|Collection from_coin_name(string $label = null)
     * @method Grid\Column|Collection is_search(string $label = null)
     * @method Grid\Column|Collection to_coin(string $label = null)
     * @method Grid\Column|Collection to_coin_name(string $label = null)
     * @method Grid\Column|Collection line_date(string $label = null)
     * @method Grid\Column|Collection delivery_time(string $label = null)
     * @method Grid\Column|Collection get_user_name(string $label = null)
     * @method Grid\Column|Collection get_user_phone(string $label = null)
     * @method Grid\Column|Collection give_coin(string $label = null)
     * @method Grid\Column|Collection goods_num(string $label = null)
     * @method Grid\Column|Collection is_appraise(string $label = null)
     * @method Grid\Column|Collection logistics_fee(string $label = null)
     * @method Grid\Column|Collection order_integral(string $label = null)
     * @method Grid\Column|Collection order_money(string $label = null)
     * @method Grid\Column|Collection order_no(string $label = null)
     * @method Grid\Column|Collection order_settlement_status(string $label = null)
     * @method Grid\Column|Collection order_settlement_time(string $label = null)
     * @method Grid\Column|Collection order_status(string $label = null)
     * @method Grid\Column|Collection pay_price(string $label = null)
     * @method Grid\Column|Collection pay_remark(string $label = null)
     * @method Grid\Column|Collection pay_remarks(string $label = null)
     * @method Grid\Column|Collection pay_time(string $label = null)
     * @method Grid\Column|Collection payment_type(string $label = null)
     * @method Grid\Column|Collection pool_coin(string $label = null)
     * @method Grid\Column|Collection ship_company(string $label = null)
     * @method Grid\Column|Collection ship_no(string $label = null)
     * @method Grid\Column|Collection shopping_id(string $label = null)
     * @method Grid\Column|Collection total_usdt(string $label = null)
     * @method Grid\Column|Collection attitude_star(string $label = null)
     * @method Grid\Column|Collection desc_star(string $label = null)
     * @method Grid\Column|Collection info(string $label = null)
     * @method Grid\Column|Collection logistics_star(string $label = null)
     * @method Grid\Column|Collection order_id(string $label = null)
     * @method Grid\Column|Collection attr_id(string $label = null)
     * @method Grid\Column|Collection indirect_num(string $label = null)
     * @method Grid\Column|Collection order_price(string $label = null)
     * @method Grid\Column|Collection product_count(string $label = null)
     * @method Grid\Column|Collection product_name(string $label = null)
     * @method Grid\Column|Collection product_price(string $label = null)
     * @method Grid\Column|Collection product_type(string $label = null)
     * @method Grid\Column|Collection share_num(string $label = null)
     * @method Grid\Column|Collection shop_id(string $label = null)
     * @method Grid\Column|Collection supplier_name(string $label = null)
     * @method Grid\Column|Collection usdt_price(string $label = null)
     * @method Grid\Column|Collection image(string $label = null)
     * @method Grid\Column|Collection price(string $label = null)
     * @method Grid\Column|Collection total_price(string $label = null)
     * @method Grid\Column|Collection audit_time(string $label = null)
     * @method Grid\Column|Collection audit_why(string $label = null)
     * @method Grid\Column|Collection phone(string $label = null)
     * @method Grid\Column|Collection product_status(string $label = null)
     * @method Grid\Column|Collection return_no(string $label = null)
     * @method Grid\Column|Collection state(string $label = null)
     * @method Grid\Column|Collection why(string $label = null)
     * @method Grid\Column|Collection point_pool(string $label = null)
     * @method Grid\Column|Collection end_money(string $label = null)
     * @method Grid\Column|Collection start_money(string $label = null)
     * @method Grid\Column|Collection carts(string $label = null)
     * @method Grid\Column|Collection category_id(string $label = null)
     * @method Grid\Column|Collection category_one(string $label = null)
     * @method Grid\Column|Collection coin_price(string $label = null)
     * @method Grid\Column|Collection end_time(string $label = null)
     * @method Grid\Column|Collection is_audit_push(string $label = null)
     * @method Grid\Column|Collection is_pay(string $label = null)
     * @method Grid\Column|Collection main_picture(string $label = null)
     * @method Grid\Column|Collection picture(string $label = null)
     * @method Grid\Column|Collection sales(string $label = null)
     * @method Grid\Column|Collection start_time(string $label = null)
     * @method Grid\Column|Collection stock(string $label = null)
     * @method Grid\Column|Collection sortorder(string $label = null)
     * @method Grid\Column|Collection img_id(string $label = null)
     * @method Grid\Column|Collection orders(string $label = null)
     * @method Grid\Column|Collection chargeid(string $label = null)
     * @method Grid\Column|Collection coin_num(string $label = null)
     * @method Grid\Column|Collection finished_at(string $label = null)
     * @method Grid\Column|Collection hash(string $label = null)
     * @method Grid\Column|Collection remarks(string $label = null)
     * @method Grid\Column|Collection to_address(string $label = null)
     * @method Grid\Column|Collection cycle(string $label = null)
     * @method Grid\Column|Collection finshed_at(string $label = null)
     * @method Grid\Column|Collection release_date(string $label = null)
     * @method Grid\Column|Collection released_num(string $label = null)
     * @method Grid\Column|Collection reset_num(string $label = null)
     * @method Grid\Column|Collection speed_num(string $label = null)
     * @method Grid\Column|Collection total_num(string $label = null)
     * @method Grid\Column|Collection award_rate(string $label = null)
     * @method Grid\Column|Collection end_count(string $label = null)
     * @method Grid\Column|Collection start_count(string $label = null)
     * @method Grid\Column|Collection audit_status(string $label = null)
     * @method Grid\Column|Collection company_logo(string $label = null)
     * @method Grid\Column|Collection company_zhi(string $label = null)
     * @method Grid\Column|Collection legal_card1(string $label = null)
     * @method Grid\Column|Collection legal_card2(string $label = null)
     * @method Grid\Column|Collection shop_category_id(string $label = null)
     * @method Grid\Column|Collection apply_grade(string $label = null)
     * @method Grid\Column|Collection destroy_rate(string $label = null)
     * @method Grid\Column|Collection jian_rate(string $label = null)
     * @method Grid\Column|Collection pot_rate(string $label = null)
     * @method Grid\Column|Collection sign_date(string $label = null)
     * @method Grid\Column|Collection day(string $label = null)
     * @method Grid\Column|Collection end_date(string $label = null)
     * @method Grid\Column|Collection speed_date(string $label = null)
     * @method Grid\Column|Collection speed_id(string $label = null)
     * @method Grid\Column|Collection start_date(string $label = null)
     * @method Grid\Column|Collection other_id(string $label = null)
     * @method Grid\Column|Collection bank_id(string $label = null)
     * @method Grid\Column|Collection bank_no(string $label = null)
     * @method Grid\Column|Collection bank_phone(string $label = null)
     * @method Grid\Column|Collection bank_user(string $label = null)
     * @method Grid\Column|Collection is_default(string $label = null)
     * @method Grid\Column|Collection idcard_no(string $label = null)
     * @method Grid\Column|Collection real_name(string $label = null)
     * @method Grid\Column|Collection activate(string $label = null)
     * @method Grid\Column|Collection amount(string $label = null)
     * @method Grid\Column|Collection cert_status(string $label = null)
     * @method Grid\Column|Collection code(string $label = null)
     * @method Grid\Column|Collection coin_money(string $label = null)
     * @method Grid\Column|Collection deep(string $label = null)
     * @method Grid\Column|Collection group_num(string $label = null)
     * @method Grid\Column|Collection is_shop(string $label = null)
     * @method Grid\Column|Collection myperfor(string $label = null)
     * @method Grid\Column|Collection nickname(string $label = null)
     * @method Grid\Column|Collection parent_name(string $label = null)
     * @method Grid\Column|Collection regip(string $label = null)
     * @method Grid\Column|Collection share_income(string $label = null)
     * @method Grid\Column|Collection share_status(string $label = null)
     * @method Grid\Column|Collection sign_money(string $label = null)
     * @method Grid\Column|Collection total_income(string $label = null)
     * @method Grid\Column|Collection total_recharge(string $label = null)
     * @method Grid\Column|Collection total_withdraw(string $label = null)
     * @method Grid\Column|Collection trade_password(string $label = null)
     * @method Grid\Column|Collection zhi_num(string $label = null)
     * @method Grid\Column|Collection is_valid(string $label = null)
     * @method Grid\Column|Collection wage_month(string $label = null)
     * @method Grid\Column|Collection end(string $label = null)
     * @method Grid\Column|Collection profit(string $label = null)
     * @method Grid\Column|Collection start(string $label = null)
     * @method Grid\Column|Collection ac_amount(string $label = null)
     * @method Grid\Column|Collection fee(string $label = null)
     * @method Grid\Column|Collection fee_amount(string $label = null)
     * @method Grid\Column|Collection finsh_time(string $label = null)
     * @method Grid\Column|Collection no(string $label = null)
     */
    class Grid {}

    class MiniGrid extends Grid {}

    /**
     * @property Show\Field|Collection created_at
     * @property Show\Field|Collection detail
     * @property Show\Field|Collection id
     * @property Show\Field|Collection name
     * @property Show\Field|Collection type
     * @property Show\Field|Collection updated_at
     * @property Show\Field|Collection version
     * @property Show\Field|Collection is_enabled
     * @property Show\Field|Collection extension
     * @property Show\Field|Collection icon
     * @property Show\Field|Collection order
     * @property Show\Field|Collection parent_id
     * @property Show\Field|Collection uri
     * @property Show\Field|Collection input
     * @property Show\Field|Collection ip
     * @property Show\Field|Collection method
     * @property Show\Field|Collection path
     * @property Show\Field|Collection user_id
     * @property Show\Field|Collection menu_id
     * @property Show\Field|Collection permission_id
     * @property Show\Field|Collection http_method
     * @property Show\Field|Collection http_path
     * @property Show\Field|Collection slug
     * @property Show\Field|Collection role_id
     * @property Show\Field|Collection value
     * @property Show\Field|Collection avatar
     * @property Show\Field|Collection password
     * @property Show\Field|Collection remember_token
     * @property Show\Field|Collection username
     * @property Show\Field|Collection bank_code
     * @property Show\Field|Collection bank_name
     * @property Show\Field|Collection status
     * @property Show\Field|Collection banner
     * @property Show\Field|Collection lang
     * @property Show\Field|Collection link
     * @property Show\Field|Collection sort
     * @property Show\Field|Collection datestr
     * @property Show\Field|Collection product_id
     * @property Show\Field|Collection product_img
     * @property Show\Field|Collection content
     * @property Show\Field|Collection is_checked
     * @property Show\Field|Collection quantity
     * @property Show\Field|Collection element
     * @property Show\Field|Collection help
     * @property Show\Field|Collection key
     * @property Show\Field|Collection rule
     * @property Show\Field|Collection tab
     * @property Show\Field|Collection connection
     * @property Show\Field|Collection exception
     * @property Show\Field|Collection failed_at
     * @property Show\Field|Collection payload
     * @property Show\Field|Collection queue
     * @property Show\Field|Collection uuid
     * @property Show\Field|Collection address
     * @property Show\Field|Collection audit_remarks
     * @property Show\Field|Collection city
     * @property Show\Field|Collection country
     * @property Show\Field|Collection district
     * @property Show\Field|Collection group_id
     * @property Show\Field|Collection province
     * @property Show\Field|Collection grade_factor
     * @property Show\Field|Collection lp_factor
     * @property Show\Field|Collection rate
     * @property Show\Field|Collection im_uid
     * @property Show\Field|Collection money
     * @property Show\Field|Collection remark
     * @property Show\Field|Collection uid
     * @property Show\Field|Collection after
     * @property Show\Field|Collection amount_type
     * @property Show\Field|Collection before
     * @property Show\Field|Collection from_id
     * @property Show\Field|Collection total
     * @property Show\Field|Collection coin_level_rate
     * @property Show\Field|Collection coin_rate
     * @property Show\Field|Collection level
     * @property Show\Field|Collection same_level_count
     * @property Show\Field|Collection same_level_rate
     * @property Show\Field|Collection team_vip
     * @property Show\Field|Collection teamperfor
     * @property Show\Field|Collection num
     * @property Show\Field|Collection fan_rate
     * @property Show\Field|Collection from_coin
     * @property Show\Field|Collection from_coin_name
     * @property Show\Field|Collection is_search
     * @property Show\Field|Collection to_coin
     * @property Show\Field|Collection to_coin_name
     * @property Show\Field|Collection line_date
     * @property Show\Field|Collection delivery_time
     * @property Show\Field|Collection get_user_name
     * @property Show\Field|Collection get_user_phone
     * @property Show\Field|Collection give_coin
     * @property Show\Field|Collection goods_num
     * @property Show\Field|Collection is_appraise
     * @property Show\Field|Collection logistics_fee
     * @property Show\Field|Collection order_integral
     * @property Show\Field|Collection order_money
     * @property Show\Field|Collection order_no
     * @property Show\Field|Collection order_settlement_status
     * @property Show\Field|Collection order_settlement_time
     * @property Show\Field|Collection order_status
     * @property Show\Field|Collection pay_price
     * @property Show\Field|Collection pay_remark
     * @property Show\Field|Collection pay_remarks
     * @property Show\Field|Collection pay_time
     * @property Show\Field|Collection payment_type
     * @property Show\Field|Collection pool_coin
     * @property Show\Field|Collection ship_company
     * @property Show\Field|Collection ship_no
     * @property Show\Field|Collection shopping_id
     * @property Show\Field|Collection total_usdt
     * @property Show\Field|Collection attitude_star
     * @property Show\Field|Collection desc_star
     * @property Show\Field|Collection info
     * @property Show\Field|Collection logistics_star
     * @property Show\Field|Collection order_id
     * @property Show\Field|Collection attr_id
     * @property Show\Field|Collection indirect_num
     * @property Show\Field|Collection order_price
     * @property Show\Field|Collection product_count
     * @property Show\Field|Collection product_name
     * @property Show\Field|Collection product_price
     * @property Show\Field|Collection product_type
     * @property Show\Field|Collection share_num
     * @property Show\Field|Collection shop_id
     * @property Show\Field|Collection supplier_name
     * @property Show\Field|Collection usdt_price
     * @property Show\Field|Collection image
     * @property Show\Field|Collection price
     * @property Show\Field|Collection total_price
     * @property Show\Field|Collection audit_time
     * @property Show\Field|Collection audit_why
     * @property Show\Field|Collection phone
     * @property Show\Field|Collection product_status
     * @property Show\Field|Collection return_no
     * @property Show\Field|Collection state
     * @property Show\Field|Collection why
     * @property Show\Field|Collection point_pool
     * @property Show\Field|Collection end_money
     * @property Show\Field|Collection start_money
     * @property Show\Field|Collection carts
     * @property Show\Field|Collection category_id
     * @property Show\Field|Collection category_one
     * @property Show\Field|Collection coin_price
     * @property Show\Field|Collection end_time
     * @property Show\Field|Collection is_audit_push
     * @property Show\Field|Collection is_pay
     * @property Show\Field|Collection main_picture
     * @property Show\Field|Collection picture
     * @property Show\Field|Collection sales
     * @property Show\Field|Collection start_time
     * @property Show\Field|Collection stock
     * @property Show\Field|Collection sortorder
     * @property Show\Field|Collection img_id
     * @property Show\Field|Collection orders
     * @property Show\Field|Collection chargeid
     * @property Show\Field|Collection coin_num
     * @property Show\Field|Collection finished_at
     * @property Show\Field|Collection hash
     * @property Show\Field|Collection remarks
     * @property Show\Field|Collection to_address
     * @property Show\Field|Collection cycle
     * @property Show\Field|Collection finshed_at
     * @property Show\Field|Collection release_date
     * @property Show\Field|Collection released_num
     * @property Show\Field|Collection reset_num
     * @property Show\Field|Collection speed_num
     * @property Show\Field|Collection total_num
     * @property Show\Field|Collection award_rate
     * @property Show\Field|Collection end_count
     * @property Show\Field|Collection start_count
     * @property Show\Field|Collection audit_status
     * @property Show\Field|Collection company_logo
     * @property Show\Field|Collection company_zhi
     * @property Show\Field|Collection legal_card1
     * @property Show\Field|Collection legal_card2
     * @property Show\Field|Collection shop_category_id
     * @property Show\Field|Collection apply_grade
     * @property Show\Field|Collection destroy_rate
     * @property Show\Field|Collection jian_rate
     * @property Show\Field|Collection pot_rate
     * @property Show\Field|Collection sign_date
     * @property Show\Field|Collection day
     * @property Show\Field|Collection end_date
     * @property Show\Field|Collection speed_date
     * @property Show\Field|Collection speed_id
     * @property Show\Field|Collection start_date
     * @property Show\Field|Collection other_id
     * @property Show\Field|Collection bank_id
     * @property Show\Field|Collection bank_no
     * @property Show\Field|Collection bank_phone
     * @property Show\Field|Collection bank_user
     * @property Show\Field|Collection is_default
     * @property Show\Field|Collection idcard_no
     * @property Show\Field|Collection real_name
     * @property Show\Field|Collection activate
     * @property Show\Field|Collection amount
     * @property Show\Field|Collection cert_status
     * @property Show\Field|Collection code
     * @property Show\Field|Collection coin_money
     * @property Show\Field|Collection deep
     * @property Show\Field|Collection group_num
     * @property Show\Field|Collection is_shop
     * @property Show\Field|Collection myperfor
     * @property Show\Field|Collection nickname
     * @property Show\Field|Collection parent_name
     * @property Show\Field|Collection regip
     * @property Show\Field|Collection share_income
     * @property Show\Field|Collection share_status
     * @property Show\Field|Collection sign_money
     * @property Show\Field|Collection total_income
     * @property Show\Field|Collection total_recharge
     * @property Show\Field|Collection total_withdraw
     * @property Show\Field|Collection trade_password
     * @property Show\Field|Collection zhi_num
     * @property Show\Field|Collection is_valid
     * @property Show\Field|Collection wage_month
     * @property Show\Field|Collection end
     * @property Show\Field|Collection profit
     * @property Show\Field|Collection start
     * @property Show\Field|Collection ac_amount
     * @property Show\Field|Collection fee
     * @property Show\Field|Collection fee_amount
     * @property Show\Field|Collection finsh_time
     * @property Show\Field|Collection no
     *
     * @method Show\Field|Collection created_at(string $label = null)
     * @method Show\Field|Collection detail(string $label = null)
     * @method Show\Field|Collection id(string $label = null)
     * @method Show\Field|Collection name(string $label = null)
     * @method Show\Field|Collection type(string $label = null)
     * @method Show\Field|Collection updated_at(string $label = null)
     * @method Show\Field|Collection version(string $label = null)
     * @method Show\Field|Collection is_enabled(string $label = null)
     * @method Show\Field|Collection extension(string $label = null)
     * @method Show\Field|Collection icon(string $label = null)
     * @method Show\Field|Collection order(string $label = null)
     * @method Show\Field|Collection parent_id(string $label = null)
     * @method Show\Field|Collection uri(string $label = null)
     * @method Show\Field|Collection input(string $label = null)
     * @method Show\Field|Collection ip(string $label = null)
     * @method Show\Field|Collection method(string $label = null)
     * @method Show\Field|Collection path(string $label = null)
     * @method Show\Field|Collection user_id(string $label = null)
     * @method Show\Field|Collection menu_id(string $label = null)
     * @method Show\Field|Collection permission_id(string $label = null)
     * @method Show\Field|Collection http_method(string $label = null)
     * @method Show\Field|Collection http_path(string $label = null)
     * @method Show\Field|Collection slug(string $label = null)
     * @method Show\Field|Collection role_id(string $label = null)
     * @method Show\Field|Collection value(string $label = null)
     * @method Show\Field|Collection avatar(string $label = null)
     * @method Show\Field|Collection password(string $label = null)
     * @method Show\Field|Collection remember_token(string $label = null)
     * @method Show\Field|Collection username(string $label = null)
     * @method Show\Field|Collection bank_code(string $label = null)
     * @method Show\Field|Collection bank_name(string $label = null)
     * @method Show\Field|Collection status(string $label = null)
     * @method Show\Field|Collection banner(string $label = null)
     * @method Show\Field|Collection lang(string $label = null)
     * @method Show\Field|Collection link(string $label = null)
     * @method Show\Field|Collection sort(string $label = null)
     * @method Show\Field|Collection datestr(string $label = null)
     * @method Show\Field|Collection product_id(string $label = null)
     * @method Show\Field|Collection product_img(string $label = null)
     * @method Show\Field|Collection content(string $label = null)
     * @method Show\Field|Collection is_checked(string $label = null)
     * @method Show\Field|Collection quantity(string $label = null)
     * @method Show\Field|Collection element(string $label = null)
     * @method Show\Field|Collection help(string $label = null)
     * @method Show\Field|Collection key(string $label = null)
     * @method Show\Field|Collection rule(string $label = null)
     * @method Show\Field|Collection tab(string $label = null)
     * @method Show\Field|Collection connection(string $label = null)
     * @method Show\Field|Collection exception(string $label = null)
     * @method Show\Field|Collection failed_at(string $label = null)
     * @method Show\Field|Collection payload(string $label = null)
     * @method Show\Field|Collection queue(string $label = null)
     * @method Show\Field|Collection uuid(string $label = null)
     * @method Show\Field|Collection address(string $label = null)
     * @method Show\Field|Collection audit_remarks(string $label = null)
     * @method Show\Field|Collection city(string $label = null)
     * @method Show\Field|Collection country(string $label = null)
     * @method Show\Field|Collection district(string $label = null)
     * @method Show\Field|Collection group_id(string $label = null)
     * @method Show\Field|Collection province(string $label = null)
     * @method Show\Field|Collection grade_factor(string $label = null)
     * @method Show\Field|Collection lp_factor(string $label = null)
     * @method Show\Field|Collection rate(string $label = null)
     * @method Show\Field|Collection im_uid(string $label = null)
     * @method Show\Field|Collection money(string $label = null)
     * @method Show\Field|Collection remark(string $label = null)
     * @method Show\Field|Collection uid(string $label = null)
     * @method Show\Field|Collection after(string $label = null)
     * @method Show\Field|Collection amount_type(string $label = null)
     * @method Show\Field|Collection before(string $label = null)
     * @method Show\Field|Collection from_id(string $label = null)
     * @method Show\Field|Collection total(string $label = null)
     * @method Show\Field|Collection coin_level_rate(string $label = null)
     * @method Show\Field|Collection coin_rate(string $label = null)
     * @method Show\Field|Collection level(string $label = null)
     * @method Show\Field|Collection same_level_count(string $label = null)
     * @method Show\Field|Collection same_level_rate(string $label = null)
     * @method Show\Field|Collection team_vip(string $label = null)
     * @method Show\Field|Collection teamperfor(string $label = null)
     * @method Show\Field|Collection num(string $label = null)
     * @method Show\Field|Collection fan_rate(string $label = null)
     * @method Show\Field|Collection from_coin(string $label = null)
     * @method Show\Field|Collection from_coin_name(string $label = null)
     * @method Show\Field|Collection is_search(string $label = null)
     * @method Show\Field|Collection to_coin(string $label = null)
     * @method Show\Field|Collection to_coin_name(string $label = null)
     * @method Show\Field|Collection line_date(string $label = null)
     * @method Show\Field|Collection delivery_time(string $label = null)
     * @method Show\Field|Collection get_user_name(string $label = null)
     * @method Show\Field|Collection get_user_phone(string $label = null)
     * @method Show\Field|Collection give_coin(string $label = null)
     * @method Show\Field|Collection goods_num(string $label = null)
     * @method Show\Field|Collection is_appraise(string $label = null)
     * @method Show\Field|Collection logistics_fee(string $label = null)
     * @method Show\Field|Collection order_integral(string $label = null)
     * @method Show\Field|Collection order_money(string $label = null)
     * @method Show\Field|Collection order_no(string $label = null)
     * @method Show\Field|Collection order_settlement_status(string $label = null)
     * @method Show\Field|Collection order_settlement_time(string $label = null)
     * @method Show\Field|Collection order_status(string $label = null)
     * @method Show\Field|Collection pay_price(string $label = null)
     * @method Show\Field|Collection pay_remark(string $label = null)
     * @method Show\Field|Collection pay_remarks(string $label = null)
     * @method Show\Field|Collection pay_time(string $label = null)
     * @method Show\Field|Collection payment_type(string $label = null)
     * @method Show\Field|Collection pool_coin(string $label = null)
     * @method Show\Field|Collection ship_company(string $label = null)
     * @method Show\Field|Collection ship_no(string $label = null)
     * @method Show\Field|Collection shopping_id(string $label = null)
     * @method Show\Field|Collection total_usdt(string $label = null)
     * @method Show\Field|Collection attitude_star(string $label = null)
     * @method Show\Field|Collection desc_star(string $label = null)
     * @method Show\Field|Collection info(string $label = null)
     * @method Show\Field|Collection logistics_star(string $label = null)
     * @method Show\Field|Collection order_id(string $label = null)
     * @method Show\Field|Collection attr_id(string $label = null)
     * @method Show\Field|Collection indirect_num(string $label = null)
     * @method Show\Field|Collection order_price(string $label = null)
     * @method Show\Field|Collection product_count(string $label = null)
     * @method Show\Field|Collection product_name(string $label = null)
     * @method Show\Field|Collection product_price(string $label = null)
     * @method Show\Field|Collection product_type(string $label = null)
     * @method Show\Field|Collection share_num(string $label = null)
     * @method Show\Field|Collection shop_id(string $label = null)
     * @method Show\Field|Collection supplier_name(string $label = null)
     * @method Show\Field|Collection usdt_price(string $label = null)
     * @method Show\Field|Collection image(string $label = null)
     * @method Show\Field|Collection price(string $label = null)
     * @method Show\Field|Collection total_price(string $label = null)
     * @method Show\Field|Collection audit_time(string $label = null)
     * @method Show\Field|Collection audit_why(string $label = null)
     * @method Show\Field|Collection phone(string $label = null)
     * @method Show\Field|Collection product_status(string $label = null)
     * @method Show\Field|Collection return_no(string $label = null)
     * @method Show\Field|Collection state(string $label = null)
     * @method Show\Field|Collection why(string $label = null)
     * @method Show\Field|Collection point_pool(string $label = null)
     * @method Show\Field|Collection end_money(string $label = null)
     * @method Show\Field|Collection start_money(string $label = null)
     * @method Show\Field|Collection carts(string $label = null)
     * @method Show\Field|Collection category_id(string $label = null)
     * @method Show\Field|Collection category_one(string $label = null)
     * @method Show\Field|Collection coin_price(string $label = null)
     * @method Show\Field|Collection end_time(string $label = null)
     * @method Show\Field|Collection is_audit_push(string $label = null)
     * @method Show\Field|Collection is_pay(string $label = null)
     * @method Show\Field|Collection main_picture(string $label = null)
     * @method Show\Field|Collection picture(string $label = null)
     * @method Show\Field|Collection sales(string $label = null)
     * @method Show\Field|Collection start_time(string $label = null)
     * @method Show\Field|Collection stock(string $label = null)
     * @method Show\Field|Collection sortorder(string $label = null)
     * @method Show\Field|Collection img_id(string $label = null)
     * @method Show\Field|Collection orders(string $label = null)
     * @method Show\Field|Collection chargeid(string $label = null)
     * @method Show\Field|Collection coin_num(string $label = null)
     * @method Show\Field|Collection finished_at(string $label = null)
     * @method Show\Field|Collection hash(string $label = null)
     * @method Show\Field|Collection remarks(string $label = null)
     * @method Show\Field|Collection to_address(string $label = null)
     * @method Show\Field|Collection cycle(string $label = null)
     * @method Show\Field|Collection finshed_at(string $label = null)
     * @method Show\Field|Collection release_date(string $label = null)
     * @method Show\Field|Collection released_num(string $label = null)
     * @method Show\Field|Collection reset_num(string $label = null)
     * @method Show\Field|Collection speed_num(string $label = null)
     * @method Show\Field|Collection total_num(string $label = null)
     * @method Show\Field|Collection award_rate(string $label = null)
     * @method Show\Field|Collection end_count(string $label = null)
     * @method Show\Field|Collection start_count(string $label = null)
     * @method Show\Field|Collection audit_status(string $label = null)
     * @method Show\Field|Collection company_logo(string $label = null)
     * @method Show\Field|Collection company_zhi(string $label = null)
     * @method Show\Field|Collection legal_card1(string $label = null)
     * @method Show\Field|Collection legal_card2(string $label = null)
     * @method Show\Field|Collection shop_category_id(string $label = null)
     * @method Show\Field|Collection apply_grade(string $label = null)
     * @method Show\Field|Collection destroy_rate(string $label = null)
     * @method Show\Field|Collection jian_rate(string $label = null)
     * @method Show\Field|Collection pot_rate(string $label = null)
     * @method Show\Field|Collection sign_date(string $label = null)
     * @method Show\Field|Collection day(string $label = null)
     * @method Show\Field|Collection end_date(string $label = null)
     * @method Show\Field|Collection speed_date(string $label = null)
     * @method Show\Field|Collection speed_id(string $label = null)
     * @method Show\Field|Collection start_date(string $label = null)
     * @method Show\Field|Collection other_id(string $label = null)
     * @method Show\Field|Collection bank_id(string $label = null)
     * @method Show\Field|Collection bank_no(string $label = null)
     * @method Show\Field|Collection bank_phone(string $label = null)
     * @method Show\Field|Collection bank_user(string $label = null)
     * @method Show\Field|Collection is_default(string $label = null)
     * @method Show\Field|Collection idcard_no(string $label = null)
     * @method Show\Field|Collection real_name(string $label = null)
     * @method Show\Field|Collection activate(string $label = null)
     * @method Show\Field|Collection amount(string $label = null)
     * @method Show\Field|Collection cert_status(string $label = null)
     * @method Show\Field|Collection code(string $label = null)
     * @method Show\Field|Collection coin_money(string $label = null)
     * @method Show\Field|Collection deep(string $label = null)
     * @method Show\Field|Collection group_num(string $label = null)
     * @method Show\Field|Collection is_shop(string $label = null)
     * @method Show\Field|Collection myperfor(string $label = null)
     * @method Show\Field|Collection nickname(string $label = null)
     * @method Show\Field|Collection parent_name(string $label = null)
     * @method Show\Field|Collection regip(string $label = null)
     * @method Show\Field|Collection share_income(string $label = null)
     * @method Show\Field|Collection share_status(string $label = null)
     * @method Show\Field|Collection sign_money(string $label = null)
     * @method Show\Field|Collection total_income(string $label = null)
     * @method Show\Field|Collection total_recharge(string $label = null)
     * @method Show\Field|Collection total_withdraw(string $label = null)
     * @method Show\Field|Collection trade_password(string $label = null)
     * @method Show\Field|Collection zhi_num(string $label = null)
     * @method Show\Field|Collection is_valid(string $label = null)
     * @method Show\Field|Collection wage_month(string $label = null)
     * @method Show\Field|Collection end(string $label = null)
     * @method Show\Field|Collection profit(string $label = null)
     * @method Show\Field|Collection start(string $label = null)
     * @method Show\Field|Collection ac_amount(string $label = null)
     * @method Show\Field|Collection fee(string $label = null)
     * @method Show\Field|Collection fee_amount(string $label = null)
     * @method Show\Field|Collection finsh_time(string $label = null)
     * @method Show\Field|Collection no(string $label = null)
     */
    class Show {}

    /**
     
     */
    class Form {}

}

namespace Dcat\Admin\Grid {
    /**
     
     */
    class Column {}

    /**
     
     */
    class Filter {}
}

namespace Dcat\Admin\Show {
    /**
     
     */
    class Field {}
}
