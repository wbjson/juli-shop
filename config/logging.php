
<?php

use Monolog\Handler\StreamHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],


        //充值回调日志
        'recharge_callback' => [
            'driver' => 'daily',
            'path' => storage_path('logs/recharge_callback.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],
        'recharge' => [
            'driver' => 'daily',
            'path' => storage_path('logs/recharge.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'withdraw' => [
            'driver' => 'daily',
            'path' => storage_path('logs/withdraw.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],
        'withdraw_callback' => [
            'driver' => 'daily',
            'path' => storage_path('logs/withdraw_callback.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'invest' => [
            'driver' => 'daily',
            'path' => storage_path('logs/invest.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'coin-release' => [
            'driver' => 'daily',
            'path' => storage_path('logs/coin-release.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],
        'sms' => [
            'driver' => 'daily',
            'path' => storage_path('logs/sms.log'),
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],
    ],

];
