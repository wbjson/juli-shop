<?php 
return [
    'labels' => [
        'Banner' => 'Banner',
        'banner' => 'Banner',
    ],
    'fields' => [
        'name' => '标题',
        'banner' => 'banner图',
        'status' => '状态 0-无效 1-有效',
        'sort' => '排序',
    ],
    'options' => [
    ],
];
