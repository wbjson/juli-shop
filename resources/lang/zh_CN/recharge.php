<?php
return [
    'labels' => [
        'Recharge' => '充值',
        'recharge' => '充值',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'machine_id' => '矿机ID',
        'nums' => '充值数量',
        'amount' => '矿机价格',
        'rate' => '收益率',
        'output_multiple' => '出局倍数',
        'hash' => '充值订单hash',
        'status' => '状态 1-待确认 2-已成功 3-已失败',
        'finish_time' => '成功时间',
    ],
    'options' => [
    ],
];
