<?php 
return [
    'labels' => [
        'PoolLog' => 'PoolLog',
        'pool-log' => 'PoolLog',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'pool_id' => '池子ID',
        'amount' => '金额',
    ],
    'options' => [
    ],
];
