<?php 
return [
    'labels' => [
        'OrdersReturnsApply' => 'OrdersReturnsApply',
        'orders-returns-apply' => 'OrdersReturnsApply',
    ],
    'fields' => [
        'audit_time' => '审核时间',
        'audit_why' => '审核原因',
        'note' => '备注',
        'order_id' => '订单ID',
        'product_status' => '货物状态 0:已收到货 1:未收到货',
        'return_no' => '售后单号',
        'state' => '类型 0 仅退款 1退货退款',
        'status' => '审核状态 -1 拒绝 0 未审核 1审核通过',
        'user_id' => '用户ID',
        'why' => '退换货原因',
    ],
    'options' => [
    ],
];
