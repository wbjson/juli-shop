<?php 
return [
    'labels' => [
        'Cart' => 'Cart',
        'cart' => 'Cart',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'product_id' => '产品ID',
        'quantity' => '产品数量',
        'is_checked' => '是否选中',
    ],
    'options' => [
    ],
];
