<?php 
return [
    'labels' => [
        'SignLog' => '签到日志',
        'sign-log' => '签到日志',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'num' => '签到奖励数量',
        'sign_date' => 'sign_date',
    ],
    'options' => [
    ],
];
