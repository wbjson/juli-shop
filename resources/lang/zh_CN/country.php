<?php
return [
    'labels' => [
        'Country' => '国家',
        'country' => '国家',
    ],
    'fields' => [
        'name' => '国家名',
        'en_name' => '英文名',
        'code' => '国家区号',
        'status' => '状态 0-下架 1-上架',
    ],
    'options' => [
    ],
];
