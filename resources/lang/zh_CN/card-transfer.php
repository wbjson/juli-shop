<?php
return [
    'labels' => [
        'CardTransfer' => '卡牌转赠记录',
        'card-transfer' => '卡牌转赠记录',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'other_id' => '受赠人ID',
        'users_card_id' => '卡牌ID',
    ],
    'options' => [
    ],
];
