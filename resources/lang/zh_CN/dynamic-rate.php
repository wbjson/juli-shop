<?php
return [
    'labels' => [
        'DynamicRate' => '动态收益管理',
        'dynamic-rate' => '动态收益管理',
    ],
    'fields' => [
        'pledge_num' => '持仓算力',
        'dynamic_rate' => '动态收益比例',
    ],
    'options' => [
    ],
];
