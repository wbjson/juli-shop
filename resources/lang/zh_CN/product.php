<?php
return [
    'labels' => [
        'Product' => '商品管理',
        'product' => '商品管理',
    ],
    'fields' => [
        'shop_id' => '商家ID',
        'category_id' => '分类ID',
        'name' => '商品名称',
        'main_picture' => '主图',
        'picture' => '其他图片',
        'price' => '价格',
        'status' => '上架状态',
        'state' => '审核状态',
        'sales' => '总销量',
        'content' => '商品详情',
    ],
    'options' => [
    ],
];
