<?php
return [
    'labels' => [
        'LpMiningPoolLog' => 'LP矿池投资记录',
        'lp-mining-pool-log' => 'LP矿池投资记录',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'pool_id' => '矿池ID',
        'status' => '状态',
        'lp_num' => '投资数量',
        'rate' => '兑u汇率',
        'power' => '算力',
    ],
    'options' => [
    ],
];
