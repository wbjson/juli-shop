<?php
return [
    'labels' => [
        'Pool' => '节点矿池',
        'pool' => '节点矿池',
    ],
    'fields' => [
        'name' => '池子名称',
        'amount' => '池子金额',
    ],
    'options' => [
    ],
];
