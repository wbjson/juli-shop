<?php
return [
    'labels' => [
        'Redemption' => '赎回',
        'redemption' => '赎回',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'num' => '赎回数量',
    ],
    'options' => [
    ],
];
