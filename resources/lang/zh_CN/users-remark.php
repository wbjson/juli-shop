<?php 
return [
    'labels' => [
        'UsersRemark' => 'UsersRemark',
        'users-remark' => 'UsersRemark',
    ],
    'fields' => [
        'other_id' => '其他用户ID',
        'remark' => '备注',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
