<?php 
return [
    'labels' => [
        'Transfer' => 'Transfer',
        'transfer' => 'Transfer',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'other_id' => '接收者用户ID',
        'num' => '金额',
    ],
    'options' => [
    ],
];
