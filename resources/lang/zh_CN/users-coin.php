<?php 
return [
    'labels' => [
        'UsersCoin' => 'UsersCoin',
        'users-coin' => 'UsersCoin',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'coin_id' => '币种ID',
        'amount' => '币种数量',
    ],
    'options' => [
    ],
];
