<?php 
return [
    'labels' => [
        'PointSet' => '积分设置',
        'point-set' => '积分设置',
    ],
    'fields' => [
        'start_money' => '开始数量',
        'end_money' => '结束数量',
        'rate' => '倍数比例',
        'status' => 'status',
    ],
    'options' => [
    ],
];
