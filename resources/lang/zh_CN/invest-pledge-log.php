<?php 
return [
    'labels' => [
        'InvestPledgeLog' => 'InvestPledgeLog',
        'invest-pledge-log' => 'InvestPledgeLog',
    ],
    'fields' => [
        'before_power' => '加成前算力',
        'income' => '总收益',
        'invest_id' => '质押策略ID',
        'main_coin' => '主流币币种',
        'main_coin_num' => '主流币质押数量',
        'main_coin_rate' => '主流币兑u汇率',
        'other_coin' => '质押币币种',
        'other_coin_num' => '质押币数量',
        'other_coin_rate' => '质押币兑u汇率',
        'power' => '加成后算力',
        'status' => '状态 1-有效 2-停止发放 3-已赎回',
        'type' => '质押类型 1-30% 2-40% 3-50% 4-100%',
        'user_id' => '用户',
    ],
    'options' => [
    ],
];
