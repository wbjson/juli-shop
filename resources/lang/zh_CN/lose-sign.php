<?php 
return [
    'labels' => [
        'LoseSign' => '未签到金额',
        'lose-sign' => '未签到金额',
    ],
    'fields' => [
        'num' => '未签到金额',
        'user_id' => 'user_id',
    ],
    'options' => [
    ],
];
