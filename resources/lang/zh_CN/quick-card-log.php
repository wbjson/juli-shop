<?php
return [
    'labels' => [
        'QuickCardLog' => '加速卡购买记录',
        'quick-card-log' => '加速卡购买记录',
    ],
    'fields' => [
        'num' => '加速卡',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
