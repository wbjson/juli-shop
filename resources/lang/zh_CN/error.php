<?php

return [

    'register_timeout' => '注册超时,请重新注册',
    'phone_registered' => '手机号已注册过',
    'email_registered' => '邮箱已注册过',
    'invite_code_not_exists' => '邀请码不存在',
    'wrong_user_name_or_password' => '用户名或密码错误',
    'user_not_exists' => '用户未注册',
    'old_password_error' => '旧密码错误',
    'payment_method_exists' => '该收款类型已添加,请重新选择',
    'mnemonic_error' => '助记词错误，请重新输入',
    'Insufficient_balance' => '余额不足',
    'max_delivery' => '每人最多只能配送',
    'Please_fill_in_the_payment_method' => '请填写收款方式',
    'The_pledge_must_be_at_least_12_days' => '质押必须满12天',
];
