<?php
return [
    'labels' => [
        'Machine' => '矿机',
        'machine' => '矿机',
    ],
    'fields' => [
        'name' => '矿机名称',
        'img' => '矿机图片',
        'price' => '矿机价格',
        'num' => '解锁电池量',
        'min_rate' => '收益率最小值',
        'max_rate' => '收益率最大值',
        'output' => '预计产量',
        'status' => '状态',
        'sort' => '排序',
    ],
    'options' => [
    ],
];
