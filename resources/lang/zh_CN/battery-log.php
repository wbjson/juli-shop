<?php
return [
    'labels' => [
        'BatteryLog' => '电池',
        'battery-log' => '电池',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'num' => '电池量',
        'type' => '类型',
    ],
    'options' => [
    ],
];
