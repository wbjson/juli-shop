<?php 
return [
    'labels' => [
        'Otc' => 'Otc',
        'otc' => 'Otc',
    ],
    'fields' => [
        'num' => '数量',
        'status' => '状态 1-待支付 2-待确认 3-已完成',
        'type' => '买卖类型 1-买 2-卖',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
