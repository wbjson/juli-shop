<?php
return [
    'labels' => [
        'BoxLog' => '购买记录',
        'box-log' => '购买记录',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'amount' => '支付价格',
        'card_name' => '抽取卡牌名称',
        'currency_id' => '支付币种',
    ],
    'options' => [
    ],
];
