<?php
return [
    'labels' => [
        'Invest' => '挖矿',
        'invest' => '挖矿',
    ],
    'fields' => [
        'buy_status' => '是否可购买 1-可以 2-不可以',
        'coin' => '币种',
        'icon' => '图标',
        'rate' => '真实收益率',
        'status' => '状态 1-上架 2-下架',
        't_rate' => '假收益率',
        'type' => '类型 1-单币 2-lp 3-saas矿',
    ],
    'options' => [
    ],
];
