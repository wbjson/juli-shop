<?php
return [
    'labels' => [
        'IncomeLog' => '资金',
        'income-log' => '资金',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'usdb' => 'USDB数量',
        'entt' => 'entt数量',
        'type' => '类型 1-静态收益 2-直推收益 3-分享收益 4-团队收益',
    ],
    'options' => [
    ],
];
