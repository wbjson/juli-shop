<?php 
return [
    'labels' => [
        'ImLog' => 'ImLog',
        'im-log' => 'ImLog',
    ],
    'fields' => [
        'uid' => 'uid',
        'id_uid' => 'id_uid',
        'money' => 'money',
        'type' => '1-加,2-减',
    ],
    'options' => [
    ],
];
