<?php
return [
    'labels' => [
        'ProductExhibit' => '推荐管理',
        'product-exhibit' => '推荐管理',
    ],
    'fields' => [
        'product_id' => '商品',
        'type' => '推荐类型',
        'img_id' => '图片',
        'status' => '状态',
        'orders' => '排序',
    ],
    'options' => [
    ],
];
