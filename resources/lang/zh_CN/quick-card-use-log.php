<?php
return [
    'labels' => [
        'QuickCardUseLog' => '加速卡使用记录',
        'quick-card-use-log' => '加速卡使用记录',
    ],
    'fields' => [
        'num' => '数量',
        'type' => '类型',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
