<?php 
return [
    'labels' => [
        'BatteryTradePay' => 'BatteryTradePay',
        'battery-trade-pay' => 'BatteryTradePay',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'order_id' => '订单ID',
        'num' => '数量',
        'price' => '单价',
        'total' => '总价',
    ],
    'options' => [
    ],
];
