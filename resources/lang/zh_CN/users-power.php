<?php 
return [
    'labels' => [
        'UsersPower' => 'UsersPower',
        'users-power' => 'UsersPower',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'type' => '类型 1-单币池 2-全网算力池 3-流动性池子 4-saas池',
        'pledge' => '质押数量',
        'dynamic_power' => '动态算力',
        'power' => '静态算力',
    ],
    'options' => [
    ],
];
