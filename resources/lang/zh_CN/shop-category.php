<?php
return [
    'labels' => [
        'ShopCategory' => '商户类型配置',
        'shop-category' => '商户类型配置',
    ],
    'fields' => [
        'name' => '店铺类型名称',
        'jean_fee' => '让利配置',
    ],
    'options' => [
    ],
];
