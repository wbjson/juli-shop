<?php
return [
    'labels' => [
        'GroupCategory' => '社区类型管理',
        'group-category' => '社区类型管理',
    ],
    'fields' => [
        'name' => '社区类型名称',
        'rate' => '算力加成比例',
        'lp_factor' => 'LP质押条件',
        'grade_factor' => '等级条件',
    ],
    'options' => [
    ],
];
