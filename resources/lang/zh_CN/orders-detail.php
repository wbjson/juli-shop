<?php 
return [
    'labels' => [
        'OrdersDetail' => '订单详情',
        'orders-detail' => '订单详情',
    ],
    'fields' => [
        'order_id' => '订单ID',
        'user_id' => '用户ID',
        'shop_id' => '商家ID',
        'product_id' => '商品ID',
        'attr_id' => '规格ID',
        'product_name' => '商品名称',
        'product_img' => '商品图片',
        'supplier_name' => '商户名称',
        'order_status' => '订单状态 0未付款,1已付款,2已发货,3已签收,-1退货申请,-2退货中,-3已退货,-4取消交易',
        'product_count' => '购买数量',
        'product_price' => '产品单价',
        'shopping_id' => '收获地址ID',
        'type' => '1=>常规订单,2=>抢单',
        'logistics_fee' => '运费金额',
        'pay_remarks' => '订单备注',
    ],
    'options' => [
    ],
];
