<?php
return [
    'labels' => [
        'Delivery' => '申请配送',
        'delivery' => '申请配送',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'num' => '配送数量',
    ],
    'options' => [
    ],
];
