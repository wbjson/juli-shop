<?php
return [
    'labels' => [
        'MainCurrency' => '主流币管理',
        'main-currency' => '主流币管理',
    ],
    'fields' => [
        'name' => '币种名称',
        'coin_img' => '币种图片',
        'rate' => '兑U汇率',
        'contract_address' => '代币合约地址',
        'precision' => '代币精度',
    ],
    'options' => [
    ],
];
