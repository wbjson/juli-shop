<?php
return [
    'labels' => [
        'MainCurrencyExchange' => '币种转换管理',
        'main-currency-exchange' => '币种转换管理',
    ],
    'fields' => [
        'from_coin' => '兑换前币种',
        'from_coin_name' => '兑换前币种名称',
        'rate' => '转换汇率',
        'to_coin' => '兑换后币种',
        'to_coin_name' => '转换后币种名称',
    ],
    'options' => [
    ],
];
