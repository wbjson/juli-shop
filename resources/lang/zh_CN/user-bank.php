<?php 
return [
    'labels' => [
        'UserBank' => '用户银行卡',
        'user-bank' => '用户银行卡',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'bank_id' => 'bank_id',
        'bank_user' => 'bank_user',
        'bank_name' => 'bank_name',
        'bank_code' => 'bank_code',
        'bank_phone' => 'bank_phone',
        'bank_no' => '银行卡号',
        'is_default' => 'is_default',
    ],
    'options' => [
    ],
];
