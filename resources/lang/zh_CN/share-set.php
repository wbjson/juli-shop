<?php 
return [
    'labels' => [
        'ShareSet' => '推荐收益',
        'share-set' => '推荐收益',
    ],
    'fields' => [
        'award_rate' => '收益比例',
        'coin_release' => '加速积分释放',
        'get_layer' => '获得层数',
        'name' => '名称',
        'status' => 'status',
    ],
    'options' => [
    ],
];
