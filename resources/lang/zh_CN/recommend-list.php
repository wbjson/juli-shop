<?php 
return [
    'labels' => [
        'RecommendList' => 'RecommendList',
        'recommend-list' => 'RecommendList',
    ],
    'fields' => [
        'type' => '1 => \'首页推荐\',2 => \'支付页推荐\',3 => \'个人中心推荐\',',
        'icon' => '图标',
        'status' => 'status',
    ],
    'options' => [
    ],
];
