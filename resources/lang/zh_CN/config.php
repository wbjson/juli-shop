<?php 
return [
    'labels' => [
        'Config' => 'Config',
        'config' => 'Config',
    ],
    'fields' => [
        'key' => '配置键',
        'value' => '配置值',
        'desc' => '配置备注',
    ],
    'options' => [
    ],
];
