<?php
return [
    'labels' => [
        'ProductsCategory' => '商品类型管理',
        'products-category' => '商品类型管理',
    ],
    'fields' => [
        'parent_id' => '上级',
        'name' => '类别名称',
        'status' => '状态',
        'sortorder' => '类别排序',
    ],
    'options' => [
    ],
];
