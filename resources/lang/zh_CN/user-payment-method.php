<?php 
return [
    'labels' => [
        'UserPaymentMethod' => 'UserPaymentMethod',
        'user-payment-method' => 'UserPaymentMethod',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'type' => '类型 1-微信 2-支付宝 3-银行卡',
        'account' => '账号',
        'bank_name' => '开户行',
        'qr_code' => '二维码',
        'status' => '1-待审核 2-审核通过 3-审核失败',
    ],
    'options' => [
    ],
];
