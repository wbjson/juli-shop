<?php
return [
    'labels' => [
        'ExchangeOrder' => '兑换',
        'exchange-order' => '兑换',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'order_no' => '订单号',
        'num' => '闪兑金额',
        'rate' => '汇率',
        'result' => '到账金额',
    ],
    'options' => [
    ],
];
