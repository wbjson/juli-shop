<?php
return [
    'labels' => [
        'NodeCategory' => '节点管理',
        'node-category' => '节点管理',
    ],
    'fields' => [
        'name' => '节点名称',
        'type' => '节点类型',
        'rate' => '算力比例',
    ],
    'options' => [
    ],
];
