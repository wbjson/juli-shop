<?php 
return [
    'labels' => [
        'WageLog' => 'WageLog',
        'wage-log' => 'WageLog',
    ],
    'fields' => [
        'num' => '签到奖励数量',
        'user_id' => 'user_id',
        'wage_month' => 'wage_month',
    ],
    'options' => [
    ],
];
