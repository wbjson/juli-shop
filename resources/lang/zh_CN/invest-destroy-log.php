<?php 
return [
    'labels' => [
        'InvestDestroyLog' => 'InvestDestroyLog',
        'invest-destroy-log' => 'InvestDestroyLog',
    ],
    'fields' => [
        'before_power' => '计算前算力',
        'coin' => '币种',
        'coin_exchange_rate' => '兑u汇率',
        'coin_num' => '质押数量',
        'income' => '总收益',
        'invest_id' => '方案ID',
        'power' => '算力',
        'status' => '状态 1-有效 2-已赎回',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
