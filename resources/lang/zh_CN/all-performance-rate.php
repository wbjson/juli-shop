<?php
return [
    'labels' => [
        'AllPerformanceRate' => '全网算力加成配置',
        'all-performance-rate' => '全网算力加成配置',
    ],
    'fields' => [
        'max_performance' => '最大算力',
        'min_performance' => '最小算力',
        'rate' => '加成比例',
    ],
    'options' => [
    ],
];
