<?php
return [
    'labels' => [
        'LpMiningPool' => 'LP矿池管理',
        'lp-mining-pool' => 'LP矿池管理',
    ],
    'fields' => [
        'apr' => '收益率',
        'card_id' => '卡牌ID',
        'coin1' => '币种1',
        'coin2' => '币种2',
        'is_search' => '是否自动更新',
        'lp_address' => 'LP合约地址',
        'multiple' => '收益倍数',
        'name' => '矿池名称',
        'per' => '单个LP价值',
        'total' => '累计质押了多少u的算力',
    ],
    'options' => [
    ],
];
