<?php
return [
    'labels' => [
        'LevelConfig' => '等级配置',
        'level-config' => '等级配置',
    ],
    'fields' => [
        'id' => 'id',
        'name' => '等级名称',
        'machina_level' => '自身矿机等级',
        'group_level' => '自身团队等级',
        'zhi_machina' => '直推几个?矿机等级',
        'zhi_group' => '直推几个?团队等级',
        'san_group' => '伞下几个?团队等级(不同线)',
        'performance' => '伞下业绩',
        'income' => '每日分红',
    ],
    'options' => [
    ],
];
