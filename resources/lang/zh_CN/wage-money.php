<?php 
return [
    'labels' => [
        'WageMoney' => '工资设置',
        'wage-money' => '工资设置',
    ],
    'fields' => [
        'end' => 'end',
        'profit' => 'profit',
        'start' => 'start',
    ],
    'options' => [
    ],
];
