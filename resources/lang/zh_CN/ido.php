<?php
return [
    'labels' => [
        'Ido' => 'IDO配置',
        'ido' => 'Ido配置',
    ],
    'fields' => [
        'coin_id' => '币种ID',
        'price' => '价格',
        'freed_coin_id' => '释放币种',
        'freed' => '释放数量',
        'num' => '每个人可购买几次',
        'total' => '总质押',
    ],
    'options' => [
    ],
];
