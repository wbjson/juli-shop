<?php 
return [
    'labels' => [
        'News' => '公告',
        'news' => '公告',
    ],
    'fields' => [
        'content' => '内容',
        'sort' => '排序',
        'status' => '状态 0-下架 1-上架',
        'title' => '标题',
        'type' => '1=>关于我们,2=>隐私正常,3=>服务器协议',
    ],
    'options' => [
    ],
];
