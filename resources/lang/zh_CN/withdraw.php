<?php
return [
    'labels' => [
        'Withdraw' => '提现',
        'withdraw' => '提现',
    ],
    'fields' => [
        'no' => '订单号',
        'user_id' => '用户ID',
        'receive_address' => '接收方地址',
        'dai_num' => '出金entt数量',
        'usdt_num' => '出金usdt数量',
        'dai_rate' => '出金时与usdt兑换比例',
        'fee' => '手续费比例',
        'fee_amount' => '手续费金额',
        'ac_amount' => '实际到账金额usdt',
        'status' => '状态 1-待通过 2-已通过 3-失败',
        'finsh_time' => '到账时间',
    ],
    'options' => [
    ],
];
