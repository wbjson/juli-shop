<?php 
return [
    'labels' => [
        'InvestLpLog' => 'InvestLpLog',
        'invest-lp-log' => 'InvestLpLog',
    ],
    'fields' => [
        'income' => '总收益',
        'invest_id' => '方案ID',
        'main_coin' => '主流币币种',
        'status' => '状态 1-有效 2-已赎回',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
