<?php
return [
    'labels' => [
        'UsersCard' => '用户卡牌列表',
        'users-card' => '用户卡牌列表',
    ],
    'fields' => [
        'name' => '卡牌名称',
        'rate' => '加成率',
        'status' => '状态 1-正常 2-已质押 3-销售中',
        'token_id' => '卡牌ID  当前ID+10000，纯展示',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
