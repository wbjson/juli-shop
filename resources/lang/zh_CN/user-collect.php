<?php 
return [
    'labels' => [
        'UserCollect' => 'UserCollect',
        'user-collect' => 'UserCollect',
    ],
    'fields' => [
        'product_id' => 'product_id',
        'user_id' => 'user_id',
    ],
    'options' => [
    ],
];
