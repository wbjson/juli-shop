<?php
return [
    'labels' => [
        'IdoLog' => 'Ido购买记录',
        'ido-log' => 'Ido购买记录',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'price' => '购买价格',
        'total' => '释放数量',
    ],
    'options' => [
    ],
];
