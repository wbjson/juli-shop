<?php 
return [
    'labels' => [
        'SpeedOrder' => '加速订单',
        'speed-order' => '加速订单',
    ],
    'fields' => [
        'day' => 'day',
        'speed_date' => 'speed_date',
        'speed_id' => 'speed_id',
        'user_id' => 'user_id',
    ],
    'options' => [
    ],
];
