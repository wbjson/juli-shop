<?php
return [
    'labels' => [
        'CardAuction' => '拍卖管理',
        'card-auction' => '拍卖管理',
    ],
    'fields' => [
        'card_id' => '卡牌ID',
        'currency_id' => '竞拍币种',
        'current_price' => '当前竞拍价格',
        'jia_rate' => '加价比例',
        'price' => '价格',
    ],
    'options' => [
    ],
];
