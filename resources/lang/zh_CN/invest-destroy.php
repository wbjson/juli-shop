<?php
return [
    'labels' => [
        'InvestDestroy' => '双币矿池管理',
        'invest-destroy' => '双币矿池管理',
    ],
    'fields' => [
        'coin_id' => '对应币种ID',
        'name' => '币对名称',
        'income' => '年化收益率',
        'dynamic_rate' => '动态算力加成',
        'rate' => '静态算力加成',
        'total' => '总销量',
        'status' => '状态',
        'sort' => '排序'
    ],
    'options' => [
    ],
];
