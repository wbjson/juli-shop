<?php 
return [
    'labels' => [
        'ProductsAttr' => 'ProductsAttr',
        'products-attr' => 'ProductsAttr',
    ],
    'fields' => [
        'shop_id' => '店铺ID',
        'product_id' => '商品ID',
        'name' => '属性名',
        'value' => '属性值',
    ],
    'options' => [
    ],
];
