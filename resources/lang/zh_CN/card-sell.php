<?php
return [
    'labels' => [
        'CardSell' => '卡牌出售管理',
        'card-sell' => '卡牌出售管理',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'user_card_id' => '卡牌ID',
        'status' => '状态',
        'other_id' => '购买人',
        'price' => '出售价格',
        'buy_time' => '出售成功时间',
    ],
    'options' => [
    ],
];
