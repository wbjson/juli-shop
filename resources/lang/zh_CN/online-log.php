<?php 
return [
    'labels' => [
        'OnlineLog' => '在线任务',
        'online-log' => '在线任务',
    ],
    'fields' => [
        'line_date' => 'line_date',
        'num' => '签到奖励数量',
        'user_id' => 'user_id',
    ],
    'options' => [
    ],
];
