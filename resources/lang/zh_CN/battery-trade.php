<?php 
return [
    'labels' => [
        'BatteryTrade' => 'BatteryTrade',
        'battery-trade' => 'BatteryTrade',
    ],
    'fields' => [
        'user_id' => '发布者ID',
        'num' => '电池量',
        'price' => '发布价格',
        'total' => '总价格',
        'status' => '状态',
        'p_user_id' => '购买者ID',
        'pay_time' => '购买时间',
    ],
    'options' => [
    ],
];
