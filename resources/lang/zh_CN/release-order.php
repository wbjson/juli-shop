<?php 
return [
    'labels' => [
        'ReleaseOrder' => '释放订单',
        'release-order' => '释放订单',
    ],
    'fields' => [
        'user_id' => 'UID',
        'order_id' => '订单ID',
        'total_num' => '总积分',
        'released_num' => '已释放数量',
        'reset_num' => '剩余释放',
        'status' => '1=>释放中,2=>已完成',
        'release_date' => '释放日期',
    ],
    'options' => [
    ],
];
