<?php 
return [
    'labels' => [
        'BrowHistory' => '浏览记录',
        'brow-history' => '浏览记录',
    ],
    'fields' => [
        'product_id' => '商品ID',
        'product_img' => '商品图片',
        'date' => 'date',
    ],
    'options' => [
    ],
];
