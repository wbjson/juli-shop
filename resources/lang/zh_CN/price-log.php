<?php 
return [
    'labels' => [
        'PriceLog' => 'PriceLog',
        'price-log' => 'PriceLog',
    ],
    'fields' => [
        'date' => '日期',
        'diff' => '涨跌幅',
        'price' => '价格',
    ],
    'options' => [
    ],
];
