<?php 
return [
    'labels' => [
        'OrdersAppraise' => 'OrdersAppraise',
        'orders-appraise' => 'OrdersAppraise',
    ],
    'fields' => [
        'attitude_star' => '服务态度 1-5',
        'desc_star' => '描述相符 1-5',
        'info' => '评价内容',
        'level' => '级别 -1差评 0中评 1好评',
        'logistics_star' => '物流服务 1-5',
        'order_id' => '订单信息',
    ],
    'options' => [
    ],
];
