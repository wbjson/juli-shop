<?php
return [
    'labels' => [
        'ShopApplication' => '商户申请管理',
        'shop-application' => '商户申请管理',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'name' => '商铺名称',
        'shop_category_id' => '申请的店铺类型',
        'phone' => '联系方式',
        'username' => '法人姓名',
        'legal_card2' => '法人身份证反面',
        'legal_card1' => '法人身份证正面',
        'company_logo' => '公司logo',
        'company_zhi' => '公司资质',
    ],
    'options' => [
    ],
];
