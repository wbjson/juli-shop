<?php
return [
    'labels' => [
        'InvestPledge' => '质押矿池管理',
        'invest-pledge' => '质押矿池管理',
    ],
    'fields' => [
        'main_coin' => '主流币',
        'other_coin' => '质押币',
        'name' => '币对名称',
        'income' => '年化收益率',
        'seven_rate' => '主流70%+质押币30%算力加成',
        'six_rate' => '主流60%+质押币40%算力加成',
        'five_rate' => '主流50%+质押币50%算力加成',
        'zero_rate' => '主流0%+质押币100%算力加成',
        'status' => '状态',
        'pledge_rate' => '质押手续费率',
        'redemption_rate' => '赎回手续费率',
        'sort' => '排序',
        'total' => '总销量',
    ],
    'options' => [
    ],
];
