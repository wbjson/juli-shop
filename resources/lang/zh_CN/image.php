<?php 
return [
    'labels' => [
        'Image' => 'Image',
        'image' => 'Image',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'path' => '图片路径',
    ],
    'options' => [
    ],
];
