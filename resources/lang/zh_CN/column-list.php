<?php 
return [
    'labels' => [
        'ColumnList' => '栏目',
        'column-list' => '栏目',
    ],
    'fields' => [
        'icon' => 'icon',
        'link' => 'link',
        'name' => 'name',
        'sort' => '排序',
        'status' => '1',
        'type' => '1=>首页,2=>商城',
    ],
    'options' => [
    ],
];
