<?php
return [
    'labels' => [
        'Card' => '卡牌配置',
        'card' => '卡牌配置',
    ],
    'fields' => [
        'name' => '卡牌名称',
        'img' => '卡牌图片',
        'rate' => '加成率',
        'lottery_rate' => '抽奖概率',
    ],
    'options' => [
    ],
];
