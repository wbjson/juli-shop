<?php 
return [
    'labels' => [
        'OrdersShopping' => 'OrdersShopping',
        'orders-shopping' => 'OrdersShopping',
    ],
    'fields' => [
        'address' => '详细地址',
        'city' => '市',
        'district' => '区',
        'group_id' => '代收点ID 非代收点为0',
        'name' => '收货人姓名',
        'order_id' => '订单ID',
        'phone' => '收货人电话',
        'province' => '省',
        'user_id' => '用户ID',
    ],
    'options' => [
    ],
];
