<?php
return [
    'labels' => [
        'UsersDestroy' => '商户销毁',
        'users-destroy' => '商户销毁',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'shop_id' => '商家ID',
        'amount' => '销毁数量',
        'shop_amount' => '商家数量',
        'shop_has_amount' => '用户已获得数量',
        'user_amount' => '用户数量',
        'user_has_amount' => '商家已获得数量',
    ],
    'options' => [
    ],
];
