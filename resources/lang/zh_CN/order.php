<?php 
return [
    'labels' => [
        'Order' => 'Order',
        'order' => 'Order',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'after_status' => '用户售后状态 0 未发起售后 1 申请售后 -1 售后已取消 2 处理中 200 处理完毕',
        'delivery_time' => '发货时间',
        'logistics_fee' => '运费金额',
        'order_amount_total' => '实际付款金额',
        'order_no' => '订单编号',
        'order_settlement_status' => '订单结算状态 0未结算 1已结算',
        'order_settlement_time' => '订单结算时间',
        'order_status' => '订单状态 0未付款,1已付款,2已发货,3已签收,-1退货申请,-2退货中,-3已退货,-4取消交易',
        'pay_remarks' => '订单备注',
        'payment_type' => '支付类型 1-现金账户 2-消费账户 3-在线支付',
        'product_amount_total' => '产品总价',
        'product_count' => '购买数量',
        'product_price' => '产品单价',
        'shopping_id' => '收货信息ID',
        'supplier_name' => '商户名称',
    ],
    'options' => [
    ],
];
