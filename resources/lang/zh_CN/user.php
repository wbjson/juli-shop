<?php
return [
    'labels' => [
        'User' => '用户',
        'user' => '用户',
    ],
    'fields' => [
        'name' => '用户名',
        'wallet' => '钱包地址',
        'path' => '推荐路径',
        'code' => '邀请码',
        'parent_id' => '上级用户',
        'avatar' => '头像',
        'status' => '状态 0-禁用 1-有效',
        'password' => 'password',
        'zhi_num' => '直推人数',
        'valid_zhi_num' => '有效认证直推人数',
        'group_num' => '团队人数',
        'battery' => '电池',
        'usdt' => 'usdt钱包',
        'usdb' => 'usdb钱包',
        'entt' => 'entt钱包',
        'recharge_id' => '购买等级 (矿机等级)',
        'group_id' => '团队等级',
        'max_div' => '最大收益金额',
        'has_div' => '已收益金额',
        'is_open' => '是否可以继续分红 1-是 2-否',
        'performance' => '伞下业绩',
        'community_performance' => '小区业绩(去除伞下最大一条线的业绩)',
    ],
    'options' => [
    ],
];
