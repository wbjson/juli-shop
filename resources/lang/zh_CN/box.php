<?php
return [
    'labels' => [
        'Box' => '盲盒价格配置',
        'box' => '盲盒价格配置',
    ],
    'fields' => [
        'amount' => '价格',
        'currency_id' => '支付币种',
        'is_exchange' => '是否为兑U价格',
        'status' => '状态 0-关闭 1-开启',
    ],
    'options' => [
    ],
];
