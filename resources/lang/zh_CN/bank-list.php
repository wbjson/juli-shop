<?php 
return [
    'labels' => [
        'BankList' => '银行卡列表',
        'bank-list' => '银行卡列表',
    ],
    'fields' => [
        'bank_name' => 'bank_name',
        'bank_code' => 'bank_code',
        'status' => 'status',
    ],
    'options' => [
    ],
];
