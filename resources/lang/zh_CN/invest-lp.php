<?php
return [
    'labels' => [
        'InvestLp' => 'LP矿池管理',
        'invest-lp' => 'LP矿池管理',
    ],
    'fields' => [
        'main_coin' => '主流币',
        'other_coin' => '质押币',
        'income' => '收益率',
        'rate' => '静态算力加成',
        'dynamic_rate' => '动态算力加成',
        'status' => '状态',
        'total' => '总销量',
    ],
    'options' => [
    ],
];
