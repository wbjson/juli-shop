<?php 
return [
    'labels' => [
        'UserCert' => '实名记录',
        'user-cert' => '实名记录',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'real_name' => '真实姓名',
        'idcard_no' => '身份证号',
        'status' => '0=>待审核,1=>已通过,2=>已拒绝',
    ],
    'options' => [
    ],
];
