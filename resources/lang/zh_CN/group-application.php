<?php
return [
    'labels' => [
        'GroupApplication' => '社区申请',
        'group-application' => '社区申请',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'status' => '状态',
        'group_id' => '社区ID',
        'country' => '申请国家',
        'province' => '申请省',
        'city' => '申请市',
        'district' => '申请区',
        'address' => '详细地址',
    ],
    'options' => [
    ],
];
