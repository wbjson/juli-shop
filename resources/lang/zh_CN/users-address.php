<?php 
return [
    'labels' => [
        'UsersAddress' => 'UsersAddress',
        'users-address' => 'UsersAddress',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'name' => '收货人姓名',
        'phone' => '收货人电话',
        'province' => '省',
        'city' => '市',
        'district' => '区',
        'address' => '详细地址',
    ],
    'options' => [
    ],
];
