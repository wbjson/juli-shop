<?php
return [
    'labels' => [
        'Bulletin' => '公告',
        'bulletin' => '公告',
    ],
    'fields' => [
        'title' => '标题',
        'content' => '内容',
        'status' => '状态',
        'sort' => '排序',
    ],
    'options' => [
    ],
];
