<?php 
return [
    'labels' => [
        'CardAuctionLog' => 'CardAuctionLog',
        'card-auction-log' => 'CardAuctionLog',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'amount' => '支付价格',
        'auction_id' => '拍卖ID',
        'status' => '状态',
    ],
    'options' => [
    ],
];
