<?php

return [

    'register_timeout' => 'Registration timed out, please re-register',
    'phone_registered' => 'The phone number has been registered',
    'email_registered' => 'Email has been registered',
    'invite_code_not_exists' => 'Invitation code does not exist',
    'wrong_user_name_or_password' => 'wrong user name or password',
    'user_not_exists' => 'User not registered',
    'old_password_error' => 'Old password error',
    'payment_method_exists' => 'The payment type has been added, please select again',
    'mnemonic_error' => 'The mnemonic phrase is wrong, please re-enter',
    'Insufficient_balance' => 'Insufficient balance',
    'max_delivery' => 'Can only deliver up to',
    'Please_fill_in_the_payment_method' => 'Please fill in the payment method',
    'The_pledge_must_be_at_least_12_days' => 'The pledge must be at least 12 days',
];
