<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
*/

Route::namespace('App\Http\Controllers\Api')->group(function () {
    //提供给区块那边调用的
    Route::middleware(['checkCallback'])->prefix('callback')->group(function () {
        Route::post('recharge_callback', 'CallbackController@recharge');
        Route::post('withdraw_callback', 'CallbackController@withdraw');
    });

    Route::prefix('auth')->group(function () {
        Route::get('saveLog', 'AuthController@saveLog');
    });


    //需要接口sign鉴权
    Route::middleware('CheckApiAuth')->group(function () {
        Route::prefix('auth')->group(function () {
            //注册
            Route::post('register', 'AuthController@register');
            ##图片验证码
            Route::post('captcha', 'AuthController@captcha');
            //登录
            Route::post('login', 'AuthController@login');
            //找回密码
            Route::post('findPassword', 'AuthController@findPassword');
            //APP信息
            Route::post('appInfo', 'AuthController@appInfo');
            ##客服二维码
            Route::post('serviceQrcode', 'AuthController@serviceQrcode');
        });

        /*商城UI*/
        Route::prefix('mall')->group(function () {
            //注册
            Route::post('mall', 'SuperMallController@mall');

        });


        Route::prefix('title')->group(function () {
            //文章详情
            Route::post('titleInfo', 'BasicController@titleInfo');

        });

        ##不需要登陆基础接口
        Route::prefix('basic')->group(function () {
            Route::post('banner', 'BasicController@banner');
            ##提现配置
            Route::post('withdrawConfig', 'BasicController@withdrawConfig');
            ##发送短信
            Route::post('sendSms', 'BasicController@sendSms');
        });

        //首页相关
        Route::prefix('index')->group(function () {
            //首页
            Route::post('index', 'IndexController@index');
            ##积分商城
            Route::post('coinMall', 'SuperMallController@coinMall');
            /*商品*/
            Route::post('productList', 'IndexController@productList');

            ##推荐商品列表
            Route::post('orderRecomList', 'IndexController@orderRecomList');
            ##积分资金池数量
            Route::post('pointPool', 'SuperMallController@pointPool');
        });

        //产品
        Route::prefix('product')->group(function () {
            //产品推荐列表
            Route::post('exhibit', 'ProductController@exhibit');
            //商品分类列表
            Route::post('productCategory', 'ProductController@productCategory');
            ##商品分类详情
            Route::post('categoryDetail', 'ProductController@categoryDetail');
            //商品列表
            Route::post('productList', 'ProductController@productList');
            //商品详情
            Route::post('detail', 'ProductController@detail');
            //上传商品,修改上传的商品
            //上传的商品列表
            //上传的商品详情
            Route::post('pointGoods', 'ProductController@pointGoods');
        });


        Route::prefix('imconntect')->group(function () {
            //支出
            Route::post('imExpend', 'ImConnectController@imExpend');

            //收入
            Route::post('imIncome', 'ImConnectController@imIncome');
        });


        //需要验证登录
        Route::middleware(['checkUserLogin'])->group(function () {

            Route::prefix('auth')->group(function () {
                //用户信息
                Route::post('me', 'AuthController@me');


                Route::post('checkPawwrord', 'AuthController@checkPawwrord');
                ##设置交易密码
                Route::post('updatePassword', 'AuthController@updatePassword');

                Route::post('setTradePassword', 'AuthController@setTradePassword');

                Route::post('logout', 'AuthController@logout');
            });


            //商家相关接口
            Route::prefix('shop')->group(function () {
                //商家入驻接口
                Route::post('shopApplication', 'ShopController@shopApplication');
                //商家入驻申请列表
                Route::post('shopApplicationList', 'ShopController@shopApplicationList');
                //商家入驻申请详情
                Route::post('shopApplicationDetail', 'ShopController@shopApplicationDetail');
                //商家基础信息
                Route::post('info', 'ShopController@info');
                //我的产品列表
                Route::post('productList', 'ShopController@productList');
                //上传或编辑产品
                Route::post('pushProduct', 'ShopController@pushProduct');
                //商品详情
                Route::post('productDetail', 'ShopController@productDetail');
                //提交审核
                Route::post('pushProductAudit', 'ShopController@pushProductAudit');
                //上下架产品
                Route::post('upDownProduct', 'ShopController@upDownProduct');
            });

            //社区相关接口
            Route::prefix('group')->group(function () {
                //社区入驻申请列表
                Route::post('applyList', 'GroupController@applyList');
                //社区入驻申请详情
                Route::post('applyDetail', 'GroupController@applyDetail');
                //社区入驻接口
                Route::post('apply', 'GroupController@apply');

            });

            //购物车相关接口
            Route::prefix('cart')->group(function () {
                //购物车列表
                Route::post('cartList', 'CartController@cartList');
                //加入购物车
                Route::post('addCart', 'CartController@addCart');
                //删除购物车商品
                Route::post('destroyCart', 'CartController@destroyCart');
                //增加减少购物车商品数量
                Route::post('updateNumCart', 'CartController@updateNumCart');
                //设置选中或不选中
                Route::post('checkedCart', 'CartController@checkedCart');
            });

            //订单相关接口
            Route::prefix('orders')->group(function () {
                //去结算
                Route::post('generate', 'OrdersController@generate');
                //支付订单
                Route::post('pay', 'OrdersController@pay');

                ##合约支付USDT
//                Route::post('payorder','InvestController@payorder');
                //我的订单列表
                Route::post('orderList', 'OrdersController@orderList');
                ##订单详情
                Route::post('orderDetail', 'OrdersController@orderDetail');
                //修改地址
                Route::post('addressUpdate', 'OrdersController@addressUpdate');
                //确认收货
                Route::post('submit', 'OrdersController@submit');
                //取消订单
                Route::post('cancel', 'OrdersController@cancel');
                //售后申请
                Route::post('returnsApply', 'OrdersController@returnsApply');
                //售后详情
                Route::post('returnsDetail', 'OrdersController@returnsDetail');
                //确认售后信息并填写退货信息
                Route::post('returns', 'OrdersController@returns');
                //订单评价
                Route::post('appraise', 'OrdersController@appraise');


                //商家端我的订单列表
                Route::post('shopOrderList', 'OrdersController@shopOrderList');
                //商家端确认发货
                Route::post('ship', 'OrdersController@ship');
                //商家端审核退货退款申请
                Route::post('auditReturns', 'OrdersController@auditReturns');
                //商家端确认收货并退款
                Route::post('submitReturns', 'OrdersController@submitReturns');
            });

            //地址相关接口
            Route::prefix('address')->group(function () {
                //收货地址列表
                Route::post('list', 'AddressController@index');
                //收货地址详情
                Route::post('detail', 'AddressController@show');
                //新增收货地址
                Route::post('store', 'AddressController@store');
                //修改收货地址
                Route::post('update', 'AddressController@update');
                //删除收货地址
                Route::post('destroy', 'AddressController@destroy');
                //设置默认
                Route::post('setDefault', 'AddressController@setDefaultAddress');
                //获取默认地址
                Route::post('getDefault', 'AddressController@getDefaultAddress');
            });


            Route::prefix('basic')->group(function () {
                //商城广告图片列表,包含banner与其他位置图片
                //上传文件
                Route::post('upload', 'BasicController@upload');
                //商铺类别列表
                Route::post('shopCategory', 'BasicController@shopCategory');
                //社区类别列表
                Route::post('groupCategory', 'BasicController@groupCategory');

                //LP矿池
                Route::post('lpMiningPool', 'BasicController@lpMiningPool');
                //LP矿池我的质押
                Route::post('lpList', 'BasicController@lpList');
                //赎回LP质押
                Route::post('redemption', 'BasicController@redemption');
            });


            Route::prefix('user')->group(function () {
                //内部转账
                Route::post('userInfo', 'UserController@userInfo');
                ##收藏列表
                Route::post('collectList', 'UserController@collectList');
                ##收藏商品
                Route::post('collectGood', 'UserController@collectGood');
                ##浏览记录
                Route::post('browHistory', 'UserController@browHistory');
                ##清空浏览记录
                Route::post('deleteHistory', 'UserController@deleteHistory');
                ##资金记录
                Route::post('incomeLog', 'UserController@incomeLog');
                ##资金类型
                Route::post('typeList', 'UserController@typeList');

                Route::post('userTeam', 'UserController@userTeam');

                ##提交实名
                Route::post('userCertVerified', 'UserController@userCertVerified');
                ##实名信息
                Route::post('userCertInfo', 'UserController@userCertInfo');
                ##签到赠送积分
                Route::post('sign', 'UserController@sign');

            });

            Route::prefix('userbank')->group(function () {
                ##银行卡列表
                Route::post('bankList', 'UserBankController@bankList');
                ##添加银行卡
                Route::post('addBankCard', 'UserBankController@addBankCard');
                ##用户银行卡
                Route::post('userBank', 'UserBankController@userBank');
                ##删除银行卡
                Route::post('delUserBank', 'UserBankController@delUserBank');
            });

            Route::prefix('transfer')->group(function () {
                //内部转账
                Route::post('transfer', 'TransferController@transfer');
                //内部转账记录
                Route::post('transferList', 'TransferController@transferList');
            });

            //充值
            Route::prefix('recharge')->group(function () {
                //检查是否可购买
                Route::post('check', 'RechargeController@check');
                //充值记录
                Route::post('list', 'RechargeController@list');
            });

            Route::prefix('withdraw')->group(function () {
                //提现
                Route::post('index', 'WithdrawController@index');
                //提现列表
                Route::post('list', 'WithdrawController@list');
            });



            Route::prefix('profit')->group(function () {
                //提现
                Route::post('onlineProfit', 'ProfitController@onlineProfit');
                Route::post('wageMoney', 'ProfitController@wageMoney');
            });
        });

    });

});

